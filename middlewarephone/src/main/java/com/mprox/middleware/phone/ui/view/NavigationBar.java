package com.mprox.middleware.phone.ui.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.phone.ui.activity.LoginActivity;
import com.mprox.middleware.phone.ui.activity.MapActivity;
import com.mprox.middleware.phone.ui.activity.MiddlewareActivity;
import com.mprox.middleware.phone.ui.activity.ProfileActivity;

public class NavigationBar extends LinearLayout {

	private Context context;
	private String titleString;

	private TextView titleView;
	private ImageButton profileBtn;
    private ImageButton profileMap;

	public NavigationBar(Context context, AttributeSet attrs) {
		super(context, attrs);

		this.context = context;

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.NavigationBar, 0, 0);
		try {
			int titleResource = a.getResourceId(R.styleable.NavigationBar_navigationTitle, 0);
			if (titleResource != 0) {
				titleString = context.getResources().getString(titleResource);
			}
		} finally {
			a.recycle();
		}

		initView();
	}

	private void initView() {
        LayoutInflater.from(context).inflate(R.layout.navigation_bar, this);

        titleView = (TextView) findViewById(R.id.navigation_bar_title);

        if (titleView != null && titleString != null && titleString.length() > 0) {
            titleView.setText(titleString);
        }

        profileBtn = (ImageButton) findViewById(R.id.btn_profile);
        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(((MiddlewareActivity) context), ProfileActivity.class);
                intent.putExtra(LoginActivity.SHOW_NAV_BAR, true);
                ((MiddlewareActivity) context).startActivity(intent);
                ((MiddlewareActivity) context).finish();
            }
        });

        profileMap = (ImageButton) findViewById(R.id.btn_map);
        if (titleString != null && titleString.equals(getResources().getString(R.string.home))){
            profileMap.setVisibility(View.VISIBLE);
            profileBtn.setVisibility(View.VISIBLE);
            profileMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    Intent intent = new Intent(((MiddlewareActivity) context), MapActivity.class);
                    ((MiddlewareActivity) context).startActivity(intent);
                }
            });
        }else{
            profileMap.setVisibility(View.GONE);
            profileBtn.setVisibility(View.GONE);
        }
	}

	public void setTitleText(String titleText) {
		this.titleString = titleText;
		titleView.setText(titleString);
	}
}
