package com.mprox.middleware.phone.ui.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Evento;
import com.mprox.middleware.core.service.LoginTask;
import com.mprox.middleware.core.service.request.LoginRequest;
import com.mprox.middleware.core.service.responses.LoginResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.util.pusher.ServerUtilities;
import com.mprox.sdk.utils.UtilsMprox;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginActivity extends MiddlewareActivity {

	private static final int MODE_LOGIN = 0;
	private static final int MODE_REGISTER = 1;
	private static final int MODE_LOGOUT = 2;

	public static final String SHOW_HOME = "showHome";
	public static final String SHOW_NAV_BAR = "showNavBar";
	public static final String SEND_CHECKIN = "sendCheckin";
	public final static String EVENT_TO_LOGIN = "eventToLogin";

	private int mode = MODE_LOGIN;

	private LinearLayout loginFields;
	private Button loginBtn;

	private boolean showHome;
	private boolean showNavBar;
	private boolean sendCheckin;
	private Evento evento;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		showHome = getIntent().getBooleanExtra(SHOW_HOME, true);
		showNavBar = getIntent().getBooleanExtra(SHOW_NAV_BAR, false);
		sendCheckin = getIntent().getBooleanExtra(SEND_CHECKIN, false);
		evento = getIntent().getExtras().getParcelable(EVENT_TO_LOGIN);

		findAndInitViews();
	}

	@Override
	protected void findAndInitViews() {
		loginBtn = (Button) findViewById(R.id.btn_login);
		loginFields = (LinearLayout) findViewById(R.id.login_fields);
		ImageView logoImage = (ImageView) findViewById(R.id.imageView);
		if (evento.getImagen() != null && evento.getImagen().length() != 0) {
			String url = UtilsMiddleware.formatImageUrl(evento.getImagen());
			Picasso.with(LoginActivity.this).load(url).into(logoImage);
		}

		EditText nameInput = (EditText) findViewById(R.id.name_imput);
		EditText passInput = (EditText) findViewById(R.id.password_input);

		TextWatcher watcher = new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				validateInputFields();
			}
		};

		nameInput.addTextChangedListener(watcher);
		passInput.addTextChangedListener(watcher);

		loginBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loginOnClick();
			}
		});

		if (!UtilsMiddleware.isNullOrEmpty(UtilsMiddleware.getInstance(this).getLogedUserEmail())) {
			setLogOutView();
		} else {
			setLoginView();
		}
	}

	private void setLoginView() {
		mode = MODE_LOGIN;

		loginFields.setVisibility(View.VISIBLE);
		loginBtn.setText(getResources().getString(R.string.login));

		validateInputFields();
	}

	private void setRegisterView() {
		mode = MODE_REGISTER;

		loginFields.setVisibility(View.GONE);
		loginBtn.setText(getResources().getString(R.string.register));
		validateInputFields();
	}

	private void setLogOutView() {
		mode = MODE_LOGOUT;

		loginFields.setVisibility(View.GONE);
		loginBtn.setText(getResources().getString(R.string.logout));
	}

	@Override
	public void onBackPressed() {
		if (mode == MODE_REGISTER) {
			setLoginView();
		} else if (showHome && !showNavBar) {
			exitPopUp();
		} else {
			super.onBackPressed();
		}
	}

	private void exitPopUp() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage(R.string._est_seguro_de_querer_salir_de_la_aplicaci_n_);
		builder.setNegativeButton("No", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.setPositiveButton("Si", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});

		builder.create().show();
	}

	private void loginOnClick() {
		if (mode == MODE_LOGIN) {
			EditText nameInput = (EditText) findViewById(R.id.name_imput);
			EditText passInput = (EditText) findViewById(R.id.password_input);

			performLogin(nameInput.getText().toString(), passInput.getText().toString(), null);
		} else if (mode == MODE_LOGOUT) {
			UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserEmail("");
			UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserId(0);
			UtilsMiddleware.getInstance(LoginActivity.this).clearTags();
			setLoginView();
		}
	}

	private void performLogin(String name, String password, String facebookEmail) {
		LoginRequest request = new LoginRequest(name, password, facebookEmail);

		findViewById(R.id.loading_view).setVisibility(View.VISIBLE);

		LoginTask task = new LoginTask() {

			@Override
			public void onServiceReturned(LoginResponse response) {
				findViewById(R.id.loading_view).setVisibility(View.GONE);
				if (response.isSuccess() && response.getData() != null) {
					UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserEmail(response.getData().getEmail());
					UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserName(response.getData().getName());
					UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserId(response.getData().getId().intValue());
                    UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserImage(response.getData().getImage());
                    UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserTeam(response.getData().getEquipoParticipante());

                    UtilsMprox.getInstance(LoginActivity.this).saveLogedUserEmail(response.getData().getEmail());
                    UtilsMprox.getInstance(LoginActivity.this).saveLogedUserName(response.getData().getName());
                    UtilsMprox.getInstance(LoginActivity.this).saveLogedUserId(String.valueOf(response.getData().getId()));
                    UtilsMprox.getInstance(LoginActivity.this).saveLogedUserTeam(String.valueOf(response.getData().getEquipoParticipante()));

                    sendTags(response.getData().getEquipoParticipante());

                    UtilsMiddleware.getInstance(LoginActivity.this).clearTags();
					if (response.getData().getTags() != null) {
						List<String> preferences = Arrays.asList(response.getData().getTags().split(";"));
						for (int i = 0; i < preferences.size(); i++) {
							UtilsMiddleware.getInstance(LoginActivity.this).addAppPreference(preferences.get(i));
						}
					}
					continueToNext();
				} else {
					String responseMsg = response.getErrorMsessage();
					if (response.getData() == null) {
						responseMsg = getResources().getString(R.string.login_bad_error);
					} else {
						if (UtilsMiddleware.isNullOrEmpty(responseMsg)) {
							responseMsg = getResources().getString(R.string.register_standar_error_msg);
						}
					}

					Toast.makeText(LoginActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
				}

			}
		};

		task.execute(request);
	}

    private void sendTags(String tags){
        try {
            String[] items = tags.split(",");
            List<String> tagsList = new ArrayList<>();
            for (int i = 0; i < items.length; i++) {
                tagsList.add(items[i]);
            }

            final JSONObject serviceParams = new JSONObject();
            serviceParams.put("uuid", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            serviceParams.put("tags", tagsList);

            new Thread(new Runnable(){
                @Override
                public void run() {
                    try {
                        ServerUtilities.post("/device/setTags", serviceParams);
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
        }catch (Exception e){
            //Nothing
        }

    }


	private void validateInputFields() {
		if (mode == MODE_LOGIN) {
			EditText nameInput = (EditText) findViewById(R.id.name_imput);
			EditText passInput = (EditText) findViewById(R.id.password_input);

			if (UtilsMiddleware.isNullOrEmpty(nameInput.getText().toString())
					|| UtilsMiddleware.isNullOrEmpty(passInput.getText().toString())) {
				loginBtn.setEnabled(false);
				loginBtn.setClickable(false);
				return;
			}
			loginBtn.setEnabled(true);
			loginBtn.setClickable(true);
		}
	}

	private void continueToNext() {
		if (sendCheckin) {
			if (UtilsMiddleware.isNullOrEmpty(UtilsMiddleware.getInstance(this).getLogedUserEmail())) {
				finish();
			}
		} else if (showHome) {
			Intent intent = new Intent(this, HomeActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		finish();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 104 && resultCode == RESULT_OK) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				cursor.close();

				decodeFile(selectedImage);
				UtilsMiddleware.getInstance(LoginActivity.this).saveLogedUserImage(picturePath);
		}
	}
}