package com.mprox.middleware.phone.util.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.core.domain.Product;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

@SuppressLint("SimpleDateFormat")
public class ActividadGridAdapter extends BaseAdapter {

	private ArrayList<Actividad> myItems;
	private Context mContext;

	public ActividadGridAdapter(Context context, ArrayList<Actividad> newList) {
		this.mContext = context;
		this.myItems = newList;
	}

	@Override
	public int getCount() {
		if (myItems == null) {
			return 0;
		}

		return myItems.size();
	}

	@Override
	public Object getItem(int arg0) {
		return myItems.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {

		View returnView = null;
		if (convertView == null) {
			returnView = LayoutInflater.from(mContext).inflate(R.layout.agendas_grid_item, arg2, false);
		} else {
			returnView = convertView;
		}

        RelativeLayout itemContent = (RelativeLayout) returnView.findViewById(R.id.itemContent);
        if(myItems.get(position).getColor() != null && myItems.get(position).getColor().length() > 0) {
            itemContent.setBackgroundColor(Color.parseColor(myItems.get(position).getColor()));
        }else{
            itemContent.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

		Typeface dinRegular = MiddlewareApplication.getDinRegular();

		TextView labelName = (TextView) returnView.findViewById(R.id.benefit_name);
		labelName.setTypeface(dinRegular);
		labelName.setText(myItems.get(position).getNombre());

		TextView labelSala = (TextView) returnView.findViewById(R.id.labelSala);
		labelSala.setTypeface(dinRegular);
		labelSala.setText(myItems.get(position).getSala().getNombre());

		TextView labelInicio = (TextView) returnView.findViewById(R.id.labelInicio);
		labelInicio.setTypeface(dinRegular);
		labelInicio.setText("(" + myItems.get(position).getHoraStart() + " - ");

		TextView labelFin =  (TextView) returnView.findViewById(R.id.labelFin);
		labelFin.setTypeface(dinRegular);
		labelFin.setText(myItems.get(position).getHoraEnd() + ")");

		RatingBar redeem_price = (RatingBar) returnView.findViewById(R.id.redeem_price);
		redeem_price.setRating(myItems.get(position).getVoteTotal());
        redeem_price.setOnClickListener(null);

		return returnView;
	}
}
