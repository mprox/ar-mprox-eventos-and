package com.mprox.middleware.phone.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Product;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.util.pusher.ServerUtilities;
import com.mprox.sdk.MproxManager;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Leo
 */
public class SplashActivity extends Activity {

    private String message;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        // Se loguea el open de la notificacion.
        if (getIntent()!= null && getIntent().getStringExtra("message") != null) {
            message = getIntent().getStringExtra("message");
            ServerUtilities.setNotificationId(getIntent().getStringExtra("notificationId"));
            ServerUtilities.logActivity();
        }

        Log.i("mprox-services", "MproxManager -- init");
        MproxManager.getInstance().init(SplashActivity.this, "1f4ed9f0271498ee9b97425b2724b45f3",
                "jk6hljluk8isd0hld85g69rpr9");

        MproxManager.getInstance().bluetoothAlertConfiguration(SplashActivity.this
                , 1, getResources().getString(R.string.message_no_bluetooth_enabled), MproxManager.AlertType.ALERT_TIMES);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
                           @Override
                           public void run() {
                               start();
                           }
                       }, 5000);
    }

    /**
     * Open Home if device is online.
     */
    private void start() {
        if (UtilsMiddleware.isNullOrEmpty(UtilsMiddleware.getInstance(this).getLogedUserEmail())) {
            Intent intent = new Intent(this, SelectEventActivity.class);
//            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, HomeActivity.class);
            ArrayList<Product> products = getIntent().getParcelableArrayListExtra(HomeActivity.PRODUCTS_LIST);
            if (products != null) {
                intent.putExtra(HomeActivity.PRODUCTS_LIST, products);
            }
            if(message != null){
                intent.putExtra(HomeActivity.MESSAGE, message);
            }
            startActivity(intent);
            finish();
        }
    }
}