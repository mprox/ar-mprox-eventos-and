package com.mprox.middleware.phone.ui.view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Store;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.squareup.picasso.Picasso;

public class StoreListItem extends LinearLayout {

	private Context mContext;

	private ImageView storeImage;
	private TextView storeName;
	private TextView storeLocation;
	private TextView storeValued;

	private ImageView share_button;

	public StoreListItem(final Context context) {
		super(context);

		LayoutInflater.from(context).inflate(R.layout.store_list_item, this);
		mContext = context;

		storeImage = (ImageView) findViewById(R.id.store_image);
		storeName = (TextView) findViewById(R.id.store_name);
		storeLocation = (TextView) findViewById(R.id.store_location);
		storeValued = (TextView) findViewById(R.id.store_valued);
		share_button = (ImageView) findViewById(R.id.share_button);
	}

	public void setStore(final Store store) {

		storeName.setText(store.getName());
		storeLocation.setText(store.getLocation());
		storeValued.setText(String.valueOf(store.getVoteQuantity()));

		if (store.getPhotoDetailsUrl() != null) {
			try {
				String url2 = UtilsMiddleware.formatImageUrl(store.getPhotoDetailsUrl());
				Picasso.with(mContext).load(url2).into(storeImage);
			} catch (Exception e) {
				Log.e("GridAdapter", "error with url: " + store.getPhotoDetailsUrl());
			}
		}

	}
}
