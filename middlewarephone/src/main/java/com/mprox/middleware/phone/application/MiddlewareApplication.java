package com.mprox.middleware.phone.application;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.StrictMode;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.application.AndroidApplication;
import com.mprox.middleware.core.domain.Pregunta;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.phone.ui.activity.MiddlewareActivity;
import com.mprox.middleware.phone.util.pusher.IPHPusher;

import java.util.ArrayList;

/**
 * Radio Disney application for Android
 * 
 * @author Leo
 * 
 */
public class MiddlewareApplication extends AndroidApplication {

	/** Keep global info about the application */
	private static Context appContext;

	public static final String SHARED_PREFERENCE = "MIDDLEWARE_DIRECTV_EVENTOS";

	/** Keep global info about the application */
	private static boolean inForeground = true;

	private Activity currentctivity;

	private static Long appId = 22L;

	private static Typeface dinRegular;
	private static Typeface dinBold;

    private ArrayList<User> asistentesList;
    private ArrayList<Pregunta> encuestasList;

    private Tracker mTracker;


    public void onCreate() {
		appContext = this.getApplicationContext();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        dinRegular = Typeface.createFromAsset(getAssets(), "DIN.ttf");
		dinBold = Typeface.createFromAsset(getAssets(), "DINAlternateBold.ttf");

		// Push Notifications
		IPHPusher.sharedInstance().init(this, appContext.getString(R.string.pusher_main_key),
				appContext.getString(R.string.pusher_secret_key),
				appContext.getString(R.string.pusher_sender_id));

		super.onCreate();
	}

    /**
     * Gets the default {@link Tracker} for this.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }


    /**
	 * Get context object.
	 * 
	 * @return
	 */
	public static MiddlewareApplication getAppContext() {

		return (MiddlewareApplication) appContext;
	}

	/**
	 * @param currentctivity
	 *            the currentctivity to set
	 */
	public void setCurrentctivity(MiddlewareActivity currentctivity) {
		this.currentctivity = currentctivity;
	}


	public static void setInForeground(boolean newInForeground) {
		inForeground = newInForeground;
	}
	
	public static Long getAppId() {
		return appId;
	}

	public static Typeface getDinRegular() {
		return dinRegular;
	}

	public static Typeface getDinBold() {
		return dinBold;
	}

    public ArrayList<User> getAsistentesList() {
        if(asistentesList == null){
            asistentesList = new ArrayList<>();
            return asistentesList;
        }else{
            return asistentesList;
        }
    }

    public void setAsistentesList(ArrayList<User> asistentesList) {
        this.asistentesList = asistentesList;
    }

    public ArrayList<Pregunta> getEncuestasList() {
        if(encuestasList == null){
            encuestasList = new ArrayList<>();
            return encuestasList;
        }else {
            return encuestasList;
        }
    }

    public void setEncuestasList(ArrayList<Pregunta> encuestasList) {
        this.encuestasList = encuestasList;
    }
}
