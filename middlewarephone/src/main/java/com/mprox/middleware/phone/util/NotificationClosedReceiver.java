package com.mprox.middleware.phone.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mprox.middleware.phone.ui.activity.SplashActivity;

public class NotificationClosedReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i = new Intent(context, SplashActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

}
