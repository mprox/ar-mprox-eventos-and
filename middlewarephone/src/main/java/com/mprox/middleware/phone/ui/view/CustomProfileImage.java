package com.mprox.middleware.phone.ui.view;

import java.lang.ref.WeakReference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CustomProfileImage extends ImageView {

	protected Context mContext;
	private static final Xfermode sXfermode = new PorterDuffXfermode(PorterDuff.Mode.DST_IN);
	private Bitmap mMaskBitmap;
	private Paint mPaint;
	private WeakReference<Bitmap> mWeakBitmap;

	public CustomProfileImage(Context context) {
		super(context);
		sharedConstructor(context);
	}

	public CustomProfileImage(Context context, AttributeSet attrs) {
		super(context, attrs);
		sharedConstructor(context);
	}

	public CustomProfileImage(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		sharedConstructor(context);
	}

	private void sharedConstructor(Context context) {
		mContext = context;

		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	}

	public void invalidate() {
		mWeakBitmap = null;
		if (mMaskBitmap != null) {
			mMaskBitmap.recycle();
		}
		super.invalidate();
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		if (!isInEditMode()) {
			int i = canvas.saveLayer(0.0f, 0.0f, getWidth(), getHeight(), null, Canvas.ALL_SAVE_FLAG);
			try {
				Bitmap bitmap = mWeakBitmap != null ? mWeakBitmap.get() : null;
				// Bitmap not loaded.
				if (bitmap == null || bitmap.isRecycled()) {
					Drawable drawable = getDrawable();
					if (drawable != null) {
						// Allocation onDraw but it's ok because it will not
						// always be called.
						bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
						Canvas bitmapCanvas = new Canvas(bitmap);
						drawable.setBounds(0, 0, getWidth(), getHeight());
						drawable.draw(bitmapCanvas);

						// If mask is already set, skip and use cached mask.
						if (mMaskBitmap == null || mMaskBitmap.isRecycled()) {
							mMaskBitmap = getBitmap(getWidth(), getHeight());
						}

						// Draw Bitmap.
						mPaint.reset();
						mPaint.setFilterBitmap(false);
						mPaint.setXfermode(sXfermode);
						bitmapCanvas.drawBitmap(mMaskBitmap, 0.0f, 0.0f, mPaint);

						mWeakBitmap = new WeakReference<Bitmap>(bitmap);
					}
				}

				// Bitmap already loaded.
				if (bitmap != null) {
					mPaint.setXfermode(null);
					canvas.drawBitmap(bitmap, 0.0f, 0.0f, mPaint);
					return;
				}
			} catch (Exception e) {
				System.gc();
			} finally {
				canvas.restoreToCount(i);
			}
		} else {
			super.onDraw(canvas);
		}
	}

	public static Bitmap getBitmap(int width, int height) {
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.BLACK);
		canvas.drawOval(new RectF(0.0f, 0.0f, width, height), paint);

		return bitmap;
	}
}
