package com.mprox.middleware.phone.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mprox.middleware.phone.ui.activity.SplashActivity;

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent i = new Intent(context, SplashActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } catch (Exception e) {
            //Nothing
        }
    }

}