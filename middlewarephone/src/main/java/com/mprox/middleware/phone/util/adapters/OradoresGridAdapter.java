package com.mprox.middleware.phone.util.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Orador;
import com.mprox.middleware.core.domain.Product;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

@SuppressLint("SimpleDateFormat")
public class OradoresGridAdapter extends BaseAdapter {

	private ArrayList<User> myItems;
	private Context mContext;

	public OradoresGridAdapter(Context context, ArrayList<User> newList) {
		this.mContext = context;
		this.myItems = newList;
	}

	@Override
	public int getCount() {
		if (myItems == null) {
			return 0;
		}

		return myItems.size();
	}

	@Override
	public Object getItem(int arg0) {
		return myItems.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		View returnView = null;
		if (convertView == null) {
			returnView = LayoutInflater.from(mContext).inflate(R.layout.benefit_grid_item, arg2, false);
		} else {
			returnView = convertView;
		}

		Typeface dinRegular = MiddlewareApplication.getDinRegular();

		TextView benefit_name = (TextView) returnView.findViewById(R.id.benefit_name);
		benefit_name.setTypeface(dinRegular);
		benefit_name.setText(myItems.get(position).getName());

		TextView store = (TextView)  returnView.findViewById(R.id.store);
		store.setTypeface(dinRegular);
		store.setText(myItems.get(position).getDescripcion());

        if(myItems.get(position).getImagen() != null) {
            if (myItems.get(position).getImagen().contains("/mproxeventos/usuarios/")) {
                String url2 = UtilsMiddleware.formatImageUrl(myItems.get(position).getImagen());
                Picasso.with(mContext)
                        .load(url2)
                        .into(((ImageView) returnView.findViewById(R.id.benefit_image)));
            } else {
                ((ImageView) returnView.findViewById(R.id.benefit_image)).setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(myItems.get(position).getImagen(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                ((ImageView) returnView.findViewById(R.id.benefit_image)).setImageBitmap(decodedByte);
            }
        }

		RatingBar redeem_price = (RatingBar) returnView.findViewById(R.id.redeem_price);
		redeem_price.setRating(myItems.get(position).getVoteTotal());

		return returnView;
	}
}
