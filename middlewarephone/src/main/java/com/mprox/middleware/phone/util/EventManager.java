/**
 * 
 */
package com.mprox.middleware.phone.util;

import android.annotation.SuppressLint;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An event Service will be created in order to manage all the application
 * events in a centralized way. Each widget will have to register itself in the
 * <code>EventService</code> class providing a topic and an observer.<br>
 * A topic is an event name that the widget is listening to and an observer is
 * an object that will take care of that event.<br>
 * The <code>EventService</code> will keep track of all the widgets that are
 * registered and will iterate over this collection of observers executing the
 * method (formed as "on" + topic). If such a method does not exist a
 * <code>Error</code> will be thrown. The method name is created with an "on"
 * prefix plus the topic name and must be in camel-case.
 *
 * @author oscar.lamas
 * @since 2.0
 */
public final class EventManager {

	/** Singleton instance */
	private static EventManager instance;

	/** Observers by topics */
	private static final Map<String,  List<Object>> topics = new HashMap<String,  List<Object>>();

	/** Private contructor */
	private EventManager(){}


	/**
	 * Get the event manager instance
	 * @return
	 */
	public static EventManager getInstance() {
		if(instance == null){
			instance = new EventManager();
		}
		return instance;
	}

	/**
	 *  Registers a given listener for a notifications
	 *  regarding the specified topic.
	 *
	 * @param anObserver Object which will receive notifications.
	 *                   Can not be null.
	 * @param aTopic Event name or notification topic to observe.
	 *               Can not be and empty string or null.
	 * @throws Runtime Exception If any of the parameters are null.
	 */
	public void addObserver(Object anObserver, String aTopic) {
		if(anObserver == null) {
			throw new RuntimeException("The observer cannot be null.");
		}
		
		if(aTopic == null || aTopic.length() < 1) {
			throw new RuntimeException("A topic cannot be empty.");
		}
		
		// Find the observer list by topic
		List<Object> observers = topics.get(aTopic);
		if(observers == null) {

			// The topic does not exist already. Create
			// a new one and add the observer into the
			// new observer list.
			List<Object> newListOfObservers = new ArrayList<Object>();
			newListOfObservers.add(anObserver);
			topics.put(aTopic, newListOfObservers);

		} else {
			if(!this._isAlreadyRegistered(anObserver, observers)){
				// The observer was not registered already
				// Add it to the list
				observers.add(anObserver);
			}
		}
	}

	/**
	 * Unregisters a given listener from notifications
	 * regarding all topics in which such listener is
	 * registered.
	 *
	 *  @param anObserver To be removed from the service.
	 *  @param aTopic The notification topic or event name.
	 *                If this parameter is null the observer
	 *                will be removed from all topics.
	 */
	public void removeObserver(Object anObserver, String aTopic) {
        try {
            if (aTopic == null) {
                // The observer should be removed
                // from the service.
                for (String key : topics.keySet()) {
                    this._removeObserverFromTopic(anObserver, key);
                }

            } else {
                // Its has to be removed from the give
                // topic only
                this._removeObserverFromTopic(anObserver, aTopic);
            }
            // remove unusable topics
            this._compactTopicsMap();
        }catch (Exception e){
            //Nothing
        }
	}

	/**
	 * Notifies all registered listeners for the given topic.
	 *
	 * @param aTopic The notification topic or event name.
	 * @param someData Data to be passed.
	 */
	public void notify(String aTopic, Object someData) {
		try{
			List<Object> observers = topics.get(aTopic);
	
			if(observers != null) {
				// For each observer registered execute
				// the method formed as 'on' plus the topic
				for (Object observer: observers) {
					this._doNotify(observer, "on" + this._capitalize(aTopic.substring(0,1)) + aTopic.substring(1), someData);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	/**
	 * Removes the observer. The hash map is updated.
	 *
	 * @param anObserver to be removed.
	 * @param aTopic
	 */
	private void _removeObserverFromTopic(Object anObserver, String aTopic) {
        try{
            List<Object> observers = topics.get(aTopic);
            observers.remove(anObserver);
        }catch (Exception e){
            //Nothing
        }
	}


	/**
	 * This function calls a given function by name.
	 *
	 * @param object that receive the function call.
	 * @param data to be passed as parameter.
	 * @throws Runtime Exception if the object does not understand
	 *         the method name.
	 */
	private void _doNotify(Object object, String target, Object data) {
		try {
			Method method = ReflectionUtils.findMethod(object.getClass(), target, 
					new Class[]{Object.class});
			method.setAccessible(true);
			ReflectionUtils.invokeMethod(method, object, new Object[]{data});

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * This function check is the object is
	 * into the list.
	 *
	 * @return true if exists. Otherwise false.
	 */
	private Boolean _isAlreadyRegistered(Object object, List<Object> list) {
		return list.contains(object);
	}

	/**
	 * Delete topics from list which
	 * no observers are asociated
	 */
	private void _compactTopicsMap() {
		List<String> keys = new ArrayList<String>();
		
		for (String key : topics.keySet()) {
			List<Object> observers = topics.get(key);
			if(observers == null || observers.isEmpty()) {
				keys.add(key);
			}
		}
		
		for(String key : keys){
			topics.remove(key);
		}		
	}

	@SuppressLint("DefaultLocale") 
	private String _capitalize(String str) {
		String firstChar = str.substring(0, 1); 
		String restOfString = str.substring(1, str.length()); 
		return firstChar.toUpperCase()+restOfString.toLowerCase(); 
	}
}