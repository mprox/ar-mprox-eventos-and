package com.mprox.middleware.phone.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.mprox.middleware.R;

public class ShowNotificationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		if (intent.getStringExtra("message") != null) {

			AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
			builder1.setTitle("Nueva Notificación.");
			builder1.setIcon(R.mipmap.ic_launcher);
			builder1.setMessage(intent.getStringExtra("message"));
			builder1.setCancelable(false);
			builder1.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
					finish();
				}
			});

			AlertDialog alert11 = builder1.create();
			alert11.show();

		} else {
			finish();
		}

	}

}
