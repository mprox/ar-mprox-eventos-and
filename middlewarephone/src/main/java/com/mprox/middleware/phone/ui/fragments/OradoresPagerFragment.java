package com.mprox.middleware.phone.ui.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Orador;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.DetailsOradorActivity;
import com.mprox.middleware.phone.util.adapters.OradoresGridAdapter;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

public class OradoresPagerFragment extends Fragment {

	private final static String BENEFITLIST = "benefit_list";

	private ArrayList<User> benefitsList;
	private ArrayList<User> benefitsListOriginal;

	private StickyGridHeadersGridView grid;
	private OradoresGridAdapter adapter;

	public static OradoresPagerFragment newInstance(ArrayList<User> benefits) {
		OradoresPagerFragment fragment = new OradoresPagerFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelableArrayList(BENEFITLIST, benefits);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		benefitsList = getArguments().getParcelableArrayList(BENEFITLIST);
		benefitsListOriginal = new ArrayList<User>();
		benefitsListOriginal.addAll(benefitsList);
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		grid = (StickyGridHeadersGridView) inflater.inflate(R.layout.fragment_headers_benefits_list, null);
		grid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				opentDetails(benefitsList.get(arg2));
			}
		});

		adapter = new OradoresGridAdapter(getActivity(), benefitsList);
		grid.setAdapter(adapter);
		((StickyGridHeadersGridView) grid).setAreHeadersSticky(true);

		return grid;
	}

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((MiddlewareApplication)getActivity().getApplication()).getDefaultTracker();
        tracker.setScreenName("Oradores");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    private void opentDetails(User user) {
		Intent intent = new Intent(getActivity(), DetailsOradorActivity.class);
		intent.putExtra(DetailsOradorActivity.ORADOR_TO_SHOW, user);
		startActivity(intent);
	}
}