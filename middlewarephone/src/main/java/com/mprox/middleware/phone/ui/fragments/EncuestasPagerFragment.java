package com.mprox.middleware.phone.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Pregunta;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.DetailsPreguntaActivity;
import com.mprox.middleware.phone.util.adapters.EncuestaGridAdapter;

import java.util.ArrayList;

public class EncuestasPagerFragment extends Fragment {

	private final static String BENEFITLIST = "benefit_list";

	private ArrayList<Pregunta> preguntasList;
	private ListView listView;

	public static EncuestasPagerFragment newInstance(ArrayList<Pregunta> preguntas) {
		EncuestasPagerFragment fragment = new EncuestasPagerFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelableArrayList(BENEFITLIST, preguntas);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preguntasList = getArguments().getParcelableArrayList(BENEFITLIST);
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		listView = (ListView) inflater.inflate(R.layout.fragment_preguntas_list, null);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                openAgendaDetails(preguntasList.get(arg2));
            }
        });

		listView.setAdapter(new EncuestaGridAdapter(getActivity(), preguntasList));
		return listView;
	}

    private void openAgendaDetails(Pregunta pregunta) {
        Intent intent = new Intent(getActivity(), DetailsPreguntaActivity.class);
        intent.putExtra(DetailsPreguntaActivity.ACTIVITY_TO_SHOW, pregunta);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((MiddlewareApplication)getActivity().getApplication()).getDefaultTracker();
        tracker.setScreenName("Encuestas");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}