package com.mprox.middleware.phone.ui.activity;

import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mprox.middleware.R;
import com.mprox.middleware.phone.application.MiddlewareApplication;

public class MapActivity extends MiddlewareActivity implements OnMapReadyCallback {

    private Double HOTEL_LAT  =  -34.4388586;
    private Double HOTEL_LONG = -59.0498871;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    protected void findAndInitViews(){
        //Nothing
    }

    @Override
    public void onMapReady(GoogleMap map) {
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng hotel = new LatLng(HOTEL_LAT, HOTEL_LONG);
        map.addMarker(new MarkerOptions().position(hotel).title("San Ceferino Hotel & Spa")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon)));

        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        map.moveCamera(CameraUpdateFactory.newLatLng(hotel));
        map.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tracker tracker = ((MiddlewareApplication)getApplication()).getDefaultTracker();
        tracker.setScreenName("Ubicacion");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}