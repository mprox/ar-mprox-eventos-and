package com.mprox.middleware.phone.location;

import java.io.Serializable;

public class Coordinate2D implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double latitude;
	public double longitude;
	public Coordinate2D() {		
	}
	public Coordinate2D(double lat, double longi) {
			latitude = lat;
			longitude = longi;
	}
	
//	public GeoPoint getAsGeoPoint()
//	{
//		int _1E6 = (int) Math.pow(10, 6);
//		return new GeoPoint((int)(this.latitude*_1E6),  (int)(this.longitude*_1E6)) ;		
//	}
	
}
