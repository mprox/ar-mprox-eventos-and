package com.mprox.middleware.phone.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;
import com.mprox.middleware.core.service.AsignAccionPointsTask;
import com.mprox.middleware.core.service.request.AsignPointRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.ui.activity.MiddlewareActivity;

public class UserPointsUtil {

	@SuppressLint("SimpleDateFormat")
	public static void assignPointsWinning(final MiddlewareActivity activity, String action, String description, int points) {

		Long userId = Long.parseLong(String.valueOf(UtilsMiddleware.getInstance(activity).getLogedUserId()));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		AsignPointRequest request = new AsignPointRequest(userId, action, sdf.format(new Date()).toString(), description, points);

		AsignAccionPointsTask task = new AsignAccionPointsTask() {
			@Override
			public void onServiceReturned(BaseResponse response) {
				// none to do
			}
		};
		task.execute(request);
	}

}
