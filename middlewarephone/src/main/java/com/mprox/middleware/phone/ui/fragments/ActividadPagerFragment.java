package com.mprox.middleware.phone.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.DetailsActividadActivity;
import com.mprox.middleware.phone.util.adapters.ActividadGridAdapter;

import java.util.ArrayList;

public class ActividadPagerFragment extends Fragment{

	private final static String BENEFITLIST = "benefit_list";

	private ArrayList<Actividad> benefitsList;
	private ArrayList<Actividad> benefitsListOriginal;

	private ListView grid;
	private ActividadGridAdapter adapter;

	public static ActividadPagerFragment newInstance(ArrayList<Actividad> benefits) {
		ActividadPagerFragment fragment = new ActividadPagerFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelableArrayList(BENEFITLIST, benefits);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		benefitsList = getArguments().getParcelableArrayList(BENEFITLIST);

		benefitsListOriginal = new ArrayList<Actividad>();
		if (benefitsList != null)
			benefitsListOriginal.addAll(benefitsList);
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		grid = (ListView) inflater.inflate(R.layout.fragment_agenda_list, null);
		grid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				openAgendaDetails(benefitsList.get(arg2));
			}
		});

		adapter = new ActividadGridAdapter(getActivity(), benefitsList);
		grid.setAdapter(adapter);
		return grid;
	}

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((MiddlewareApplication)getActivity().getApplication()).getDefaultTracker();
        tracker.setScreenName("Temas");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void openAgendaDetails(Actividad actividad) {
		Intent intent = new Intent(getActivity(), DetailsActividadActivity.class);
		intent.putExtra(DetailsActividadActivity.ACTIVITY_TO_SHOW, actividad);
		startActivity(intent);
	}
}