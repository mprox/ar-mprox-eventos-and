package com.mprox.middleware.phone.util.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Pregunta;
import com.mprox.middleware.phone.application.MiddlewareApplication;

import java.util.ArrayList;

@SuppressLint("SimpleDateFormat")
public class EncuestaGridAdapter extends BaseAdapter {

	private ArrayList<Pregunta> preguntas;
	private Context mContext;
	private View returnView;

	public EncuestaGridAdapter(Context context, ArrayList<Pregunta> newList) {
		this.mContext = context;
		this.preguntas = newList;
	}

	@Override
	public int getCount() {
		if (preguntas == null) {
			return 0;
		}

		return preguntas.size();
	}

	@Override
	public Object getItem(int arg0) {
		return preguntas.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		if (convertView == null) {
			returnView = LayoutInflater.from(mContext).inflate(R.layout.preguntas_grid_item, arg2, false);
		} else {
			returnView = convertView;
		}

		Typeface dinRegular = MiddlewareApplication.getDinRegular();

		TextView labelName = (TextView) returnView.findViewById(R.id.benefit_name);
		labelName.setTypeface(dinRegular);
		labelName.setText(preguntas.get(position).getPregunta());

		return returnView;
	}
}
