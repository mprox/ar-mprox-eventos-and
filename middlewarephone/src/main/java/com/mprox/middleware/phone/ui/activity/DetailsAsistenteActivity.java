package com.mprox.middleware.phone.ui.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.squareup.picasso.Picasso;

public class DetailsAsistenteActivity extends MiddlewareActivity{

	public final static String ASISTENTE_TO_SHOW = "sistente_to_show";
	private User asistente;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		asistente = getIntent().getExtras().getParcelable(ASISTENTE_TO_SHOW);

		setContentView(R.layout.activity_asistente_details);

		findAndInitViews();
	}

	@Override
	protected void findAndInitViews() {
		hideNavigationBarOptions();

		TextView title = (TextView) findViewById(R.id.benefit_name);
		title.setText(asistente.getName());
		title.setTypeface(MiddlewareApplication.getDinBold());

		TextView description = (TextView) findViewById(R.id.benefit_description_text);
		description.setText(asistente.getDescripcion());
		description.setTypeface(MiddlewareApplication.getDinRegular());

		ImageView benefitImage = (ImageView) findViewById(R.id.benefit_image);

        if(asistente.getImage() != null) {
            if (asistente.getImage().contains("/mproxeventos/usuarios/")) {
                String url2 = UtilsMiddleware.formatImageUrl(asistente.getImage());
                Picasso.with(DetailsAsistenteActivity.this)
                        .load(url2)
                        .into(benefitImage);
            } else {
                benefitImage.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(asistente.getImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                benefitImage.setImageBitmap(decodedByte);
            }
        }
	}

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = ((MiddlewareApplication)getApplication()).getDefaultTracker();
        tracker.setScreenName("AsistentesDetalle");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}