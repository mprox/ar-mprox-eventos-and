package com.mprox.middleware.phone.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.CalificacionActividad;
import com.mprox.middleware.core.domain.Opcion;
import com.mprox.middleware.core.domain.Pregunta;
import com.mprox.middleware.core.domain.Respuesta;
import com.mprox.middleware.core.service.GetAgendaCalificacionesTask;
import com.mprox.middleware.core.service.GetRespuestasTask;
import com.mprox.middleware.core.service.SendEncuestasTask;
import com.mprox.middleware.core.service.request.ActividadRequest;
import com.mprox.middleware.core.service.request.GetRespuestaRequest;
import com.mprox.middleware.core.service.request.SendEncuestaRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.service.responses.GetCalificacionesResponse;
import com.mprox.middleware.core.service.responses.GetRespuestaResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.view.CalificacionListItem;

import java.util.ArrayList;
import java.util.List;

public class DetailsPreguntaActivity extends MiddlewareActivity{

	public final static String ACTIVITY_TO_SHOW = "activity_to_show";
	private Pregunta pregunta;
    private RelativeLayout loading;
    private RadioGroup radioGroup;
    //Para las preguntas sin opciones
    private EditText resSinOpc;
    private View resSinOpcV;
    private TextView suResp;
    private TextView ingSuResp;

    private List<Respuesta> respuestas;
    private boolean canResponde = true;
    private boolean isopcion = false;
    private Button buttonAceptar;
    private String textRespFree;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		pregunta = getIntent().getExtras().getParcelable(ACTIVITY_TO_SHOW);
        setContentView(R.layout.activity_pregunta_details);
        loading = (RelativeLayout) findViewById(R.id.loading);

        getRespuestas();
	}

	@Override
	protected void findAndInitViews() {
		hideNavigationBarOptions();
        buttonAceptar = (Button) findViewById(R.id.buttonAceptar);

        resSinOpc = (EditText) findViewById(R.id.textViewRespFree);
        suResp = (TextView) findViewById(R.id.suRespuesta);
        ingSuResp = (TextView) findViewById(R.id.ingreseSuRespuesta);

        resSinOpcV = (View) findViewById(R.id.line2);
        TextView labelQuestion = (TextView) findViewById(R.id.labelQuestion);
        labelQuestion.setText(pregunta.getPregunta());
        labelQuestion.setTypeface(MiddlewareApplication.getDinRegular());
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        buttonAceptar.setTypeface(MiddlewareApplication.getDinRegular());
        isopcion=pregunta.getOpciones().isEmpty();
        if(!isopcion) {
            resSinOpc.setVisibility(View.GONE);
            resSinOpcV.setVisibility(View.GONE);
            suResp.setVisibility(View.GONE);
            ingSuResp.setVisibility(View.GONE);
            buttonAceptar.setEnabled(false);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    buttonAceptar.setEnabled(true);
                }
            });
            for (Opcion opcion : pregunta.getOpciones()) {
                RadioButton radio = new RadioButton(this);
                radio.setText(opcion.getOpcion());
                radio.setTypeface(MiddlewareApplication.getDinRegular());
                radio.setTextColor(getResources().getColor(R.color.black));
                radio.setPadding(10, 0, 0, 10);
                radio.setBackgroundColor(getResources().getColor(R.color.clear_color));
                radio.setButtonDrawable(R.drawable.selector_checkbox);
                radio.setTextSize(25);
                radio.setGravity(Gravity.CENTER_VERTICAL);
                radio.setTag(opcion.getId());

                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 10, 0, 10);
                radio.setLayoutParams(params);
                radioGroup.addView(radio);

                if (checkUserAswers(opcion.getId(),0)) {
                    radio.setChecked(true);
                    canResponde = false;
                }
            }
        }else{

            radioGroup.setVisibility(View.GONE);
            suResp.setVisibility(View.GONE);
            buttonAceptar.setEnabled(false);
            buttonAceptar.setClickable(false);
            resSinOpc.addTextChangedListener(watcher);
            if (checkUserAswers(-1,pregunta.getId())) {canResponde = false;}

        }

       // Button buttonAceptar = (Button) findViewById(R.id.buttonAceptar);

        if(canResponde){
            buttonAceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SendEncuestasTask task = new SendEncuestasTask(DetailsPreguntaActivity.this) {
                        @Override
                        public void onServiceReturned(BaseResponse response) {
                            if(response != null){
                                Toast.makeText(DetailsPreguntaActivity.this,getResources().getString(R.string.question_ok),
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }else{
                                Toast.makeText(DetailsPreguntaActivity.this,getResources().getString(R.string.question_error),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    };
                    long userId = Long.parseLong(String.valueOf(UtilsMiddleware.getInstance(DetailsPreguntaActivity.this).getLogedUserId()));
                    SendEncuestaRequest request;
                    if(!pregunta.getOpciones().isEmpty()) {
                        int radioButtonID = radioGroup.getCheckedRadioButtonId();
                        View radioButton = radioGroup.findViewById(radioButtonID);
                        int idx = radioGroup.indexOfChild(radioButton);
                        RadioButton r = (RadioButton) radioGroup.getChildAt(idx);
                        String respondeId = r.getTag().toString();
                        request = new SendEncuestaRequest(String.valueOf(userId), respondeId,null,null);
                    }else{
                        String textIngresado = resSinOpc.getText().toString();
                        request = new SendEncuestaRequest(String.valueOf(userId), null,pregunta.getId().toString(),textIngresado);
                    }
                    task.execute(request);
                }
            });
        }else{
            ingSuResp.setVisibility(View.GONE);
            buttonAceptar.setBackgroundColor(getResources().getColor(R.color.hintGray));
            if(!isopcion){
                for (int i = 0; i < radioGroup.getChildCount(); i++) {
                    radioGroup.getChildAt(i).setEnabled(false);
                }
            }else{
                resSinOpc.setText(textRespFree);
                resSinOpc.setEnabled(false);
                suResp.setVisibility(View.VISIBLE);
            }
        }
        resSinOpc.addTextChangedListener(watcher);
        Button buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
        buttonCancelar.setTypeface(MiddlewareApplication.getDinRegular());
        buttonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loading.setVisibility(View.GONE);
	}
    private boolean checkUserAswers(long id,long idPreg){
        try {
            for (Respuesta respuesta : respuestas) {
                if(respuesta.getOpcion()!= null) {
                    if (respuesta.getOpcion().getId() == id) {

                        return true;
                    }
                }else{
                    if(respuesta.getPregunta().getId()== idPreg){
                        textRespFree=respuesta.getTextoResp();
                        return true;
                    }
                }
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    private void getRespuestas(){
        GetRespuestasTask task = new GetRespuestasTask(this) {
            @Override
            public void onServiceReturned(GetRespuestaResponse response) {
                if (response.getData() != null && response.getData().size() > 0) {
                    respuestas = response.getData();
                }
                    findAndInitViews();
            }
        };
        long userId = Long.parseLong(String.valueOf(UtilsMiddleware.getInstance(this).getLogedUserId()));
        GetRespuestaRequest request = new GetRespuestaRequest(userId);
        task.execute(request);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = ((MiddlewareApplication)getApplication()).getDefaultTracker();
        tracker.setScreenName("EncuestasDetalle");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    protected void validateRespuestaSinOpcion() {
            EditText nameInput = (EditText) findViewById(R.id.textViewRespFree);
           if (UtilsMiddleware.isNullOrEmpty(nameInput.getText().toString())) {
                buttonAceptar.setEnabled(false);
                buttonAceptar.setClickable(false);
                return;
            }

            buttonAceptar.setEnabled(true);
            buttonAceptar.setClickable(true);
            isopcion=true;
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            validateRespuestaSinOpcion();
        }
    };
}