package com.mprox.middleware.phone.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.mprox.middleware.R;
import com.mprox.middleware.core.service.GetEventosTask;
import com.mprox.middleware.core.service.responses.GetEventosResponse;

public class EventoActivity extends MiddlewareActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		getEventosService();
	}

	@Override
	protected void findAndInitViews() {

	}

	private void getEventosService() {
		GetEventosTask task = new GetEventosTask(EventoActivity.this) {
			@Override
			public void onServiceReturned(GetEventosResponse response) {
				if (response != null && response.getData() != null) {

				}
				findAndInitViews();
			}
		};

		findViewById(R.id.loading_view).setVisibility(View.VISIBLE);
		task.execute();
	}

	@Override
	public void onBackPressed() {
		// Voy a Home
	}
}