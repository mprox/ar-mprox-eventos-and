package com.mprox.middleware.phone.util.pusher;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Base64;

import com.google.android.gcm.GCMRegistrar;
import com.mprox.middleware.phone.application.MiddlewareApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public final class ServerUtilities{

	private static String SERVER_URL = "http://107.20.237.55/middleware/pushcommunication";
	private static String API_SERVER_URL = "http://107.20.237.55/mprox-eventos/apicommunication";
	protected static double lastTime = 0;
	protected static double duration = 0;
	protected static Timer timer_;
	protected static TimerTask timerTask_;
	private static String notificationId;
	
    public static void register(final Context context, final String regId) {
        JSONObject params = new JSONObject();
        
        try {
	        params.put("type", "ANDROID");
	        params.put("deviceToken", regId);
	        params.put("uuid", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
			params.put("deviceModel", android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);
	        params.put("deviceSystemVersion", android.os.Build.VERSION.RELEASE);
	        params.put("language", Locale.getDefault().getISO3Language());
	        params.put("country", context.getResources().getConfiguration().locale.getISO3Country());
	        params.put("geolocalization", "0.0|0.0");

        
			post("/device/registerDevice", params);
			GCMRegistrar.setRegisteredOnServer(context, true);

		} catch (JSONException e) {
			e.printStackTrace();
		}
    }

    private static void logActivity(final String notificationId) {
    	
    	if(ServerUtilities.getNotificationId()==null)
    		return;
    	
    	ServerUtilities.stopLoginActivity();
    	
    	if(lastTime>0)
    		lastTime = (System.currentTimeMillis() / 1000.0) - duration;
    	
    	timer_ = new Timer();
    	timerTask_ = new TimerTask() {
			@Override
			public void run() {
				JSONObject params = new JSONObject();
				try {
					params.put("uuid", Secure.getString(IPHPusher.sharedInstance().get_context().getContentResolver(), Secure.ANDROID_ID));
					params.put("notificationId", notificationId);
				} catch (JSONException e) { e.printStackTrace(); }
				
				if(lastTime == 0) {
					post("/activity/logOpenedActivity", params);
					lastTime = System.currentTimeMillis() / 1000.0;
		    	} else {
					double currTime = System.currentTimeMillis() / 1000.0;
					duration = (int) (currTime - lastTime);
					
					try {
						params.put("uptime", duration);
					} 
					catch (JSONException e) { e.printStackTrace();}
		    		post("/activity/logKeepAliveActivity", params);
		    	}
			}
		};
    	timer_.schedule(timerTask_, 2 * 1000,  15 * 1000); 
    }
    
    public static String post(String action, JSONObject jBody) {
		return post(action, jBody, SERVER_URL);
	}

	private static String post(String action, JSONObject jBody, String baseUrl) {
        URL url;
        try {
            url = new URL(baseUrl + action);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + baseUrl + action);
        }
        
        byte[] bBody = jBody.toString().getBytes();
        String body = Base64.encodeToString(bBody, Base64.DEFAULT);
        bBody = body.getBytes();
        
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bBody.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "text/plain");
            conn.setRequestProperty("Authorization","Basic " + Base64.encodeToString((IPHPusher.sharedInstance().get_mainKey() + ":" + IPHPusher.sharedInstance().get_secretKey()).getBytes(), Base64.NO_WRAP));

            OutputStream out = conn.getOutputStream();
            out.write(bBody);
            out.close();

            int status = conn.getResponseCode();
            if (status != 200) {
                return null;
            }
            return streamToString(conn.getInputStream());
            
        } catch (Exception e) {
        	e.printStackTrace();
        	//nothing
		} finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
		return null;
    }
    
    private static String streamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
    
    public static void stopLoginActivity(){
    	
    	if(ServerUtilities.getNotificationId()==null 
    			|| (timerTask_==null && timer_==null))
    		return;
    	
		timerTask_.cancel();
		timer_.cancel();
		timerTask_= null;
		timer_ = null;
    }

	public static void logActivity() {
		if(notificationId!=null){
			logActivity(notificationId);
		}
	}
	
	public static void setNotificationId(String notificationId){
		ServerUtilities.notificationId = notificationId;
	}
	
	public static String getNotificationId(){
		return ServerUtilities.notificationId;
	}

}
