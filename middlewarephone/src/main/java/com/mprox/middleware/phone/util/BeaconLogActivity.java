package com.mprox.middleware.phone.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Base64;

import com.estimote.sdk.Beacon;
import com.mprox.middleware.core.domain.Product;
import com.mprox.middleware.core.service.OpenAppTask;
import com.mprox.middleware.core.service.request.OpenAppRequest;
import com.mprox.middleware.core.service.responses.BeaconActivityResponse;
import com.mprox.middleware.core.util.ConstsService;

public final class BeaconLogActivity {

	private static Long trackId;
	protected static double lastTime = 0;
	protected static double duration = 0;
	protected static Timer timer_;
	protected static TimerTask timerTask_;

	@SuppressLint({ "DefaultLocale", "SimpleDateFormat" })
	private static void logOpenActivity(Product p, Beacon b, Context context) {

		String beaconId = b.getProximityUUID().toUpperCase() + "-"
				+ b.getMajor() + "-" + b.getMinor();

		Calendar c = Calendar.getInstance();
		Date today = c.getTime();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		String date = sdf.format(today).split(" ")[0];
		String time = sdf.format(today).split(" ")[1];

		OpenAppRequest request = new OpenAppRequest(beaconId, String.valueOf(p
				.getId()), date, time);
		OpenAppTask task = new OpenAppTask(context) {
			@Override
			public void onServiceReturned(BeaconActivityResponse response) {
				if (response.isSuccess()) {
					if (response.getData() != null) {
						trackId = response.getData().getId();
						logActivity();
					}
				}
			}
		};
		task.execute(request);
	}

	public static void logActivity() {

		if (trackId == null) {
			return;
		}

		BeaconLogActivity.stopLoginActivity();

		if (lastTime > 0)
			lastTime = (System.currentTimeMillis() / 1000.0) - duration;

		timer_ = new Timer();
		timerTask_ = new TimerTask() {
			@Override
			public void run() {
				JSONObject params = new JSONObject();

				if (lastTime == 0) {
					lastTime = System.currentTimeMillis() / 1000.0;
				} else {
					double currTime = System.currentTimeMillis() / 1000.0;
					duration = (int) (currTime - lastTime);

					try {
						params.put("trackId", trackId);
						params.put("uptime", duration);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					post("/activity/track/trackApp", params);
				}
			}
		};
		timer_.schedule(timerTask_, 2 * 1000, 15 * 1000);
	}

	public static void stopLoginActivity() {

		if (trackId == null || (timerTask_ == null && timer_ == null))
			return;

		timerTask_.cancel();
		timer_.cancel();
		timerTask_ = null;
		timer_ = null;
	}

	public static void logActivity(Product p, Beacon b, Context context) {
		lastTime = 0;
		duration = 0;
		logOpenActivity(p, b, context);
	}

	private static String post(String action, JSONObject jBody) {
		URL url;
		try {
			url = new URL(ConstsService.PROD_BASE_URL + action);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: "
					+ ConstsService.PROD_BASE_URL + action);
		}

		byte[] bBody = jBody.toString().getBytes();
		String body = Base64.encodeToString(bBody, Base64.DEFAULT);
		bBody = body.getBytes();

		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bBody.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "text/plain");
			conn.setRequestProperty( "Authorization", "Basic " + Base64.encodeToString(
					(ConstsService.PROD_MAIN_KEY + ":" + ConstsService.PROD_SECRET_KEY).getBytes(), Base64.NO_WRAP));

			OutputStream out = conn.getOutputStream();
			out.write(bBody);
			out.close();

			int status = conn.getResponseCode();
			if (status != 200) {
				return null;
			}
			return streamToString(conn.getInputStream());

		} catch (Exception e) {
			// nothing
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}

	private static String streamToString(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	public static void setTrackId(Long trackId) {
		BeaconLogActivity.trackId = trackId;
	}
	
}
