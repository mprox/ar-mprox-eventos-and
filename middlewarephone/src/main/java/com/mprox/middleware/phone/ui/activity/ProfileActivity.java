package com.mprox.middleware.phone.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.service.UpdateUserTask;
import com.mprox.middleware.core.service.request.RegisterRequest;
import com.mprox.middleware.core.service.responses.LoginResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.sdk.utils.UtilsMprox;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

public class ProfileActivity extends MiddlewareActivity {

	private static int RESULT_LOAD_IMAGE = 1;

	private RelativeLayout loadingLayout;
	private ImageView profileUserImage;
	private TextView profileName;
	private TextView profileEmail;

	private String profileImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		// Se pinta la pantalla.
		findAndInitViews();

	}

	@Override
	protected void findAndInitViews() {

		showProfileNavigationBarOptions();

		loadingLayout = (RelativeLayout) findViewById(R.id.loading_layout);

        profileUserImage = (ImageView) findViewById(R.id.profile_user_image);
        profileUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        if(UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserImage() != null &&
                UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserImage().length() > 0) {

            if (UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserImage().contains("/mproxeventos/usuarios/")) {
                String url2 = UtilsMiddleware.formatImageUrl(UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserImage());
                Picasso.with(this)
                        .load(url2)
                        .into(profileUserImage);
            } else {
                byte[] decodedString = Base64.decode(UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                profileUserImage.setImageBitmap(decodedByte);
            }
        }

		profileName = (TextView) findViewById(R.id.profile_name);
		profileName.setText(UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserName());
		profileName.setTypeface(MiddlewareApplication.getDinRegular());

		profileEmail = (TextView) findViewById(R.id.profile_email);
		profileEmail.setText(UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserEmail());
		profileEmail.setTypeface(MiddlewareApplication.getDinRegular());

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			cursor.close();

			decodeFile(selectedImage);
			UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserImage(picturePath);

			performRegister();
		}
	}

	private void performRegister() {
		findViewById(R.id.loading_view).setVisibility(View.VISIBLE);

		if (imageSelected != null) {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			imageSelected.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
			byte[] attachmentBytes = byteArrayOutputStream.toByteArray();

			profileImage = Base64.encodeToString(attachmentBytes, Base64.DEFAULT);
		}

		Bitmap userImage = BitmapFactory.decodeFile(UtilsMiddleware.getInstance(ProfileActivity.this).getLogedUserImage());
		if (userImage != null) {
			profileUserImage.setImageBitmap(userImage);
		}

		RegisterRequest request = new RegisterRequest(String.valueOf(UtilsMiddleware.getInstance(this).getLogedUserId()),
				null , null, null, null, null, UtilsMiddleware.getInstance(this).getLogedUserEmail(), profileImage);

		UpdateUserTask task = new UpdateUserTask(this) {
			@Override
			public void onServiceReturned(LoginResponse response) {
				findViewById(R.id.loading_view).setVisibility(View.GONE);
				if (response.isSuccess() && response.getData() != null) {

					UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserEmail(response.getData().getEmail());
					UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserName(response.getData().getName());
					UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserId(response.getData().getId().intValue());
                    UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserImage(response.getData().getImage());

                    UtilsMprox.getInstance(ProfileActivity.this).saveLogedUserEmail(response.getData().getEmail());
                    UtilsMprox.getInstance(ProfileActivity.this).saveLogedUserName(response.getData().getName());
                    UtilsMprox.getInstance(ProfileActivity.this).saveLogedUserId(String.valueOf(response.getData().getId()));

					Toast.makeText(ProfileActivity.this, "Se modifico su imagen de perfil.", Toast.LENGTH_SHORT).show();
				} else {
					String responseMsg = response.getErrorMsessage();
					if (UtilsMiddleware.isNullOrEmpty(responseMsg)) {
						responseMsg = getResources().getString(R.string.register_standar_error_msg);
					}

					Toast.makeText(ProfileActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
				}
			}
		};

		task.execute(request);
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent loginIntent = new Intent(ProfileActivity.this, HomeActivity.class);
		loginIntent.putExtra(LoginActivity.SHOW_NAV_BAR, true);
		startActivity(loginIntent);
		finish();
	}

	public void goToCloseSession(View view) {

		UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserEmail("");
		UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserName("");
		UtilsMiddleware.getInstance(ProfileActivity.this).saveLogedUserId(0);

		Intent loginIntent = new Intent(ProfileActivity.this, LoginActivity.class);
		loginIntent.putExtra(LoginActivity.SHOW_NAV_BAR, false);
		startActivity(loginIntent);
		finish();
	}

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = ((MiddlewareApplication)getApplication()).getDefaultTracker();
        tracker.setScreenName("Perfil");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}