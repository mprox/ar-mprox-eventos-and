package com.mprox.middleware.phone.ui.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Orador;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.DetailsAsistenteActivity;
import com.mprox.middleware.phone.ui.activity.DetailsOradorActivity;
import com.mprox.middleware.phone.util.adapters.AsistentesGridAdapter;

public class AsistentesPagerFragment extends Fragment {

	private final static String BENEFITLIST = "benefit_list";

	private ArrayList<User> usersList;
	private GridView grid;

	public static AsistentesPagerFragment newInstance(ArrayList<User> users) {
		AsistentesPagerFragment fragment = new AsistentesPagerFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelableArrayList(BENEFITLIST, users);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		usersList = getArguments().getParcelableArrayList(BENEFITLIST);
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		grid = (GridView) inflater.inflate(R.layout.fragment_benefits_list, null);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                openDetails(usersList.get(arg2));
            }
        });
		grid.setAdapter(new AsistentesGridAdapter(getActivity(), usersList));
		return grid;
	}

    private void openDetails(User asistente) {
        Intent intent = new Intent(getActivity(), DetailsAsistenteActivity.class);
        intent.putExtra(DetailsAsistenteActivity.ASISTENTE_TO_SHOW, asistente);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Tracker tracker = ((MiddlewareApplication)getActivity().getApplication()).getDefaultTracker();
        tracker.setScreenName("Asistentes");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}