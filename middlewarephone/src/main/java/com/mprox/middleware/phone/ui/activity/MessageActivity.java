package com.mprox.middleware.phone.ui.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.phone.application.MiddlewareApplication;

public class MessageActivity extends MiddlewareActivity {

	public static final String	MESSAGE	= "message";
	
	private String	message;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_screen_message);
		
		this.message = getIntent().getStringExtra(MESSAGE);
		
		findAndInitViews();
	}
	@Override
	protected void findAndInitViews() {
		hideNavigationBarOptions();

		Typeface dinRegular = MiddlewareApplication.getDinRegular();

		TextView msgTitle = (TextView) findViewById(R.id.msg_title);
		msgTitle.setText(message);
		msgTitle.setTypeface(dinRegular);

		ImageButton close = (ImageButton) findViewById(R.id.msg_close);
		close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

}
