package com.mprox.middleware.phone.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.core.domain.CalificacionActividad;
import com.mprox.middleware.core.service.RatingActividadTask;
import com.mprox.middleware.core.service.request.RateActividadRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.MiddlewareActivity;
import com.mprox.middleware.phone.util.EventManager;
import com.mprox.middleware.phone.util.UserPointsUtil;

@SuppressLint("InflateParams")
public class RatingActividadFragment extends Fragment {

	private static final String PRODUCT_ID = "product_id";
	private static final String CALIFICACION = "calificacion";
	private int starTouched = 0;
	private String observaciones = null;
    private Actividad actividad;
	private CalificacionActividad calificacionActividad;
    private LinearLayout starsContainer;
	private boolean yaVoto= false;

	public static RatingActividadFragment newInstance(Actividad actividad, CalificacionActividad calificacionActividad) {
		RatingActividadFragment fragment = new RatingActividadFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelable(PRODUCT_ID, actividad);
		args.putParcelable(CALIFICACION, calificacionActividad);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		actividad = getArguments().getParcelable(PRODUCT_ID);
		calificacionActividad = getArguments().getParcelable(CALIFICACION);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_rate_benefit, null);
		if(calificacionActividad.getId()!= null){
			yaVoto=true;
		}
		starsContainer = (LinearLayout) view.findViewById(R.id.stars_container);
//		Logica para completar las estrellas de la votacion
		if(yaVoto) {
			starTouched = calificacionActividad.getVoteTotal().intValue();
			for (int j = 0; j < starsContainer.getChildCount(); j++) {
				ImageButton child = (ImageButton) starsContainer.getChildAt(j);
				if (j < starTouched) {
					child.setImageResource(R.drawable.star_full_big);
				} else {
					child.setImageResource(R.drawable.star_empty_big);
				}
				child.setClickable(false);
				child.setEnabled(false);
			}
			starsContainer.setClickable(false);
			starsContainer.setEnabled(false);
		}

		final EditText observacionesEditText = (EditText) view.findViewById(R.id.observacionesEditText);
		observacionesEditText.setTypeface(MiddlewareApplication.getDinRegular());
		if(yaVoto) {
			observacionesEditText.setText(calificacionActividad.getObservacion());
			observacionesEditText.setClickable(false);
			observacionesEditText.setEnabled(false);
		}
		ImageButton close = (ImageButton) view.findViewById(R.id.qr_close);
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack();
			}
		});

		ImageButton accept = (ImageButton) view.findViewById(R.id.qr_accept);
		accept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (starTouched > 0) {
					if(observacionesEditText.getText().toString().length() > 0){
						observaciones = observacionesEditText.getText().toString();
					}
					rate(starTouched, observaciones);
				} else {
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.rating_required), Toast.LENGTH_LONG)
							.show();
				}
			}
		});
		if(yaVoto) {
			accept.setVisibility(View.GONE);
		}

		View.OnClickListener starOnClick = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				starTouched = Integer.parseInt(v.getTag().toString());
				for (int j = 0; j < starsContainer.getChildCount(); j++) {
					ImageButton child = (ImageButton) starsContainer.getChildAt(j);
					if (j < starTouched) {
						child.setImageResource(R.drawable.star_full_big);
					} else {
						child.setImageResource(R.drawable.star_empty_big);
					}
				}
			}
		};

		for (int i = 0; i < starsContainer.getChildCount(); i++) {
			starsContainer.getChildAt(i).setOnClickListener(starOnClick);
		}

		return view;
	}

	private void rate(final int rating, String observaciones) {
		RatingActividadTask task = new RatingActividadTask() {
			@Override
			public void onServiceReturned(BaseResponse response) {
				getView().findViewById(R.id.loading_view).setVisibility(View.GONE);

				if (response.isSuccess()) {
					UtilsMiddleware.getInstance(getActivity()).addRatedProduct(String.valueOf(actividad.getId()));

					UserPointsUtil.assignPointsWinning(((MiddlewareActivity) getActivity()),
							getResources().getString(R.string.actions_points_rate_benefit), "Idproduct:" + actividad, 10);

					Toast.makeText(getActivity(), R.string.rating_saved, Toast.LENGTH_SHORT).show();
                    EventManager.getInstance().notify("setActividadRanking", rating);
                    getFragmentManager().popBackStack();
				} else {
                    if(response.getErrorTrace() != null && response.getErrorTrace().contains("ConstraintViolationException")){
                        Toast.makeText(getActivity(), R.string.rating_duplicated_activity, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), R.string.rating_error, Toast.LENGTH_SHORT).show();
                    }
				}
			}
		};

		long userId = Long.parseLong(String.valueOf(UtilsMiddleware.getInstance(getActivity()).getLogedUserId()));
		RateActividadRequest request = new RateActividadRequest(String.valueOf(actividad.getId()), rating, userId, observaciones);
		task.execute(request);
		getView().findViewById(R.id.loading_view).setVisibility(View.VISIBLE);
	}
}