package com.mprox.middleware.phone.ui.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mprox.middleware.R;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.util.BeaconLogActivity;

/**
 * @author Leo
 * 
 */
public abstract class MiddlewareActivity extends FragmentActivity {
	protected Bitmap imageSelected;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();
		UtilsMiddleware.getInstance(this).addSubscribedActivity();
	}

	@Override
	protected void onPause() {
		BeaconLogActivity.stopLoginActivity();
		MiddlewareApplication.setInForeground(false);

		super.onPause();
	}

	@Override
	protected void onResume() {
		BeaconLogActivity.logActivity();
		
		MiddlewareApplication.setInForeground(true);

		super.onResume();
		MiddlewareApplication.getAppContext().setCurrentctivity(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		UtilsMiddleware.getInstance(this).removeSubscribedActivity();
	}

	/**
	 * Esconde el teclado virtual cunado se hace click fuera de el y de un
	 * EditTExt
	 */
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];
			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	/**
	 * Find and initialize content's views.
	 */
	protected abstract void findAndInitViews();
	
	public void hideNavigationBarOptions() {
		ImageButton profileBtn = (ImageButton) findViewById(R.id.btn_profile);

		profileBtn.setVisibility(View.GONE);
	}
	
	public void showNavigationBarOptions() {
		ImageButton profileBtn = (ImageButton) findViewById(R.id.btn_profile);
		profileBtn.setVisibility(View.VISIBLE);
	}
	
	public void showAddProductNavigationBarOption() {
		ImageButton profileBtn = (ImageButton) findViewById(R.id.btn_profile);
		profileBtn.setVisibility(View.GONE);
	}
	
	public void showProfileNavigationBarOptions() {
		ImageButton profileBtn = (ImageButton) findViewById(R.id.btn_profile);

		profileBtn.setVisibility(View.GONE);
	}


	public void decodeFile(Uri fileUri) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(getContentResolver().openInputStream(fileUri), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = 512;

			// Find the correct scale value. It should be the power of 2.
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			imageSelected = BitmapFactory.decodeStream(getContentResolver().openInputStream(fileUri), null, o2);
		}catch (Exception e){}
	}
}
