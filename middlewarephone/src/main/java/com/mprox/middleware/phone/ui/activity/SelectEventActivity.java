package com.mprox.middleware.phone.ui.activity;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Evento;
import com.mprox.middleware.core.service.GetEventosTask;
import com.mprox.middleware.core.service.LoginTask;
import com.mprox.middleware.core.service.request.LoginRequest;
import com.mprox.middleware.core.service.responses.GetEventosResponse;
import com.mprox.middleware.core.service.responses.LoginResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.fragments.EventoPagerFragment;
import com.mprox.middleware.phone.ui.fragments.AsistentesPagerFragment;
import com.mprox.middleware.phone.ui.fragments.EncuestasPagerFragment;
import com.mprox.middleware.phone.ui.fragments.OradoresPagerFragment;
import com.mprox.middleware.phone.util.adapters.EventoGridAdapter;
import com.mprox.middleware.phone.util.pusher.ServerUtilities;
import com.mprox.sdk.utils.UtilsMprox;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectEventActivity extends MiddlewareActivity {

	private static final int MODE_LOGIN = 0;
	private static final int MODE_REGISTER = 1;
	private static final int MODE_LOGOUT = 2;

	private SelectEventActivity.ScreenSlidePagerAdapter eventoAdapter;
	private ViewPager mPager;

	public static final String SHOW_HOME = "showHome";
	public static final String SHOW_NAV_BAR = "showNavBar";
	public static final String SEND_CHECKIN = "sendCheckin";

	private int mode = MODE_LOGIN;

	private boolean showHome;
	private boolean showNavBar;
	private boolean sendCheckin;

	private ArrayList<Evento> eventosList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_select_event);

		showHome = getIntent().getBooleanExtra(SHOW_HOME, true);
		showNavBar = getIntent().getBooleanExtra(SHOW_NAV_BAR, false);
		sendCheckin = getIntent().getBooleanExtra(SEND_CHECKIN, false);
		getEventosService();
		findAndInitViews();
	}

	@Override
	protected void findAndInitViews() {
//		loginBtn = (Button) findViewById(R.id.btn_login);
//		loginFields = (LinearLayout) findViewById(R.id.login_fields);
//
//		EditText nameInput = (EditText) findViewById(R.id.name_imput);
//		EditText passInput = (EditText) findViewById(R.id.password_input);
//		nameInput.addTextChangedListener(watcher);
//		passInput.addTextChangedListener(watcher);
//		loginBtn.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				loginOnClick();
//			}
//		});
		ListView grid;
		EventoGridAdapter adapter;
		mPager = (ViewPager) findViewById(R.id.benefits_pager);
//		grid = (ListView) inflater.inflate(R.layout.fragment_agenda_list, null);
//		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//				openAgendaDetails(benefitsList.get(arg2));
//			}
//		});
//
////		adapter = new ActividadGridAdapter(getActivity(), benefitsList);
////		grid.setAdapter(adapter);
//		grid.setAdapter(eventoAdapter);
	}

	@Override
	public void onBackPressed() {
		if (mode == MODE_REGISTER) {
//			setLoginView();
		} else if (showHome && !showNavBar) {
			exitPopUp();
		} else {
			super.onBackPressed();
		}
	}

	private void exitPopUp() {
		Builder builder = new Builder(this);
		builder.setMessage(R.string._est_seguro_de_querer_salir_de_la_aplicaci_n_);
		builder.setNegativeButton("No", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.setPositiveButton("Si", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});

		builder.create().show();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 104 && resultCode == RESULT_OK) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				cursor.close();

				decodeFile(selectedImage);
				UtilsMiddleware.getInstance(SelectEventActivity.this).saveLogedUserImage(picturePath);
		}
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return EventoPagerFragment.newInstance(eventosList);
		}

		@Override
		public int getItemPosition(Object item) {
			return POSITION_NONE;
		}

		@Override
		public int getCount() {
			return 4;
		}
	}

	private void getEventosService() {
		GetEventosTask task = new GetEventosTask(this) {
			@Override
			public void onServiceReturned(GetEventosResponse response) {
				if (response != null && response.getData() != null) {
					eventosList.clear();
					eventosList.addAll(response.getData());
				}
				if (eventoAdapter != null) {
					eventoAdapter.notifyDataSetChanged();
				} else {
					eventoAdapter = new SelectEventActivity.ScreenSlidePagerAdapter(getSupportFragmentManager());
					mPager.setAdapter(eventoAdapter);
				}
			}
		};
		findViewById(R.id.loading_view).setVisibility(View.VISIBLE);
		task.execute();
	}
}