package com.mprox.middleware.phone.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.ConsultaActividad;
import com.mprox.middleware.core.service.GetAgendaCuestionariosTask;
import com.mprox.middleware.core.service.SendConsultaTask;
import com.mprox.middleware.core.service.request.ActividadRequest;
import com.mprox.middleware.core.service.request.SendConsultaRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.service.responses.GetConsultasResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.List;

public class WriteConsulta extends LinearLayout {

    private Context mContext;
    private EditText consultaText;
    private String actividadId;
    private Button sendButton;

    private boolean sended = false;

	public WriteConsulta(Context context, long idActividad) {
		super(context);

        mContext = context;
        this.actividadId = String.valueOf(idActividad);
		LayoutInflater.from(context).inflate(R.layout.write_consulta, this);

        consultaText = (EditText) findViewById(R.id.consultaText);

        sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sended) {
                    SendConsultaTask task = new SendConsultaTask(mContext) {
                        @Override
                        public void onServiceReturned(BaseResponse response) {
                            if (response != null) {
                                Toast.makeText(mContext, "Su consulta se envio con exito.", Toast.LENGTH_SHORT).show();
                                sendButton.setBackgroundColor(getResources().getColor(R.color.hintGray));
                                consultaText.setTextColor(getResources().getColor(R.color.hintGray));
                                consultaText.setKeyListener(null);
                                sended = true;
                            } else {
                                Toast.makeText(mContext, "Imposible enviar su consulta, intente nuevamente más tarde.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    };

                    long userId = Long.parseLong(String.valueOf(UtilsMiddleware.getInstance(mContext).getLogedUserId()));
                    SendConsultaRequest request = new SendConsultaRequest(String.valueOf(userId),
                            actividadId, consultaText.getText().toString());
                    task.execute(request);
                }
            }
        });

	}



}
