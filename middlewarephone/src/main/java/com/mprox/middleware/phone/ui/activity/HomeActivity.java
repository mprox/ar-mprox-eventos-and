package com.mprox.middleware.phone.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.service.GetAgendaTask;
import com.mprox.middleware.core.service.GetAsistentesTask;
import com.mprox.middleware.core.service.GetEncuestasTask;
import com.mprox.middleware.core.service.GetOradoresTask;
import com.mprox.middleware.core.service.request.GetActividadRequest;
import com.mprox.middleware.core.service.request.GetByUserIdRequest;
import com.mprox.middleware.core.service.responses.GetAgendaResponse;
import com.mprox.middleware.core.service.responses.GetEncuestaResponse;
import com.mprox.middleware.core.service.responses.GetOradoresResponse;
import com.mprox.middleware.core.service.responses.GetUserResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.fragments.ActividadPagerFragment;
import com.mprox.middleware.phone.ui.fragments.AsistentesPagerFragment;
import com.mprox.middleware.phone.ui.fragments.EncuestasPagerFragment;
import com.mprox.middleware.phone.ui.fragments.OradoresPagerFragment;

import java.util.ArrayList;

public class HomeActivity extends MiddlewareActivity {

	public static final String PRODUCTS_LIST = "productsList";
    public static final String MESSAGE = "message";

	private ViewPager mPager;
	private ArrayList<String> optionsList;
	private static int pageNumber;

	private ArrayList<Actividad> agendaList;
	private ArrayList<User> oradoresList;

	private TextView selectedCategory;

	private TextView option_agenda;
	private TextView option_oradores;
	private TextView option_encuestas;
    private TextView option_asistentes;

	private ScreenSlidePagerAdapter agendaAdapter;
	private ScreenSlidePagerAdapter oradoresAdapter;
	private ScreenSlidePagerAdapter asistentesAdapter;
    private ScreenSlidePagerAdapter encuestasAdapter;

	private boolean isAlive = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		agendaList = new ArrayList<Actividad>();
		oradoresList = new ArrayList<User>();

		pageNumber = 0;
        getAsistentesService();
		findAndInitViews();
	}

	@Override
	protected void findAndInitViews() {

		showNavigationBarOptions();
        findViewById(R.id.loading_view).setVisibility(View.VISIBLE);

		Typeface dinRegular = MiddlewareApplication.getDinRegular();

		mPager = (ViewPager) findViewById(R.id.benefits_pager);
		optionsList = new ArrayList<String>();

		View.OnClickListener categoryOnClick = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView tag = (TextView) v;
				scrollToCategory(tag);
			}
		};

		option_agenda = (TextView) findViewById(R.id.option_agenda);
		option_agenda.setTypeface(dinRegular);
		option_agenda.setOnClickListener(categoryOnClick);
		optionsList.add(option_agenda.getText().toString());

		option_oradores = (TextView) findViewById(R.id.option_oradores);
		option_oradores.setTypeface(dinRegular);
		option_oradores.setOnClickListener(categoryOnClick);
		optionsList.add(option_oradores.getText().toString());

		option_asistentes = (TextView) findViewById(R.id.option_asistentes);
		option_asistentes.setTypeface(dinRegular);
		option_asistentes.setOnClickListener(categoryOnClick);
		optionsList.add(option_asistentes.getText().toString());

        option_encuestas = (TextView) findViewById(R.id.option_encuestas);
		option_encuestas.setTypeface(dinRegular);
        option_encuestas.setOnClickListener(categoryOnClick);
        optionsList.add(option_encuestas.getText().toString());

		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				pageNumber = arg0;
				if (pageNumber == 0) {
					updateCategoriesScroll(option_agenda);
				} else if (pageNumber == 1) {
					updateCategoriesScroll(option_oradores);
				} else if (pageNumber == 2) {
					updateCategoriesScroll(option_asistentes);
				} else {
					updateCategoriesScroll(option_encuestas);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

        if (getIntent()!= null && getIntent().getStringExtra(MESSAGE) != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getIntent().getStringExtra(MESSAGE));
            builder.setCancelable(false).setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    return;
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

	}

	@Override
	protected void onStart() {
		super.onStart();
		isAlive = true;
	}

	@Override
	protected void onStop() {
		super.onStop();
		isAlive = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		switch (pageNumber) {
		case 0:
			option_agenda.performClick();
			break;
		case 1:
			option_oradores.performClick();
			break;
		case 2:
			option_asistentes.performClick();
			break;
        case 3:
            option_encuestas.performClick();
            break;
        }
		mPager.setCurrentItem(pageNumber);
        getAgendaService();
	}

	private void getAgendaService() {
		GetAgendaTask task = new GetAgendaTask(HomeActivity.this) {
			@Override
			public void onServiceReturned(GetAgendaResponse response) {
				if (!isAlive) {
					return;
				}

				if (response != null && response.getData() != null) {
					agendaList.clear();
					agendaList.addAll(response.getData());

					if (agendaAdapter != null) {
						agendaAdapter.notifyDataSetChanged();
					} else {
						agendaAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
						mPager.setAdapter(agendaAdapter);
					}
				}
				getOradorService();
			}
		};
		GetActividadRequest request =
				new GetActividadRequest(UtilsMiddleware.getInstance(HomeActivity.this).getLogedUserId());
		task.execute(request);
	}

	private void getOradorService() {
		GetOradoresTask task = new GetOradoresTask(HomeActivity.this) {
			@Override
			public void onServiceReturned(GetOradoresResponse response) {
				if (!isAlive) {
					return;
				}

				if (response != null && response.getData() != null) {
					oradoresList.clear();
					oradoresList.addAll(response.getData());

					if (oradoresAdapter != null) {
						oradoresAdapter.notifyDataSetChanged();
					} else {
						oradoresAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
						mPager.setAdapter(oradoresAdapter);
					}
				}
                findViewById(R.id.loading_view).setVisibility(View.GONE);
			}
		};
		task.execute();
	}

	private void getAsistentesService() {
		GetAsistentesTask task = new GetAsistentesTask(HomeActivity.this) {
			@Override
			public void onServiceReturned(GetUserResponse response) {
				if (!isAlive) {
					return;
				}
				if (response.getData() != null && response.getData().size() > 0) {
					MiddlewareApplication.getAppContext().getAsistentesList().clear();
                    MiddlewareApplication.getAppContext().getAsistentesList().addAll(response.getData());
				}

				if (asistentesAdapter != null) {
					asistentesAdapter.notifyDataSetChanged();
				} else {
					asistentesAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
					mPager.setAdapter(asistentesAdapter);
				}

                getEncuestasService();
			}
		};

		Long appId = Long.parseLong(String.valueOf(MiddlewareApplication.getAppId()));
		GetByUserIdRequest request = new GetByUserIdRequest(appId);
		task.execute(request);
	}

    private void getEncuestasService() {
        GetEncuestasTask task = new GetEncuestasTask(HomeActivity.this) {
            @Override
            public void onServiceReturned(GetEncuestaResponse response) {
                if (!isAlive) {
                    return;
                }
                if (response.getData() != null && response.getData().size() > 0) {
                    MiddlewareApplication.getAppContext().getEncuestasList().clear();
                    MiddlewareApplication.getAppContext().getEncuestasList().addAll(response.getData());
                }

                if (encuestasAdapter != null) {
                    encuestasAdapter.notifyDataSetChanged();
                } else {
                    encuestasAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
                    mPager.setAdapter(encuestasAdapter);
                }

                mPager.setCurrentItem(pageNumber);
            }
        };
        task.execute();
    }

	private void updateCategoriesScroll(TextView categoryView) {
		if (categoryView.equals(selectedCategory)) {
			return;
		}

		categoryView.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
				getResources().getDrawable(R.drawable.white_dot));

		if (selectedCategory != null) {
			selectedCategory.setCompoundDrawables(null, null, null, null);
		}
		selectedCategory = categoryView;
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return ActividadPagerFragment.newInstance(agendaList);
			case 1:
                return OradoresPagerFragment.newInstance(oradoresList);
			case 2:
                return AsistentesPagerFragment.newInstance(MiddlewareApplication.getAppContext().getAsistentesList());
			default:
                return EncuestasPagerFragment.newInstance(MiddlewareApplication.getAppContext().getEncuestasList());
			}
		}

        @Override
        public int getItemPosition(Object item) {
            return POSITION_NONE;
        }

		@Override
		public int getCount() {
			return 4;
		}
    }

	private void scrollToCategory(TextView categoryView) {
		updateCategoriesScroll(categoryView);
		int page = optionsList.indexOf(categoryView.getText().toString());
		if (page >= 0) {
			mPager.setCurrentItem(page, true);
		}
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.exit);
		builder.setCancelable(false).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				return;
			}
		});
		builder.setCancelable(false).setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}