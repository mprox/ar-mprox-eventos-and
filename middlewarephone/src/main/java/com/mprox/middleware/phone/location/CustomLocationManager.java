package com.mprox.middleware.phone.location;

import java.util.Date;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.mprox.middleware.phone.application.MiddlewareApplication;

public class CustomLocationManager implements LocationListener {

	private static final String	LOG_TAG	= "mprox location";

	//5 minutos de ventana, antes de expirar.
	private static final int LOCATION_EXPIRATION_TIME = 1000 * 60 * 5;

	private static CustomLocationManager shareInstance;
	
    private LocationManagerDelegate delegate;
    private Location lastReadLocation;
    
    //#define DEFAULT_COORDS mprox headquarters
    public static double DEFAULT_LAT = -34.589918;
    public static double DEFAULT_LONG = -58.440510;
    
    // additional data that delegate could need
    private Object data;

    public CustomLocationManager() {
    	this.startLocationListeners();
    }

    private void retrieveCurrentLocation(boolean showMessageIfGPSIsDisabled) {
    	//check if we have to get a new location.
    	if (this.lastReadLocation == null
    			|| (new Date().getTime() - this.lastReadLocation.getTime()) > LOCATION_EXPIRATION_TIME) {
    		if (this.isGPSEnabled() && this.lastReadLocation != null) {
    			Log.i(LOG_TAG, "LocationManager - retrieveCurrentLocation: start listening to a new location.");
    			this.startLocationListeners();
    		} else {
    			Log.i(LOG_TAG, "LocationManager - retrieveCurrentLocation: return default location.");
    			Coordinate2D newCoordinate = this.getLastReadLocation(showMessageIfGPSIsDisabled);

    			this.notifyToDelegate(newCoordinate.latitude, newCoordinate.longitude);
    		}

    		return;
    	}

    	Log.i(LOG_TAG, "LocationManager - retrieveCurrentLocation: we already have a valid coordinate.");
		this.notifyToDelegate(this.lastReadLocation.getLatitude(), this.lastReadLocation.getLongitude());
    }

    private boolean isGPSEnabled() {
    	LocationManager locationManager = (LocationManager)MiddlewareApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);

    	return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    			|| locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    private void startLocationListeners() {
    	LocationManager locationManager = (LocationManager)MiddlewareApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
    }

    private void notifyToDelegate(double theLatitude, double theLongitude) {
		if (delegate != null)
			delegate.locationManagerFinshedWithCoords(this, theLongitude, theLatitude);

		//reset the delegate after the notification.
		delegate = null;
    }

	public Object getData() {
		return data;
	}

	public static void startRetrievingCurrentLocationWithDelegate(LocationManagerDelegate theDelegate, Object data) {
		CustomLocationManager currentManager = getLocationManager();
		currentManager.delegate = theDelegate;
		currentManager.data = data;

		currentManager.retrieveCurrentLocation(true);		
	}

	public Coordinate2D getLastReadLocation(boolean showMessageIfGPSIsDisabled) {
		if (this.lastReadLocation != null)
			return new Coordinate2D(this.lastReadLocation.getLatitude(), this.lastReadLocation.getLongitude());

		if (showMessageIfGPSIsDisabled)
			Toast.makeText(MiddlewareApplication.getAppContext(), "gps deshabilitado",//R.string.location_gps_deshabilitado,
	                Toast.LENGTH_LONG).show();

		return new Coordinate2D(DEFAULT_LAT, DEFAULT_LONG);
	}

	public void onLocationChanged(Location arg0) {
		Log.i(LOG_TAG, "LocationManager - onLocationChanged: provider: "+ arg0.getProvider() 
				+" - latitude: "+ arg0.getLatitude() +" - longitude: "+ arg0.getLongitude());

		lastReadLocation = arg0;

		this.notifyToDelegate(lastReadLocation.getLatitude(), lastReadLocation.getLongitude());

    	LocationManager locationManager = (LocationManager)MiddlewareApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
		locationManager.removeUpdates(this);
	}

	public void onProviderDisabled(String arg0) {
		Log.i(LOG_TAG, "LocationManager - GPS disabled");
	}

	public void onProviderEnabled(String arg0) {
		Log.i(LOG_TAG, "LocationManager - GPS enabled");
	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}

	public static Location createLocation(double latitude, double longitude) {
		Location newLocation = new Location(LocationManager.GPS_PROVIDER);
		newLocation.setLatitude(latitude);
		newLocation.setLongitude(longitude);

		return newLocation;
	}

	public static void setup() {
		// saving the phone gps position for the first time.
		getLocationManager();
	}

	public static CustomLocationManager getLocationManager() {
		//create the singleton.
		if (shareInstance == null)
			shareInstance = new CustomLocationManager();

		return shareInstance;
	}
}
