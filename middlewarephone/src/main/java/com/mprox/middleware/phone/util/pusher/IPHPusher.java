package com.mprox.middleware.phone.util.pusher;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.android.gcm.GCMRegistrar;
import com.mprox.middleware.phone.ui.activity.ShowNotificationActivity;

public class IPHPusher {
	
	private static IPHPusher _sharedInstance;
	private boolean _isVisible = false;
	private Context _context = null;
	private String _mainKey = "";
	private String _secretKey = "";
	private String _senderId = "";

	public Context get_context() {
		return _context;
	}
	
	public String get_mainKey() {
		return _mainKey;
	}

	public String get_secretKey() {
		return _secretKey;
	}

	public String get_senderId() {
		return _senderId;
	}
	
	public boolean is_isVisible() {
		return _isVisible;
	}
	
	static public IPHPusher sharedInstance(){
		if (_sharedInstance == null)
			_sharedInstance = new IPHPusher();
		
		return _sharedInstance;
	}
	
	public void init(Context context, String mainKey, String secretKey, String senderId)
	{
		try
		{
			_context = context;
			_mainKey = mainKey;
			_secretKey = secretKey;
			_senderId = senderId;
			
			context.registerReceiver(mHandleMessageReceiver, new IntentFilter("com.mprox.middleware.DISPLAY_MESSAGE"));
			
			GCMRegistrar.checkDevice(context);
			GCMRegistrar.checkManifest(context);
			
			final String regId = GCMRegistrar.getRegistrationId(context);
			   
			if (regId.equals("")) {
				GCMRegistrar.register(context, _senderId);
			} else {
				ServerUtilities.register(_context, regId);
			}			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {				
				
	    	Intent i = new Intent(context, ShowNotificationActivity.class);
	    	i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
	    	i.putExtra("message", intent.getStringExtra("message"));
	        context.startActivity(i);
	    	
	        ServerUtilities.setNotificationId(intent.getStringExtra("notificationId"));
			ServerUtilities.logActivity();
		}
	};
	
	public void onStart() {
		_isVisible = true;
	}
	
	public void onStop() {
		_isVisible = false;
	}
}
