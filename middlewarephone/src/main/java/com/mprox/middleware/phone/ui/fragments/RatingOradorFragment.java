package com.mprox.middleware.phone.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Orador;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.service.RatingOradorTask;
import com.mprox.middleware.core.service.request.RateOradorRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.DetailsOradorActivity;
import com.mprox.middleware.phone.util.EventManager;

@SuppressLint("InflateParams")
public class RatingOradorFragment extends Fragment {

	private static final String USER = "user";
	private int starTouched = 0;
	private String observaciones = null;
    private User user;
    private LinearLayout starsContainer;

	public static RatingOradorFragment newInstance(User user) {
		RatingOradorFragment fragment = new RatingOradorFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelable(USER, user);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		user = getArguments().getParcelable(USER);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_rate_benefit, null);

		starsContainer = (LinearLayout) view.findViewById(R.id.stars_container);

		final EditText observacionesEditText = (EditText) view.findViewById(R.id.observacionesEditText);
		observacionesEditText.setTypeface(MiddlewareApplication.getDinRegular());

		ImageButton close = (ImageButton) view.findViewById(R.id.qr_close);
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack();
			}
		});

		ImageButton accept = (ImageButton) view.findViewById(R.id.qr_accept);
		accept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (starTouched > 0) {
					if(observacionesEditText.getText().toString().length() > 0){
						observaciones = observacionesEditText.getText().toString();
					}
					rate(starTouched, observaciones);
				} else {
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.rating_required), Toast.LENGTH_LONG)
							.show();
				}
			}
		});

		View.OnClickListener starOnClick = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				starTouched = Integer.parseInt(v.getTag().toString());
				for (int j = 0; j < starsContainer.getChildCount(); j++) {
					ImageButton child = (ImageButton) starsContainer.getChildAt(j);
					if (j < starTouched) {
						child.setImageResource(R.drawable.star_full_big);
					} else {
						child.setImageResource(R.drawable.star_empty_big);
					}
				}
			}
		};

		for (int i = 0; i < starsContainer.getChildCount(); i++) {
			starsContainer.getChildAt(i).setOnClickListener(starOnClick);
		}

		return view;
	}

	private void rate(final int rating, String observaciones) {
		RatingOradorTask task = new RatingOradorTask() {
			@Override
			public void onServiceReturned(BaseResponse response) {
				getView().findViewById(R.id.loading_view).setVisibility(View.GONE);

				if (response.isSuccess()) {
					UtilsMiddleware.getInstance(getActivity()).addRatedProduct(String.valueOf(user.getId()));

					Toast.makeText(getActivity(), R.string.rating_saved, Toast.LENGTH_SHORT).show();
                    EventManager.getInstance().notify("setUserRanking", rating);
                    getFragmentManager().popBackStack();
				} else {
                    if(response.getErrorTrace() != null && response.getErrorTrace().contains("ConstraintViolationException")){
                        Toast.makeText(getActivity(), R.string.rating_duplicated_orador, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), R.string.rating_error, Toast.LENGTH_SHORT).show();
                    }
				}
			}
		};

		long userId = Long.parseLong(String.valueOf(UtilsMiddleware.getInstance(getActivity()).getLogedUserId()));
		RateOradorRequest request = new RateOradorRequest(String.valueOf(user.getId()), rating, userId, observaciones);
		task.execute(request);
		getView().findViewById(R.id.loading_view).setVisibility(View.VISIBLE);
	}
}