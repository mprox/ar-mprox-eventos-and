package com.mprox.middleware.phone.ui.activity;

import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Orador;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.fragments.RatingOradorFragment;
import com.mprox.middleware.phone.util.EventManager;
import com.squareup.picasso.Picasso;

public class DetailsOradorActivity extends MiddlewareActivity{

	public final static String ORADOR_TO_SHOW = "orador_to_show";
	private User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		user = getIntent().getExtras().getParcelable(ORADOR_TO_SHOW);

		setContentView(R.layout.activity_orador_details);

        EventManager.getInstance().addObserver(this, "setOradorRanking");

		findAndInitViews();
	}

    public void onSetOradorRanking(Object object){
        int rating = (int) object;
		user.setVoteQuantity(user.getVoteQuantity() + 1f);
		user.setVoteTotal((user.getVoteTotal() + rating)/ 2);
        findAndInitViews();
    }

    @Override
    protected void onDestroy() {
        EventManager.getInstance().removeObserver(this, "setActividadRanking");
        super.onDestroy();
    }

	@Override
	protected void findAndInitViews() {
		hideNavigationBarOptions();

		TextView title = (TextView) findViewById(R.id.benefit_name);
		title.setText(user.getName());
		title.setTypeface(MiddlewareApplication.getDinBold());

		TextView description = (TextView) findViewById(R.id.benefit_description_text);
		description.setText(user.getDescripcion());
		description.setTypeface(MiddlewareApplication.getDinRegular());

		ImageView benefitImage = (ImageView) findViewById(R.id.benefit_image);

        if(user.getImagen() != null) {
            if (user.getImagen().contains("/mproxeventos/usuarios/")) {
                String url2 = UtilsMiddleware.formatImageUrl(user.getImagen());
                Picasso.with(DetailsOradorActivity.this)
                        .load(url2)
                        .into(benefitImage);
            } else {
                benefitImage.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(user.getImagen(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                benefitImage.setImageBitmap(decodedByte);
            }
        }

		RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar) ;
		ratingBar.setRating(user.getVoteTotal());

		TextView ratingVotes = (TextView) findViewById(R.id.benefit_rating_votes);
		ratingVotes.setText(user.getVoteQuantity().intValue() + " Votos");
		ratingVotes.setTypeface(MiddlewareApplication.getDinRegular());

		LinearLayout ratingContainer = (LinearLayout) findViewById(R.id.rating_caontainer);
		ratingContainer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.main_frame, RatingOradorFragment.newInstance(user));
				transaction.addToBackStack("Rating");
				transaction.commit();
			}
		});

	}

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = ((MiddlewareApplication)getApplication()).getDefaultTracker();
        tracker.setScreenName("OradoresDetalle");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}