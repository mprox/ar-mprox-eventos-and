package com.mprox.middleware.phone.ui.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.core.domain.CalificacionActividad;
import com.mprox.middleware.core.domain.ConsultaActividad;
import com.mprox.middleware.core.service.GetAgendaCalificacionesTask;
import com.mprox.middleware.core.service.GetAgendaCuestionariosTask;
import com.mprox.middleware.core.service.request.ActividadRequest;
import com.mprox.middleware.core.service.responses.GetCalificacionesResponse;
import com.mprox.middleware.core.service.responses.GetConsultasResponse;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.fragments.RatingActividadFragment;
import com.mprox.middleware.phone.ui.view.CalificacionListItem;
import com.mprox.middleware.phone.ui.view.ConsultasListItem;
import com.mprox.middleware.phone.ui.view.WriteConsulta;
import com.mprox.middleware.phone.util.EventManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailsActividadActivity extends MiddlewareActivity {

	public final static String ACTIVITY_TO_SHOW = "activity_to_show";
	private Actividad actividad;
    private CalificacionActividad caliAc;
    private List<CalificacionActividad> calificaciones;

	private LinearLayout questionsContent;
	private LinearLayout calificationContent;
	private LinearLayout descriptionContent;

    private TextView noQuestionsData;
    private TextView noCalificationsData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		actividad = getIntent().getExtras().getParcelable(ACTIVITY_TO_SHOW);

		setContentView(R.layout.activity_agenda_details);

        descriptionContent = (LinearLayout) findViewById(R.id.descriptionContent);
        calificationContent = (LinearLayout) findViewById(R.id.calificationContent);
        questionsContent = (LinearLayout) findViewById(R.id.questionsContent);

        noQuestionsData = (TextView) findViewById(R.id.noQuestionsData);
        noQuestionsData.setTypeface(MiddlewareApplication.getDinRegular());

        noCalificationsData = (TextView) findViewById(R.id.noCalificationsData);
        noCalificationsData.setTypeface(MiddlewareApplication.getDinRegular());

        EventManager.getInstance().addObserver(this, "setActividadRanking");

        getCalificaciones();
        getPreguntas();
		findAndInitViews();
	}

    public void onSetActividadRanking(Object object){
        int rating = (int) object;
        actividad.setVoteQuantity(actividad.getVoteQuantity() + 1f);
        actividad.setVoteTotal((actividad.getVoteTotal() + rating)/ 2);
        getCalificaciones();
        findAndInitViews();
    }

    @Override
    protected void onDestroy() {
        EventManager.getInstance().removeObserver(this, "setActividadRanking");
        super.onDestroy();
    }

    @Override
	protected void findAndInitViews() {
		hideNavigationBarOptions();

		TextView title = (TextView) findViewById(R.id.benefit_name);
		title.setText(actividad.getNombre());
        title.setTypeface(MiddlewareApplication.getDinBold());

		ImageView benefitImage = (ImageView) findViewById(R.id.benefit_image);
		if (actividad.getImagen() != null && actividad.getImagen().length() != 0) {
			String url = UtilsMiddleware.formatImageUrl(actividad.getImagen());
			Picasso.with(DetailsActividadActivity.this).load(url).into(benefitImage);
		}

		RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
		ratingBar.setRating(actividad.getVoteTotal());

		TextView ratingVotes = (TextView) findViewById(R.id.benefit_rating_votes);
		ratingVotes.setText(actividad.getVoteQuantity().intValue() + " Votos");
        ratingVotes.setTypeface(MiddlewareApplication.getDinRegular());

		LinearLayout ratingContainer = (LinearLayout) findViewById(R.id.rating_caontainer);
		ratingContainer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

                caliAc = hizoVotacion(calificaciones);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.main_frame, RatingActividadFragment.newInstance(actividad,caliAc));
				transaction.addToBackStack("Rating");
				transaction.commit();
			}
		});



		TextView labelDate = (TextView) findViewById(R.id.labelDate);
		labelDate.setText(actividad.getFechaStart());
        labelDate.setTypeface(MiddlewareApplication.getDinRegular());

		TextView labelTime = (TextView) findViewById(R.id.labelTime);
		labelTime.setText(actividad.getHoraStart() + " hs. a " + actividad.getHoraEnd() + " hs.");
        labelTime.setTypeface(MiddlewareApplication.getDinRegular());

        TextView labelOrador = (TextView) findViewById(R.id.labelOrador);
        labelOrador.setText("Orador: " + actividad.getUser().getName());
        labelOrador.setTypeface(MiddlewareApplication.getDinRegular());

		TextView description = (TextView) findViewById(R.id.benefit_description_text);
		description.setText(actividad.getDescripcion());
        description.setTypeface(MiddlewareApplication.getDinBold());

        TextView contenido = (TextView) findViewById(R.id.benefit_contenido_text);
        contenido.setText(actividad.getContenido());
        contenido.setTypeface(MiddlewareApplication.getDinRegular());

		TextView labelDescription = (TextView) findViewById(R.id.labelDescription);
        labelDescription.setTypeface(MiddlewareApplication.getDinRegular());
		labelDescription.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				descriptionContent.setVisibility(View.VISIBLE);
				calificationContent.setVisibility(View.GONE);
				questionsContent.setVisibility(View.GONE);
			}
		});

		TextView labelCalification = (TextView) findViewById(R.id.labelCalification);
        labelCalification.setTypeface(MiddlewareApplication.getDinRegular());
		labelCalification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				calificationContent.setVisibility(View.VISIBLE);
				questionsContent.setVisibility(View.GONE);
				descriptionContent.setVisibility(View.GONE);
			}
		});


		TextView labelQuestions = (TextView) findViewById(R.id.labelQuestions);
        labelQuestions.setTypeface(MiddlewareApplication.getDinRegular());
		labelQuestions.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				questionsContent.setVisibility(View.VISIBLE);
				calificationContent.setVisibility(View.GONE);
				descriptionContent.setVisibility(View.GONE);
			}
		});

	}

    private CalificacionActividad hizoVotacion(List<CalificacionActividad>  caliAct){
        if(!caliAct.isEmpty()) {
            for (CalificacionActividad cali : caliAct) {
                if (cali.getUser().getId() == UtilsMiddleware.getInstance(this).getLogedUserId()) {
                    return cali;
                }
            }
        }
        return new CalificacionActividad();
    }

    private void getCalificaciones(){
        GetAgendaCalificacionesTask task = new GetAgendaCalificacionesTask(this) {
            @Override
            public void onServiceReturned(GetCalificacionesResponse response) {
                calificaciones = new ArrayList<CalificacionActividad>();
                if (response.getData() != null && response.getData().size() > 0) {
                    calificaciones.addAll(response.getData());

                    noCalificationsData.setVisibility(View.GONE);
                    calificationContent.removeAllViews();
                    for(CalificacionActividad calificacion: calificaciones){
                        calificationContent.addView(new CalificacionListItem(DetailsActividadActivity.this, calificacion));
                    }
                }
            }
        };
        ActividadRequest request = new ActividadRequest(actividad.getId());
        task.execute(request);
    }

    private void getPreguntas(){
        if(actividad.getUser().getId() == UtilsMiddleware.getInstance(this).getLogedUserId()){
            GetAgendaCuestionariosTask task = new GetAgendaCuestionariosTask(this) {
                @Override
                public void onServiceReturned(GetConsultasResponse response) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        List<ConsultaActividad> consultas = new ArrayList<ConsultaActividad>();
                        consultas.addAll(response.getData());
                        noQuestionsData.setVisibility(View.GONE);

                        for(ConsultaActividad consulta: consultas){
                            questionsContent.addView(new ConsultasListItem(DetailsActividadActivity.this, consulta));
                        }
                    }
                }
            };
            ActividadRequest request = new ActividadRequest(actividad.getId());
            task.execute(request);
        }else{
            noQuestionsData.setVisibility(View.GONE);
            questionsContent.addView(new WriteConsulta(DetailsActividadActivity.this, actividad.getId()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = ((MiddlewareApplication)getApplication()).getDefaultTracker();
        tracker.setScreenName("TemasDetalle");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}