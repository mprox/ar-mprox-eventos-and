package com.mprox.middleware.phone.ui.view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.CalificacionActividad;
import com.mprox.middleware.core.domain.Store;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.squareup.picasso.Picasso;

public class CalificacionListItem extends LinearLayout {

	public CalificacionListItem(Context context, CalificacionActividad calificacionActividad) {
		super(context);

		LayoutInflater.from(context).inflate(R.layout.calificacion_list_item, this);

//        TextView labelComentarios = (TextView) findViewById(R.id.labelComentarios);
//        labelComentarios.setTypeface(MiddlewareApplication.getDinRegular());
//        labelComentarios.setText(calificacionActividad.getObservacion());

        TextView labelUser = (TextView) findViewById(R.id.labelUser);
        labelUser.setTypeface(MiddlewareApplication.getDinRegular());
        labelUser.setText(calificacionActividad.getUser().getName());

        RatingBar redeem_price = (RatingBar) findViewById(R.id.redeem_price);
        redeem_price.setRating(calificacionActividad.getVoteTotal());

	}



}
