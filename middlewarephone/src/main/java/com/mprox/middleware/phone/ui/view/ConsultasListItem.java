package com.mprox.middleware.phone.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.CalificacionActividad;
import com.mprox.middleware.core.domain.ConsultaActividad;
import com.mprox.middleware.phone.application.MiddlewareApplication;

public class ConsultasListItem extends LinearLayout {

	public ConsultasListItem(Context context, ConsultaActividad consultaActividad) {
		super(context);

		LayoutInflater.from(context).inflate(R.layout.consulta_list_item, this);

        TextView labelConsulta = (TextView) findViewById(R.id.labelConsulta);
        labelConsulta.setTypeface(MiddlewareApplication.getDinRegular());
        labelConsulta.setText(consultaActividad.getConsulta());

        TextView labelUser = (TextView) findViewById(R.id.labelUser);
        labelUser.setText(consultaActividad.getUser().getName());
        labelUser.setTypeface(MiddlewareApplication.getDinRegular());

	}
}
