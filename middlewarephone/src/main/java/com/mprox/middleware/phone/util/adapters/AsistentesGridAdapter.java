package com.mprox.middleware.phone.util.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.util.UtilsMiddleware;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

@SuppressLint("SimpleDateFormat")
public class AsistentesGridAdapter extends BaseAdapter {

	private ArrayList<User> asistentes;
	private Context mContext;
	private View returnView;

	public AsistentesGridAdapter(Context context, ArrayList<User> newList) {
		this.mContext = context;
		this.asistentes = newList;
	}

	@Override
	public int getCount() {
		if (asistentes == null) {
			return 0;
		}

		return asistentes.size();
	}

	@Override
	public Object getItem(int arg0) {
		return asistentes.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		if (convertView == null) {
			returnView = LayoutInflater.from(mContext).inflate(R.layout.asistente_grid_item, arg2, false);
		} else {
			returnView = convertView;
		}

		try {
			if(asistentes.get(position).getImage() != null) {
                if (asistentes.get(position).getImage().contains("/mproxeventos/usuarios/")){
                    String url2 = UtilsMiddleware.formatImageUrl(asistentes.get(position).getImage());
                    Picasso.with(mContext)
                            .load(url2)
                            .into(((ImageView) returnView.findViewById(R.id.benefit_image)));
                }else{
                    ((ImageView) returnView.findViewById(R.id.benefit_image)).setVisibility(View.VISIBLE);
                    byte[] decodedString = Base64.decode(asistentes.get(position).getImage(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                    ((ImageView) returnView.findViewById(R.id.benefit_image)).setImageBitmap(decodedByte);
                }
			}else{
				((ImageView) returnView.findViewById(R.id.benefit_image)).setVisibility(View.GONE);
			}
		}catch (Exception e) {
			//Nothing
		}

		Typeface dinRegular = MiddlewareApplication.getDinRegular();

		TextView nombre = (TextView) returnView.findViewById(R.id.nombre);
		nombre.setTypeface(dinRegular);
		nombre.setText(asistentes.get(position).getName());

		TextView email = (TextView) returnView.findViewById(R.id.email);
		email.setTypeface(dinRegular);
		email.setText(asistentes.get(position).getEmail());

		return returnView;
	}
}
