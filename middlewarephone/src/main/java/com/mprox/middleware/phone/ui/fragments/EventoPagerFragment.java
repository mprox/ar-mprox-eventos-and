package com.mprox.middleware.phone.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mprox.middleware.R;
import com.mprox.middleware.core.domain.Evento;
import com.mprox.middleware.phone.application.MiddlewareApplication;
import com.mprox.middleware.phone.ui.activity.LoginActivity;
import com.mprox.middleware.phone.util.adapters.EventoGridAdapter;

import java.util.ArrayList;

public class EventoPagerFragment extends Fragment{

	private final static String BENEFITLIST = "benefit_list";

	private ArrayList<Evento> benefitsList;
	private ArrayList<Evento> benefitsListOriginal;

	private ListView grid;
	private EventoGridAdapter adapter;

	public static EventoPagerFragment newInstance(ArrayList<Evento> benefits) {
		EventoPagerFragment fragment = new EventoPagerFragment();

		Bundle args = fragment.getArguments();
		if (args == null) {
			args = new Bundle();
		}

		args.putParcelableArrayList(BENEFITLIST, benefits);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		benefitsList = getArguments().getParcelableArrayList(BENEFITLIST);

		benefitsListOriginal = new ArrayList<Evento>();
		if (benefitsList != null)
			benefitsListOriginal.addAll(benefitsList);
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		grid = (ListView) inflater.inflate(R.layout.fragment_agenda_list, null);
		grid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				openEventLogin(benefitsList.get(arg2));
			}
		});

		adapter = new EventoGridAdapter(getActivity(), benefitsList);
		grid.setAdapter(adapter);
		return grid;
	}

	@Override
	public void onResume() {
		super.onResume();
		Tracker tracker = ((MiddlewareApplication)getActivity().getApplication()).getDefaultTracker();
		tracker.setScreenName("Temas");
		tracker.send(new HitBuilders.ScreenViewBuilder().build());
	}

	private void openEventLogin(Evento Evento) {
		Intent intent = new Intent(getActivity(), LoginActivity.class);
		intent.putExtra(LoginActivity.EVENT_TO_LOGIN, Evento);
		startActivity(intent);
	}
}