package com.mprox.middleware;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.google.android.gcm.GCMBaseIntentService;
import com.mprox.middleware.phone.ui.activity.SplashActivity;
import com.mprox.middleware.phone.util.pusher.IPHPusher;
import com.mprox.middleware.phone.util.pusher.ServerUtilities;

/**
 * {@link IntentService} responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		super(IPHPusher.sharedInstance().get_senderId());
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		ServerUtilities.register(context, regId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		if (!IPHPusher.sharedInstance().is_isVisible()) {
			generateNotification(context, intent);
		} else {
			displayMessage(context, intent.getExtras().getString("message"), intent.getExtras().getString("notificationId"), intent
					.getExtras().getString("extradata"));
		}
	}

	static void displayMessage(Context context, String message, String notificationId, String extraData) {
		Intent intent = new Intent("com.mprox.middleware.DISPLAY_MESSAGE");
		intent.putExtra("message", message);
		intent.putExtra("notificationId", notificationId);
		intent.putExtra("extradata", extraData);
		context.sendBroadcast(intent);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private void generateNotification(Context context, Intent intent) {
		try {
			int icon = R.drawable.directv_notification;
			String title = context.getString(R.string.app_name);
			long when = System.currentTimeMillis();

            String message = intent.getExtras().getString("message");
            String notificationId = intent.getExtras().getString("notificationId");
            String extraData = intent.getExtras().getString("extradata");

			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = new Notification(icon, message, when);

            PackageManager packageManager = getPackageManager();
            Intent notificationIntent = packageManager.getLaunchIntentForPackage(context.getPackageName());
            notificationIntent.putExtra("message", message);
            notificationIntent.putExtra("notificationId", notificationId);
            notificationIntent.putExtra("extradata", extraData);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

			Notification.Builder builder = new Notification.Builder(context);
			builder.setAutoCancel(true);
			builder.setContentTitle(title);
			builder.setContentText(message);
			builder.setSmallIcon(R.drawable.directv_notification);
			builder.setContentIntent(pendingIntent);
			builder.setOngoing(false);
			builder.setNumber(100);
			builder.build();

			notification = builder.getNotification();
			notificationManager.notify(0, notification);

		} catch (Exception e) {
			//Nothing
		}
	}

	@Override
	protected void onError(Context context, String errorId) {
	}

	@Override
	protected void onUnregistered(Context context, String regId) {
	}
}
