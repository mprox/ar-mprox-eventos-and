package com.mprox.sdk.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.mprox.sdk.R;
import com.mprox.sdk.db.dao.BeaconActivityDao;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.utils.UtilsMprox;

public class MproxMessageActivity extends Activity {

	public static final String MESSAGE = "message";
	public static final String PRODUCT = "product";
	public static final String BEACON = "beacon";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mprox_full_screen_message);
		
		if(getIntent() == null){
			finish();
		}

		String message = getIntent().getStringExtra(MESSAGE);
		Product product = (Product) getIntent().getSerializableExtra(PRODUCT);
		Beacon beacon = getIntent().getParcelableExtra(BEACON);

		if(message != null ){
			TextView msgTitle = (TextView) findViewById(R.id.msg_title);
			msgTitle.setText(message);
		}else{
			finish();
		}

		if(beacon != null && product != null)
			logOpenNotification(UtilsMprox.getFullBeaconID(beacon),
					String.valueOf(product.getId()));

	}

	@SuppressLint("SimpleDateFormat")
	private void logOpenNotification(String beaconId, String productId) {
		// Saving notification open track
		BeaconActivityDao beaconDao = new BeaconActivityDao(MproxMessageActivity.this);
		beaconDao.saveBeaconOpenTrack(beaconId, String.valueOf(productId));
	}
	
	public void closeActivity(View view){
		finish();
	}
	
	@Override
	protected void onDestroy() {
		
		Intent intent = new Intent();
		intent.setAction("com.mprox.sdk.activity.NOTIFICATION_CLOSED");
		sendBroadcast(intent);

		super.onDestroy();
	}
}
