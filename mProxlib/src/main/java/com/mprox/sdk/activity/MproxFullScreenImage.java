package com.mprox.sdk.activity;

import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.estimote.sdk.Beacon;
import com.mprox.sdk.R;
import com.mprox.sdk.db.dao.BeaconActivityDao;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.utils.UtilsMprox;
import com.squareup.picasso.Picasso;

public class MproxFullScreenImage extends Activity {

	public static final String PRODUCT = "product";
	public static final String BEACON = "beacon";

	private ImageView picture;
	private Boolean allreadyAskedToSaveImage = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.mprox_full_screen_image);

		if (getIntent() == null) {
			finish();
		}

		Product product = (Product) getIntent().getSerializableExtra(PRODUCT);
		Beacon beacon = getIntent().getParcelableExtra(BEACON);

		if (product != null) {

			picture = (ImageView) findViewById(R.id.full_screen_image);
			Picasso.with(MproxFullScreenImage.this).load(UtilsMprox.formatImageUrl(product.getImageUrl())).into(picture);

			ImageView save = (ImageView) findViewById(R.id.save);
			save.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					saveImageOnDevice();
				}
			});

			if (beacon != null) {
				logOpenNotification(UtilsMprox.getFullBeaconID(beacon), String.valueOf(product.getId()));
			}

		} else {
			finish();
		}
	}

	@SuppressLint("SimpleDateFormat")
	private void logOpenNotification(String beaconId, String productId) {
		// Saving notification open track
		BeaconActivityDao beaconDao = new BeaconActivityDao(MproxFullScreenImage.this);
		beaconDao.saveBeaconOpenTrack(beaconId, String.valueOf(productId));
	}

	public void closeActivity(View view) {
		if (!allreadyAskedToSaveImage) {
			displaySaveImageDialog();
		} else {
			finish();
		}
	}

	@Override
	protected void onDestroy() {

		Intent intent = new Intent();
		intent.setAction("com.mprox.sdk.activity.NOTIFICATION_CLOSED");
		sendBroadcast(intent);

		super.onDestroy();
	}

	private void displaySaveImageDialog() {
		allreadyAskedToSaveImage = true;

		AlertDialog.Builder builder = new AlertDialog.Builder(MproxFullScreenImage.this);
		builder.setMessage("¿Desea guardar la promoción en la galería de imágenes?")
				.setPositiveButton("Si", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						saveImageOnDevice();
						finish();
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						finish();
					}
				});
		builder.create().show();
	}

	private void saveImageOnDevice() {

		try {

			Random rnd = new Random();
			rnd.setSeed(System.currentTimeMillis());
			Integer al6 = 100000 + rnd.nextInt(900000);

			String filename = "promo-" + al6.toString() + ".png";

			MediaStore.Images.Media.insertImage(getContentResolver(), ((BitmapDrawable) picture.getDrawable()).getBitmap(), filename,
					filename);

		} catch (Exception e) {
			
			Log.i("mprox-services", "MproxFullScreenImage -- saveImageOnDevice" + e.getMessage());
			
			e.printStackTrace();
		}
	}

}