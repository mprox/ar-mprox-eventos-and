package com.mprox.sdk.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.estimote.sdk.Beacon;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnFullscreenListener;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.mprox.sdk.R;
import com.mprox.sdk.db.dao.BeaconActivityDao;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.utils.UtilsMprox;

public class MproxFullScreenVideo extends YouTubeBaseActivity implements OnInitializedListener, OnFullscreenListener {

	private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9 ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
			: ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;

	private static final int LANDSCAPE_ORIENTATION = Build.VERSION.SDK_INT < 9 ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
			: ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;

	public static final String PRODUCT = "product";
	public static final String BEACON = "beacon";

	private static final String YOUTUBE_APP_KEY = "AIzaSyD9Wawkqs_6JidWUjpRVgQZSYXWxfULeeQ";

	private Product product;

	private YouTubePlayer mPlayer = null;
	private boolean mAutoRotation = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.mprox_full_screen_video);
		
		if(getIntent() == null){
			finish();
		}

		product = (Product) getIntent().getSerializableExtra(PRODUCT);
		Beacon beacon = getIntent().getParcelableExtra(BEACON);

		mAutoRotation = Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1;

		if(product != null ){
			logOpenNotification(UtilsMprox.getFullBeaconID(beacon), String.valueOf(product.getId()));
			findAndInitViews();
		}else{
			finish();
		}
	}

	protected void findAndInitViews() {
		YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		youTubeView.initialize(YOUTUBE_APP_KEY, this);
	}

	@Override
	public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
	}

	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer player, boolean wasRestored) {
		mPlayer = player;
		player.setOnFullscreenListener(this);

		if (mAutoRotation) {
			player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
					| YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI | YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE
					| YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
		} else {
			player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
					| YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI | YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
		}

		if (!wasRestored) {
			player.cueVideo(product.getVideoFullScreenUrl());
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			if (mPlayer != null)
				mPlayer.setFullscreen(true);
		}
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			if (mPlayer != null)
				mPlayer.setFullscreen(false);
		}
	}

	@Override
	public void onFullscreen(boolean fullsize) {
		if (fullsize) {
			setRequestedOrientation(LANDSCAPE_ORIENTATION);
		} else {
			setRequestedOrientation(PORTRAIT_ORIENTATION);
		}
	}

	@SuppressLint("SimpleDateFormat")
	private void logOpenNotification(String beaconId, String productId) {
		// Saving notification open track
		BeaconActivityDao beaconDao = new BeaconActivityDao(MproxFullScreenVideo.this);
		beaconDao.saveBeaconOpenTrack(beaconId, String.valueOf(productId));
	}
	
	public void closeActivity(View view){
		finish();
	}
	
	@Override
	protected void onDestroy() {
		
		Intent intent = new Intent();
		intent.setAction("com.mprox.sdk.activity.NOTIFICATION_CLOSED");
		sendBroadcast(intent);

		super.onDestroy();
	}
}