package com.mprox.sdk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.mprox.sdk.comunication.BeaconTrackingActivityTask;
import com.mprox.sdk.comunication.GetAllProductsTask;
import com.mprox.sdk.comunication.request.BeaconTrackingActivityRequest;
import com.mprox.sdk.comunication.request.ProdutsByLocationRequest;
import com.mprox.sdk.comunication.response.GetProductsResponse;
import com.mprox.sdk.db.dao.BeaconActivityDao;
import com.mprox.sdk.db.dao.ProductDao;
import com.mprox.sdk.location.CustomLocationManager;
import com.mprox.sdk.location.LocationManagerDelegate;
import com.mprox.sdk.model.BeaconActivity;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.utils.Log;
import com.mprox.sdk.utils.UtilsMprox;

public class ProductsService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					// save tracking data
					callSaveTrackingData();
				}
			}, 1000 * 30, 1000 * 60 * 10);

		} catch (Exception e) {
			Log.i(ProductsService.this, "mprox-services", "ProductsService -- onStartCommand error [" + e.getMessage() + "]");
		}

		return Service.START_STICKY;
	}

	protected void callSaveTrackingData() {
		try {
			final BeaconActivityDao dao = new BeaconActivityDao(ProductsService.this);

			final List<BeaconActivity> beacon_user_track = dao.getBeaconUserTrackList();

			BeaconTrackingActivityRequest request = new BeaconTrackingActivityRequest(UtilsMprox.getInstance(ProductsService.this)
					.getLogedUserId(), beacon_user_track);

			BeaconTrackingActivityTask task = new BeaconTrackingActivityTask(ProductsService.this) {
				@Override
				public void onServiceReturned(GetProductsResponse response) {
					Log.i(ProductsService.this, "mprox-services", "ProductsService -- Save beacon tracking[" + response.isSuccess() + "]");
					if(!response.isSuccess()){
						dao.saveBeaconUserTrack(beacon_user_track);
					}
				}
			};
			task.execute(request);

		} catch (Exception e) {
			Log.i(ProductsService.this, "mprox-services", "ProductsService -- CallSaveTrackingData error [" + e.getMessage() + "]");
		}
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
	}
}