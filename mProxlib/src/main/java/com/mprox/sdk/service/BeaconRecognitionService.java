package com.mprox.sdk.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.mprox.sdk.R;
import com.mprox.sdk.broadcast.NotificationCreator;
import com.mprox.sdk.comunication.UserBluetoothStatusTask;
import com.mprox.sdk.comunication.request.UserBluetoothStatusRequest;
import com.mprox.sdk.comunication.response.LoginResponse;
import com.mprox.sdk.db.DataHelper;
import com.mprox.sdk.db.dao.BeaconActivityDao;
import com.mprox.sdk.db.dao.BeaconProductDao;
import com.mprox.sdk.db.dao.ProductDao;
import com.mprox.sdk.model.BeaconActivity;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.utils.Log;
import com.mprox.sdk.utils.UtilsMprox;


public class BeaconRecognitionService<T extends Activity> extends Service {

    private static final int NOTIFICATION_ID = 10302;
    private boolean alreadySended = false;

	public static final String BEACON = "BEACON";
	public static final String PRODUCT = "PRODUCT";
	public static final String REVIVEME = "REVIVE_ME";

	public static final String REGION_ID = "b9407f30-f5f8-466e-aff9-25556b57fe6d";
	public static final int SAME_BEACON_CONNECTION_LIMIT = 5;

	public static Boolean isRunning = false;

	private BeaconManager beaconManager;
	private Region beaconsRegion;

	private Beacon lastNearestBeacon;
	private Integer beaconConnections = 0;
	
	private List<Beacon> auxbeacons;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			
			beaconsRegion = new Region("rid", REGION_ID, null, null);
			beaconManager = new BeaconManager(this);

			BeaconRecognitionService.isRunning = true;
			auxbeacons = new ArrayList<Beacon>();
			
			beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
			    @Override
			    public void onEnteredRegion(Region region, List<Beacon> list) {
			    	try {
			    		Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- on Entered Region");
						beaconManager.startRanging(beaconsRegion);
					} catch (RemoteException e) {
						Log.e(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Cannot start monitoring");
						startRangingWithDelay(5000);
					}
			    }
			    @Override
			    public void onExitedRegion(Region region) {
			    	try {
			    		Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- on Exited Region");
			    		beaconManager.stopRanging(beaconsRegion);
			    	} catch (RemoteException e) {
						Log.e(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Cannot stop monitoring");
					}
			    }
			});
			
			beaconManager.setRangingListener(new BeaconManager.RangingListener() {
				@Override
				public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
					Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- on Beacons Discovered");

                    if (beacons.size() > 0) {
                        if(!alreadySended && !UtilsMprox.getInstance(BeaconRecognitionService.this).getLogedUserTeam().equals("")){
                            long ultimoEnvio = 0l;

                            DataHelper dataHelper = new DataHelper(BeaconRecognitionService.this);
                            Cursor cursor = dataHelper.selectAll("notificationActivity", null);
                            if (cursor.moveToFirst()) {
                                do {
                                    try {
                                        ultimoEnvio = Long.parseLong(cursor.getString(cursor.getColumnIndex("date")));
                                    }catch (Exception e){}
                                } while (cursor.moveToNext());
                            }

                            if (cursor != null && !cursor.isClosed()) {
                                cursor.close();
                            }
                            dataHelper.close();

                            if(ultimoEnvio == 0l){
                                senNotification();
                            }
                        }

						Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Beacons count" + beacons.size());
						
						auxbeacons.clear();
						auxbeacons.addAll(beacons);
						
						UtilsMprox.getInstance(BeaconRecognitionService.this).sortBeaconByRssi(auxbeacons);

						if (auxbeacons != null && auxbeacons.size() > 0) {
							UtilsMprox.getInstance(BeaconRecognitionService.this).addTopBeacon(auxbeacons.get(0));
						}

						Beacon nearestBeacon = UtilsMprox.getInstance(BeaconRecognitionService.this).getMostFrecuentBeacon();
						if (nearestBeacon != null) {
							beaconConnections++;

							// Saving beacon recognition track
							BeaconActivityDao beaconDao = new BeaconActivityDao(BeaconRecognitionService.this);
							beaconDao.saveBeaconUserTrack(UtilsMprox.getFullBeaconID(nearestBeacon));

							if (beaconConnections > SAME_BEACON_CONNECTION_LIMIT || (lastNearestBeacon == null || 
									(!UtilsMprox.getFullBeaconID(nearestBeacon).equals(UtilsMprox.getFullBeaconID(lastNearestBeacon))))) {

								Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- nearestBeacon detected");
								lastNearestBeacon = nearestBeacon;
								beaconConnections = 0;
							}
						}
					}
				}
			});

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					startBeaconRecognition();
				}
			}, 500);
		} catch (Exception e) {
			Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- onStartCommand error [" + e.getMessage() + "]");
		}

		return Service.START_STICKY;
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
	}

	private void startBeaconRecognition() {
		if (beaconManager == null) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					startBeaconRecognition();
				}
			}, 5000);
			return;
		}

		// Check if device supports Bluetooth Low Energy.
		if (!beaconManager.hasBluetooth()) {
			Log.e(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Device does not have Bluetooth Low Energy");
			stopSelf();
			return;
		}

		// If Bluetooth is not enabled, let user enable it.
		if (!beaconManager.isBluetoothEnabled()) {
			Log.e(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Device Bluetooth off");
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					startBeaconRecognition();
				}
			}, 5000);
			return;
		} else {
			beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
				@Override
				public void onServiceReady() {
					startRangingWithDelay(1);
				}
			});
		}
	}

	private void startRangingWithDelay(int delay) {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				try {
					beaconManager.startMonitoring(beaconsRegion);
					Log.i(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Start Monitoring");

                    String logedUserId = UtilsMprox.getInstance(BeaconRecognitionService.this).getLogedUserId();
                    UserBluetoothStatusRequest request = new UserBluetoothStatusRequest(logedUserId,
                            beaconManager.isBluetoothEnabled(), beaconManager.hasBluetooth());
                    UserBluetoothStatusTask task = new UserBluetoothStatusTask(BeaconRecognitionService.this) {
                        public void onServiceReturned(LoginResponse response) {
                        }
                    };
                    task.execute(request);
				} catch (RemoteException e) {
					Log.e(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Cannot start ranging, something terrible happened. Restart ranging in 5 seconds");
					startRangingWithDelay(5000);
				}
			}
		}, delay);
	}

	@Override
	public void onDestroy() {
		if (beaconManager != null) {
			try {
				beaconManager.stopRanging(beaconsRegion);
				beaconManager.stopMonitoring(beaconsRegion);
			} catch (RemoteException e) {
				Log.e(BeaconRecognitionService.this, "mprox-services", "BeaconRecognitionService -- Cannot stop ranging, something terrible happened");
				e.printStackTrace();
			}
		}
		super.onDestroy();
		BeaconRecognitionService.isRunning = true;
	}

    private void senNotification(){
        alreadySended = true;

        DataHelper dataHelper = new DataHelper(BeaconRecognitionService.this);
        dataHelper.deleteAll("notificationActivity");
        dataHelper.insert("notificationActivity", "date", String.valueOf(new Date().getTime()));
        dataHelper.close();

        // prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent();
        intent.setAction("com.mprox.middleware.phone.util.NotificationReceiver");
        PendingIntent pIntent = PendingIntent.getBroadcast(BeaconRecognitionService.this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        long when = System.currentTimeMillis();

        // build notification
        // the addAction re-use the same intent to keep the example short
        Notification n = new Notification.Builder(BeaconRecognitionService.this)
                .setContentTitle("Bienvenido")
                .setContentText("A la Jornada de Operaciones")
                .setContentIntent(pIntent).setSmallIcon(R.drawable.directv_notification)
                .setWhen(when).setAutoCancel(true).build();

        n.defaults|= Notification.DEFAULT_SOUND;
        n.defaults|= Notification.DEFAULT_LIGHTS;
        n.defaults|= Notification.DEFAULT_VIBRATE;

        NotificationManager notificationManager = (NotificationManager) BeaconRecognitionService.this
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_ID, n);
    }
}