package com.mprox.sdk.location;

import java.io.Serializable;

public class Coordinate2D implements Serializable {

	private static final long serialVersionUID = 1L;
	public double latitude;
	public double longitude;

	public Coordinate2D(double lat, double longi) {
		latitude = lat;
		longitude = longi;
	}

}
