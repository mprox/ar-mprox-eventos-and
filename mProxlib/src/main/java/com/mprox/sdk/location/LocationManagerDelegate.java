package com.mprox.sdk.location;

public interface LocationManagerDelegate {

	void locationManagerFinshedWithCoords(CustomLocationManager manager,
			Double longitude, Double latitude);

}
