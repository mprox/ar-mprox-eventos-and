package com.mprox.sdk.location;

import android.app.Service;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class CustomLocationManager implements LocationListener {

	private static final String LOG_TAG = "mprox location";
	private static CustomLocationManager shareInstance;

	private LocationManagerDelegate delegate;
	private Location lastReadLocation;

	// #define DEFAULT_COORDS mprox headquarters
	public static double DEFAULT_LAT = 0D;
	public static double DEFAULT_LONG = 0D;

	private Service mContext;

	// additional data that delegate could need
	private Object data;

	public static CustomLocationManager getLocationManager(Service context) {
		// create the singleton.
		if (shareInstance == null)
			shareInstance = new CustomLocationManager(context);

		return shareInstance;
	}

	public CustomLocationManager(Service context) {
		mContext = context;
		this.startLocationListeners();
	}

	private void startLocationListeners() {
		LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	}

	private void notifyToDelegate(double theLatitude, double theLongitude) {
		Log.i(LOG_TAG, "LocationManager - notifyToDelegate");
		if (delegate != null) {
			delegate.locationManagerFinshedWithCoords(this, theLongitude, theLatitude);
			Log.i(LOG_TAG, "LocationManager - notifyToDelegate: delegate != null");
		}
		// reset the delegate after the notification.
		delegate = null;
	}

	public Object getData() {
		return data;
	}

	public static void startRetrievingCurrentLocationWithDelegate(
			Service context, LocationManagerDelegate theDelegate, Object data) {
		CustomLocationManager currentManager = getLocationManager(context);
		currentManager.delegate = theDelegate;
		currentManager.data = data;

		currentManager.startLocationListeners();
	}

	public Coordinate2D getLastReadLocation(boolean showMessageIfGPSIsDisabled) {
		if (this.lastReadLocation != null)
			return new Coordinate2D(this.lastReadLocation.getLatitude(),
					this.lastReadLocation.getLongitude());

		return new Coordinate2D(DEFAULT_LAT, DEFAULT_LONG);
	}

	public void onLocationChanged(Location arg0) {
		Log.i(LOG_TAG, "LocationManager - onLocationChanged: provider: "
						+ arg0.getProvider() + " - latitude: "
						+ arg0.getLatitude() + " - longitude: "
						+ arg0.getLongitude());

		lastReadLocation = arg0;

		this.notifyToDelegate(lastReadLocation.getLatitude(),
				lastReadLocation.getLongitude());

		LocationManager locationManager = (LocationManager) mContext
				.getSystemService(Context.LOCATION_SERVICE);
		locationManager.removeUpdates(this);
	}

	public void onProviderDisabled(String arg0) {
		Log.i(LOG_TAG, "LocationManager - GPS disabled");
	}

	public void onProviderEnabled(String arg0) {
		Log.i(LOG_TAG, "LocationManager - GPS enabled");
	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}

	public static Location createLocation(double latitude, double longitude) {
		Location newLocation = new Location(LocationManager.GPS_PROVIDER);
		newLocation.setLatitude(latitude);
		newLocation.setLongitude(longitude);

		return newLocation;
	}

	public static void setup(Service context) {
		// saving the phone gps position for the first time.
		getLocationManager(context);
	}
}
