package com.mprox.sdk.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("idProduct")
	private int id;

	@JsonProperty("beaconsIds")
	private String beaconsIds;

	@JsonProperty("productType")
	private int productType;

	@JsonProperty("category")
	private String category;

	@JsonProperty("categoryId")
	private Integer categoryId;

	@JsonProperty("notificationMessage")
	private String notificationMessage;

	@JsonProperty("title")
	private String title;

	@JsonProperty("description")
	private String description;

	@JsonProperty("imageUrl")
	private String imageUrl;

	@JsonProperty("priceBefore")
	private float priceBefore;

	@JsonProperty("priceNow")
	private float priceNow;

	@JsonProperty("discount")
	private float discount;

	@JsonProperty("points")
	private float points;

	@JsonProperty("rating")
	private float rating;

	@JsonProperty("isPriority")
	private boolean isPriority;

	@JsonProperty("startDate")
	private String startDate;

	@JsonProperty("timeToWait")
	private String timeToWait;

	@JsonProperty("endDate")
	private String endDate;

	@JsonProperty("termAndConditions")
	private String termAndConditions;

	@JsonProperty("showBefore")
	private boolean showBefore;

	@JsonProperty("showAfter")
	private boolean showAfter;

	@JsonProperty("isHighlight")
	private boolean isHighlight;

	@JsonProperty("imageFullScreenUrl")
	private String imageFullScreenUrl;

	@JsonProperty("videoFullScreenUrl")
	private String videoFullScreenUrl;

	@JsonProperty("photoPromo")
	private String photoPromo;

	@JsonProperty("photoQR")
	private String photoQR;

	@JsonProperty("logoMapUrl")
	private String logoMapUrl;

	@JsonProperty("logoUrl")
	private String logoUrl;

	@JsonProperty("storeName")
	private String storeName;

	@JsonProperty("voteQuantity")
	private Integer voteQuantity;

	@JsonProperty("daysOfWeek")
	private String daysOfWeek;

	@JsonProperty("starHourToShow")
	private String starHourToShow;

	@JsonProperty("endHourToShow")
	private String endHourToShow;

	@JsonProperty("frequencyToShow")
	private Integer frequencyToShow;

	@JsonProperty("datefrequencyToShow")
	private Integer datefrequencyToShow;

	@JsonProperty("limitForUser")
	private Integer limitForUser;

	@JsonProperty("totalSendedToUserQuantity")
	private Integer totalSendedToUserQuantity;

	@JsonProperty("daySendedToUserQuantity")
	private Integer daySendedToUserQuantity;

	@JsonProperty("lastNotificationSendedTime")
	private String lastNotificationSendedTime;

	private String productHeader;

	public Product() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProductType() {
		return productType;
	}

	public void setProductType(int productType) {
		this.productType = productType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public float getPriceBefore() {
		return priceBefore;
	}

	public void setPriceBefore(float priceBefore) {
		this.priceBefore = priceBefore;
	}

	public float getPriceNow() {
		return priceNow;
	}

	public void setPriceNow(float priceNow) {
		this.priceNow = priceNow;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public float getPoints() {
		return points;
	}

	public void setPoints(float points) {
		this.points = points;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public boolean isPriority() {
		return isPriority;
	}

	public void setPriority(boolean isPriority) {
		this.isPriority = isPriority;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTimeToWait() {
		return timeToWait;
	}

	public void setTimeToWait(String timeToWait) {
		if (timeToWait != null && !timeToWait.equalsIgnoreCase("null")) {
			this.timeToWait = timeToWait;
		} else {
			this.timeToWait = "0";
		}
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTermAndConditions() {
		return termAndConditions;
	}

	public void setTermAndConditions(String termAndConditions) {
		this.termAndConditions = termAndConditions;
	}

	public boolean isShowBefore() {
		return showBefore;
	}

	public void setShowBefore(boolean showBefore) {
		this.showBefore = showBefore;
	}

	public boolean isShowAfter() {
		return showAfter;
	}

	public void setShowAfter(boolean showAfter) {
		this.showAfter = showAfter;
	}

	public boolean isHighlight() {
		return isHighlight;
	}

	public void setHighlight(boolean isHighlight) {
		this.isHighlight = isHighlight;
	}

	public String getImageFullScreenUrl() {
		return imageFullScreenUrl;
	}

	public void setImageFullScreenUrl(String imageFullScreenUrl) {
		this.imageFullScreenUrl = imageFullScreenUrl;
	}

	public String getVideoFullScreenUrl() {
		return videoFullScreenUrl;
	}

	public void setVideoFullScreenUrl(String videoFullScreenUrl) {
		this.videoFullScreenUrl = videoFullScreenUrl;
	}

	public String getPhotoPromo() {
		return photoPromo;
	}

	public void setPhotoPromo(String photoPromo) {
		this.photoPromo = photoPromo;
	}

	public String getPhotoQR() {
		return photoQR;
	}

	public void setPhotoQR(String photoQR) {
		this.photoQR = photoQR;
	}

	public String getLogoMapUrl() {
		return logoMapUrl;
	}

	public void setLogoMapUrl(String logoMapUrl) {
		this.logoMapUrl = logoMapUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Integer getVoteQuantity() {
		return voteQuantity;
	}

	public void setVoteQuantity(Integer voteQuantity) {
		this.voteQuantity = voteQuantity;
	}

	public String getProductHeader() {
		return productHeader;
	}

	public void setProductHeader(String productHeader) {
		this.productHeader = productHeader;
	}

	public String getDaysOfWeek() {
		return daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public String getStarHourToShow() {
		return starHourToShow;
	}

	public void setStarHourToShow(String starHourToShow) {
		this.starHourToShow = starHourToShow;
	}

	public String getEndHourToShow() {
		return endHourToShow;
	}

	public void setEndHourToShow(String endHourToShow) {
		this.endHourToShow = endHourToShow;
	}

	public Integer getFrequencyToShow() {
		return frequencyToShow;
	}

	public void setFrequencyToShow(Integer frequencyToShow) {
		if (frequencyToShow == null) {
			this.frequencyToShow = 0;
		} else {
			this.frequencyToShow = frequencyToShow;
		}
	}

	public Integer getDatefrequencyToShow() {
		return datefrequencyToShow;
	}

	public void setDatefrequencyToShow(Integer datefrequencyToShow) {
		if (datefrequencyToShow == null) {
			this.datefrequencyToShow = 0;
		} else {
			this.datefrequencyToShow = datefrequencyToShow;
		}
	}

	public Integer getLimitForUser() {
		return limitForUser;
	}

	public void setLimitForUser(Integer limitForUser) {
		if (limitForUser == null) {
			this.limitForUser = 0;
		} else {
			this.limitForUser = limitForUser;
		}
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getBeaconsIds() {
		return beaconsIds;
	}

	public void setBeaconsIds(String beaconsIds) {
		this.beaconsIds = beaconsIds;
	}

	public Integer getTotalSendedToUserQuantity() {
		return totalSendedToUserQuantity;
	}

	public void setTotalSendedToUserQuantity(Integer totalSendedToUserQuantity) {
		if (totalSendedToUserQuantity == null) {
			this.totalSendedToUserQuantity = 0;
		} else {
			this.totalSendedToUserQuantity = totalSendedToUserQuantity;
		}
	}

	public Integer getDaySendedToUserQuantity() {
		return daySendedToUserQuantity;
	}

	public void setDaySendedToUserQuantity(Integer daySendedToUserQuantity) {
		if (daySendedToUserQuantity == null) {
			this.daySendedToUserQuantity = 0;
		} else {
			this.daySendedToUserQuantity = daySendedToUserQuantity;
		}
	}

	public String getLastNotificationSendedTime() {
		if (lastNotificationSendedTime != null && lastNotificationSendedTime.length() > 1
				&& !lastNotificationSendedTime.equalsIgnoreCase("null")) {
			return lastNotificationSendedTime;
		} else {
			return "0";
		}
	}

	public void setLastNotificationSendedTime(String lastNotificationSendedTime) {
		this.lastNotificationSendedTime = lastNotificationSendedTime;
	}

	public String toString() {
		return "Product[idProduct:" + id + ",beaconsIds:" + beaconsIds + ",productType:" + productType + ",category:" + category
				+ ",categoryId:" + categoryId + ",notificationMessage:" + notificationMessage + ",title:" + title + ",description:"
				+ description + ",imageUrl:" + imageUrl + ",priceBefore:" + priceBefore + ",priceNow:" + priceNow + ",discount:" + discount
				+ ",points:" + points + ",rating:" + rating + ",isPriority:" + isPriority + ",startDate:" + startDate + ",timeToWait:"
				+ timeToWait + ",endDate:" + endDate + ",termAndConditions:" + termAndConditions + ",showBefore:" + showBefore
				+ ",showAfter:" + showAfter + ",isHighlight:" + isHighlight + ",imageFullScreenUrl:" + imageFullScreenUrl
				+ ",videoFullScreenUrl:" + videoFullScreenUrl + ",photoPromo:" + photoPromo + ",photoQR:" + photoQR + ",logoMapUrl:"
				+ logoMapUrl + ",logoUrl:" + logoUrl + ",storeName:" + storeName + ",voteQuantity:" + voteQuantity + ",daysOfWeek:"
				+ daysOfWeek + ",starHourToShow:" + starHourToShow + ",endHourToShow:" + endHourToShow + ",frequencyToShow:"
				+ frequencyToShow + ",datefrequencyToShow:" + datefrequencyToShow + ",limitForUser:" + limitForUser
				+ ",totalSendedToUserQuantity:" + totalSendedToUserQuantity + ",daySendedToUserQuantity:" + daySendedToUserQuantity
				+ ",lastNotificationSendedTime:" + lastNotificationSendedTime + "]";
	}
}