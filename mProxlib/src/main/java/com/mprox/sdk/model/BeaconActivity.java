package com.mprox.sdk.model;

import org.codehaus.jackson.annotate.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.Date;


public class BeaconActivity {

	@JsonProperty("uuidBeacon")
	private String uuidBeacon;

	@JsonProperty("idProduct")
	private Long idProduct;

	@JsonProperty("date")
	private String date;

	@JsonProperty("uptime")
	private String uptime;

	public String getUuidBeacon() {
		return uuidBeacon;
	}

	public void setUuidBeacon(String uuidBeacon) {
		this.uuidBeacon = uuidBeacon;
	}

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return  sdf.format(new Date(Long.parseLong(this.date)));
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}
}
