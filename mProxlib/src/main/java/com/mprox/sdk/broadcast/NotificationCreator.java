package com.mprox.sdk.broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.mprox.sdk.R;
import com.mprox.sdk.db.dao.BeaconActivityDao;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.service.BeaconRecognitionService;
import com.mprox.sdk.utils.UtilsMprox;

public class NotificationCreator {

	private static final int NOTIFICATION_ID = 10301;

	public static void processNotification(Context context, Product product, Beacon beacon) {		
		try{
			// Saving product recived track
			BeaconActivityDao beaconDao = new BeaconActivityDao(context);
			beaconDao.saveBeaconRecivedTrack(UtilsMprox.getFullBeaconID(beacon), String.valueOf(product.getId()));
			
			// prepare intent which is triggered if the
			// notification is selected
			Intent intent = new Intent();
			intent.setAction("com.mprox.sdk.broadcast.BeaconReceiver");
			intent.putExtra(BeaconRecognitionService.BEACON, beacon);
			intent.putExtra(BeaconRecognitionService.PRODUCT, product);
			PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			long when = System.currentTimeMillis();
			
			// build notification
			// the addAction re-use the same intent to keep the example short
			Notification n = new Notification.Builder(context)
					.setContentTitle(product.getNotificationMessage())
					.setContentIntent(pIntent).setSmallIcon(R.drawable.directv_notification)
					.setWhen(when).setAutoCancel(true).build();
			
			n.defaults|= Notification.DEFAULT_SOUND;
			n.defaults|= Notification.DEFAULT_LIGHTS;
			n.defaults|= Notification.DEFAULT_VIBRATE; 
	
			NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
	
			notificationManager.notify(NOTIFICATION_ID, n);
	
		}catch(Exception e ){
			Log.i("mprox-services", "NotificationCreator -- Process Notification error [" + e.getMessage() + "]");
		}
	}
	
}
