package com.mprox.sdk.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.estimote.sdk.Beacon;
import com.mprox.sdk.activity.MproxFullScreenImage;
import com.mprox.sdk.activity.MproxFullScreenVideo;
import com.mprox.sdk.activity.MproxMessageActivity;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.service.BeaconRecognitionService;
import com.mprox.sdk.utils.UtilsMprox;

public class BeaconReceiver extends BroadcastReceiver {

	Context appContext;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			appContext = context;
		
			if (Integer.valueOf(UtilsMprox.getInstance(context).getLogedUserId()) != 0) {

				if (intent.getExtras().getBoolean(BeaconRecognitionService.REVIVEME, false)) {
					Intent serviceIntent = new Intent(context, BeaconRecognitionService.class);
					context.startService(serviceIntent);
					return;
				}

				Beacon b = intent.getParcelableExtra(BeaconRecognitionService.BEACON);
				Product p = (Product) intent.getSerializableExtra(BeaconRecognitionService.PRODUCT);

				if (b != null && p != null) {
					switch (p.getProductType()) {
					case 4:// VIDEO
						showVideo(p, b);
						break;
					case 5:// IMAGEN
						showPicture(p, b);
						break;
					case 6:// MENSAJE
						showMessage(p, b);
						break;
					default:
						break;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showPicture(Product p, Beacon b) {
		if (p != null && p.getImageUrl() != null && p.getImageUrl().length() > 1) {
			Intent i = new Intent(appContext, MproxFullScreenImage.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra(MproxFullScreenImage.PRODUCT, p);
			i.putExtra(MproxFullScreenImage.BEACON, b);
			appContext.startActivity(i);
		}
	}

	private void showVideo(Product p, Beacon b) {
		if (p != null && p.getVideoFullScreenUrl() != null && p.getVideoFullScreenUrl().length() > 1) {
			Intent i = new Intent(appContext, MproxFullScreenVideo.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra(MproxFullScreenVideo.PRODUCT, p);
			i.putExtra(MproxFullScreenVideo.BEACON, b);
			appContext.startActivity(i);
		}
	}

	private void showMessage(final Product p, Beacon b) {
		if (p != null && p.getNotificationMessage() != null && p.getNotificationMessage().length() > 1) {
			Intent i = new Intent(appContext, MproxMessageActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra(MproxMessageActivity.MESSAGE, p.getNotificationMessage());
			i.putExtra(MproxMessageActivity.PRODUCT, p);
			i.putExtra(MproxMessageActivity.BEACON, b);
			appContext.startActivity(i);
		}
	}

}