package com.mprox.sdk.utils;

import android.content.Context;

public class Log {

	public static void i(Context context, String tag, String msg) {
		if (UtilsMprox.getInstance(context).isLogEnabled()) {
			android.util.Log.i(tag, msg);
		}
	}
	
	public static void v(Context context, String tag, String msg) {
		if (UtilsMprox.getInstance(context).isLogEnabled()) {
			android.util.Log.v(tag, msg);
		}
	}
	
	public static void d(Context context, String tag, String msg) {
		if (UtilsMprox.getInstance(context).isLogEnabled()) {
			android.util.Log.d(tag, msg);
		}
	}
	
	public static void e(Context context, String tag, String msg) {
		if (UtilsMprox.getInstance(context).isLogEnabled()) {
			android.util.Log.e(tag, msg);
		}
	}
	
	public static void w(Context context, String tag, String msg) {
		if (UtilsMprox.getInstance(context).isLogEnabled()) {
			android.util.Log.w(tag, msg);
		}
	}
	
	public static void wtf(Context context, String tag, String msg) {
		if (UtilsMprox.getInstance(context).isLogEnabled()) {
			android.util.Log.wtf(tag, msg);
		}
	}
}
