package com.mprox.sdk.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;
import com.mprox.sdk.MproxManager.AlertType;
import com.mprox.sdk.comunication.ConstsService;

public class UtilsMprox {

	private static final String MIDDLEWARE_PREFERENCES = "MIDDLEWARE_PREFERENCES";

	private SharedPreferences mSharedPreferences;
	private static UtilsMprox mInstance = null;

	private List<Beacon> topBeacons;
	private final int TOPBEACONSLISTSIZE = 4;

	public UtilsMprox(Context context) {
		mSharedPreferences = context.getSharedPreferences(context.getApplicationContext().getPackageName() + "-" + MIDDLEWARE_PREFERENCES,
				Context.MODE_PRIVATE);
	}

	public static UtilsMprox getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new UtilsMprox(context);
		}

		return mInstance;
	}

	private Editor getEditor() {
		return mSharedPreferences.edit();
	}

	// User ------------------------------------------------

	public void saveLogedUserEmail(String userEmail) {
		getEditor().putString("MPROX_USER_EMAIL", userEmail).commit();
	}

	public void saveLogedUserName(String userName) {
		getEditor().putString("MPROX_USER_NAME", userName).commit();
	}
	
	public void saveBluetoothAlertPeriod(Integer bluetoothAlertPeriod, AlertType alertType) {
		getEditor().putInt("MPROX_BLUETOOTH_ALERT_PERIOD", bluetoothAlertPeriod).commit();
		getEditor().putInt("MPROX_BLUETOOTH_ALERT_TYPE", alertType.ordinal()).commit();
	}
	
	public void saveBluetoothAlertMessage(String message) {
		getEditor().putString("MPROX_BLUETOOTH_ALERT_MESSAGE", message).commit();
	}
	
	public void saveLastBluetoothAlert(Long lastAlert) {
		getEditor().putLong("MPROX_LAST_BLUETOOTH_ALERT", lastAlert).commit();
	}

    public void saveLogedUserTeam(String team) {
        getEditor().putString("LOGED_USER_TEAM", team).commit();
    }
    public String getLogedUserTeam() {
        return mSharedPreferences.getString("LOGED_USER_TEAM", "");
    }

	public void saveBluetoothAlertCount(Integer lastAlert) {
		getEditor().putInt("MPROX_BLUETOOTH_ALERT_COUNT", lastAlert).commit();
	}

	public void saveLogedUserId(String userID) {
		getEditor().putString("MPROX_USER_ID", userID).commit();
	}
	
	public String getLogedUserEmail() {
		return mSharedPreferences.getString("MPROX_USER_EMAIL", "");
	}
	
	public String getBluetoothAlertMessage() {
		return mSharedPreferences.getString("MPROX_BLUETOOTH_ALERT_MESSAGE", 
				"Active el Bluetooth del dispositivo para comenzar a utilizar la aplicacion.");
	}
	
	public Integer getBluetoothAlertPeriod() {
		return mSharedPreferences.getInt("MPROX_BLUETOOTH_ALERT_PERIOD", 7);
	}
	
	public AlertType getBluetoothAlertType() {
		return AlertType.values()[mSharedPreferences.getInt("MPROX_BLUETOOTH_ALERT_TYPE", 0)];
	}
	
	public Long getLastBluetoothAlert() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, getBluetoothAlertPeriod() + 2);
		
		return mSharedPreferences.getLong("MPROX_LAST_BLUETOOTH_ALERT", cal.getTime().getTime());
	}
	public Integer getBluetoothAlertCount() {
		return mSharedPreferences.getInt("MPROX_BLUETOOTH_ALERT_COUNT", 0);
	}
	
	public String getLogedUserName() {
		return mSharedPreferences.getString("MPROX_USER_NAME", "");
	}
	
	public String getLogedUserId() {
		return mSharedPreferences.getString("MPROX_USER_ID", "0");
	}

	// Application ------------------------------------------------
	public void saveSDKMainKey(String mainKey) {
		getEditor().putString("MPROX_APPLICATION_MAIN_KEY", mainKey).commit();
	}

	public void saveSDKSecretKey(String secretKey) {
		getEditor().putString("MPROX_APPLICATION_SECRET_KEY", secretKey).commit();
	}

	public String getSDKMainKey() {
		return mSharedPreferences.getString("MPROX_APPLICATION_MAIN_KEY", "");
	}

	public String getSDKSecretKey() {
		return mSharedPreferences.getString("MPROX_APPLICATION_SECRET_KEY", "");
	}

	public static boolean isNullOrEmpty(String stringToCheck) {
		if (stringToCheck == null) {
			return true;
		}

		if (stringToCheck.length() == 0) {
			return true;
		}

		return false;
	}
	
	// Beacons ------------------------------------------------
	@SuppressLint("DefaultLocale") 
	public static String getFullBeaconID(Beacon beacon) {
		if (beacon != null)
			return beacon.getProximityUUID() + "-" + beacon.getMajor() + "-" + beacon.getMinor();
		else
			return null;
	}

	public Beacon getBeaconById(List<Beacon> beacons, String beaconID) {
		for (Beacon beacon : beacons) {
			String id = beacon.getProximityUUID() + "-" + beacon.getMajor() + "-" + beacon.getMinor();
			if (id.equalsIgnoreCase(beaconID)) {
				return beacon;
			}
		}
		return null;
	}

	public void sortBeaconByRssi(final List<Beacon> auxBeacon) {
		Collections.sort(auxBeacon, new Comparator<Beacon>() {
			@Override
			public int compare(Beacon lhs, Beacon rhs) {
				return compareBeaconRssi(lhs, rhs);
			}
		});
	}

	public int compareBeaconRssi(Beacon lhs, Beacon rhs) {
		if (Math.abs(lhs.getRssi()) < Math.abs(rhs.getRssi())) {
			return -1;
		} else if (Math.abs(lhs.getRssi()) < Math.abs(rhs.getRssi())) {
			return 1;
		} else {
			return 0;
		}
	}

	public void addTopBeacon(Beacon beacon) {
		if (topBeacons == null) {
			topBeacons = new ArrayList<Beacon>();
		}

		if (Utils.computeAccuracy(beacon) <= 15D) {
			topBeacons.add(0, beacon);
		}

		if (topBeacons.size() > TOPBEACONSLISTSIZE) {
			topBeacons.remove(TOPBEACONSLISTSIZE);
		}
	}

	public Beacon getMostFrecuentBeacon() {
		if (topBeacons != null && topBeacons.size() > 0) {
			Map<String, Integer> map = new HashMap<String, Integer>();
			for (Beacon beacon : topBeacons) {
				Integer val = map.get(getFullBeaconID(beacon));
				map.put(getFullBeaconID(beacon), val == null ? 1 : val + 1);
			}

			Entry<String, Integer> max = null;
			for (Entry<String, Integer> e : map.entrySet()) {
				if (max == null || e.getValue() > max.getValue())
					max = e;
			}

			Beacon nearest = getBeaconById(topBeacons, max.getKey());
			return nearest;
		}
		return null;
	}
	
	// Images ------------------------------------------------
	public static String formatImageUrl(String originalUrl) {
		try {
			int startSubString = originalUrl.indexOf("public_html/middlewarecache") + "public_html/middlewarecache/".length();

			String finalPart = originalUrl.substring(startSubString);

			return ConstsService.BASE_URL.concat(finalPart);
		} catch (Exception e) {
			return ConstsService.BASE_URL.concat(originalUrl);
		}
	}

	// Logs ------------------------------------------------
	public void enableLogs(boolean enable) {
		getEditor().putBoolean("ENABLE_LOGS", enable).commit();
		
	}
	
	public boolean isLogEnabled() {
		return mSharedPreferences.getBoolean("ENABLE_LOGS", false);
	}

}