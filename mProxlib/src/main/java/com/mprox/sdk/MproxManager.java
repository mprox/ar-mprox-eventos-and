/**
 * Copyright (c) 2015-present, Mprox, Inc. All rights reserved.
 */
package com.mprox.sdk;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.mprox.sdk.comunication.RegisterTask;
import com.mprox.sdk.comunication.UserBluetoothStatusTask;
import com.mprox.sdk.comunication.UserInformationTask;
import com.mprox.sdk.comunication.UserPreferencesTask;
import com.mprox.sdk.comunication.request.RegisterRequest;
import com.mprox.sdk.comunication.request.UserBluetoothStatusRequest;
import com.mprox.sdk.comunication.request.UserInformationRequest;
import com.mprox.sdk.comunication.request.UserPreferencesRequest;
import com.mprox.sdk.comunication.response.LoginResponse;
import com.mprox.sdk.service.BeaconRecognitionService;
import com.mprox.sdk.service.ProductsService;
import com.mprox.sdk.utils.Log;
import com.mprox.sdk.utils.UtilsMprox;

/**
 * This class represents an immutable access token for using Mprox APIs.
 */
public class MproxManager {
	public enum AlertType {
		ALERT_DAYS,
		ALERT_TIMES
	}

	private static MproxManager _instance;

	/**
	 * Creates a new MproxManager.
	 */
	static public MproxManager getInstance() {
		if (_instance == null) {
			_instance = new MproxManager();
		}
		return _instance;
	}

	/**
	 * Starts Mprox library leaving it ready to begin communication with the
	 * beacons.
	 *
	 * @param context the application context
	 * @param mainKey the mainKey given by Mprox.
	 * @param secretKey the secretKey given by Mprox.
	 */
	public void init(Context context, String mainKey, String secretKey) {

		try {
			// SHOW ALERT TO TURN ON BLUETOOTH
			boolean shouldAsk;
			AlertType bluetoothAlertType = UtilsMprox.getInstance(context).getBluetoothAlertType();
			if (bluetoothAlertType.equals(AlertType.ALERT_DAYS)) {
				Date lastBluetoothAlert = new Date(UtilsMprox.getInstance(context).getLastBluetoothAlert());
				shouldAsk = getDateDiff(new Date(), lastBluetoothAlert, TimeUnit.DAYS, context) >= UtilsMprox.getInstance(context).getBluetoothAlertPeriod()
						&& !isBluetoothOn();
			} else {
				Integer count = UtilsMprox.getInstance(context).getBluetoothAlertCount() + 1;
				shouldAsk = (count >= UtilsMprox.getInstance(context).getBluetoothAlertPeriod())
						&& !isBluetoothOn();
				UtilsMprox.getInstance(context).saveBluetoothAlertCount(count);
			}
			if (shouldAsk) {
				if (bluetoothAlertType.equals(AlertType.ALERT_DAYS)) {
					UtilsMprox.getInstance(context).saveLastBluetoothAlert(new Date().getTime());
				} else {
					UtilsMprox.getInstance(context).saveBluetoothAlertCount(0);
				}

				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage(UtilsMprox.getInstance(context).getBluetoothAlertMessage()).setCancelable(false)
						.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// do things
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}

			UtilsMprox.getInstance(context).saveSDKMainKey(mainKey);
			UtilsMprox.getInstance(context).saveSDKSecretKey(secretKey);

            bluetoothStatusReport(context);

			if (!BeaconRecognitionService.isRunning) {
				Log.i(context, "mprox-services", "MproxManager -- Beacon Recognition Service started");
				Intent serviceIntent = new Intent(context, BeaconRecognitionService.class);
				context.startService(serviceIntent);
			}

			if (!BeaconRecognitionService.isRunning) {
				Log.i(context, "mprox-services", "MproxManager -- Products Service started");
				Intent serviceIntent = new Intent(context, ProductsService.class);
				context.startService(serviceIntent);
			}

		} catch (Exception e) {
			Log.e(context, "mprox-services", e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Set configuration for bluetooth on alert. Same effect as calling 
	 * bluetoothAlertConfiguration(context, bluetoothAlertPeriod, message, AlertType.ALERT_DAYS)
	 *
	 * @param context the application context
	 * @param bluetoothAlertPeriod the appearance alert period to turn on
	 *            bluetooth on days (if null, default 7 days).
	 * 
	 */
	@Deprecated
	public void bluetoothAlertConfiguration(Context context, Integer bluetoothAlertPeriod, String message) {
		bluetoothAlertConfiguration(context, bluetoothAlertPeriod, message, AlertType.ALERT_DAYS);
	}
			
	/**
	 * Set configuration for bluetooth on alert.
	 *
	 * @param context the application context
	 * @param bluetoothAlertPeriod the appearance alert period to turn on
	 *            bluetooth on days (if null, default 7 days).
	 * @param message the message to show
	 * 
	 */
	public void bluetoothAlertConfiguration(Context context, Integer bluetoothAlertPeriod, String message, AlertType alertType) {
		if (bluetoothAlertPeriod != null) {
			UtilsMprox.getInstance(context).saveBluetoothAlertPeriod(bluetoothAlertPeriod, alertType);
		}

		if (message != null && message.length() > 1) {
			UtilsMprox.getInstance(context).saveBluetoothAlertMessage(message);
		}
	}

	/**
	 * Updates user data in Mprox system. Note that the caller is asserting that
	 * all parameters provided are correct; no validation is done to verify they
	 * are correct.
	 *
	 * @param context the application context
	 * @param name the new user name
	 * @param gender the new user gender [male,female]
	 * @param birthDay the new user birthday [dd/MM/yyyy]
	 * @param password the new user password
	 * @param email the new user email.
	 * @param erasePreferences should delete user preferences
	 */
	public void updateUserInformation(final Context context, String name, String gender, String birthDay, String password, String email,
			Boolean erasePreferences) {

		try {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			UserInformationRequest request = new UserInformationRequest(name, gender, format.format(birthDay), password, email,
					erasePreferences);
			UserInformationTask task = new UserInformationTask(context) {
				@Override
				public void onServiceReturned(LoginResponse response) {
					// Nothing to do.
				}
			};
			task.execute(request);

		} catch (Exception e) {
			Log.e(context, "mprox-services", e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Updates user preferences in Mprox system. Note that the caller is
	 * asserting that all parameters provided are correct; no validation is done
	 * to verify they are correct.
	 *
	 * @param context the application context
	 * @param preferences list of user preferences
	 */
	public void updateUserPreferences(final Context context, List<String> preferences) {
		try {
			String userId = UtilsMprox.getInstance(context).getLogedUserId();
			UserPreferencesRequest request = new UserPreferencesRequest(userId, preferences);
			UserPreferencesTask task = new UserPreferencesTask(context) {
				@Override
				public void onServiceReturned(LoginResponse response) {
					// Nothing to do.
				}
			};
			task.execute(request);

		} catch (Exception e) {
			Log.e(context, "mprox-services", e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Enables or disables development logs. For production releases logs should be disabled.
	 * @param enable
	 * @param context 
	 */
	public void enableLogs(boolean enable, Context context) {
		UtilsMprox.getInstance(context).enableLogs(enable);
	}
	
	private void performRegister(final Context context) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			RegisterRequest request = new RegisterRequest("-", "-", format.format(new Date()), "-", "-", "-");
			RegisterTask task = new RegisterTask(context) {
				@Override
				public void onServiceReturned(LoginResponse response) {
					Log.i(context, "mprox-services", "MproxManager -- Register Response [" + response.getData() + "]");
					if (response.isSuccess() && response.getData() != null) {

						UtilsMprox.getInstance(context).saveLogedUserEmail(response.getData().getEmail());
						UtilsMprox.getInstance(context).saveLogedUserName(response.getData().getName());
						UtilsMprox.getInstance(context).saveLogedUserId(String.valueOf(response.getData().getId()));

						bluetoothStatusReport(context);
					} else {
						Log.i(context, "mprox-services", response.getErrorMsessage());
					}
				}
			};

			task.execute(request);

		} catch (Exception e) {
			Log.e(context, "mprox-services", e.getMessage());
			e.printStackTrace();
		}
	}

	private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit, Context context) {
		try {
			if (date1 != null && date2 != null) {
				long diffInMillies = date2.getTime() - date1.getTime();
				return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
			} else {
				return 100;
			}
		} catch (Exception e) {
			Log.i(context, "mprox-services", "BeaconActivityDao -- GetDateDiff error [" + e.getMessage() + "]");
			return 0;
		}
	}

	private void bluetoothStatusReport(Context context) {
		try {
			boolean isBluetoothOn = isBluetoothOn();

			String logedUserId = UtilsMprox.getInstance(context).getLogedUserId();
			Log.e(context, "mprox-services", "User ID: " + logedUserId + ", isBluetoothOn: " + isBluetoothOn);
			UserBluetoothStatusRequest request = new UserBluetoothStatusRequest(logedUserId,
					isBluetoothOn, hasBluetooth(context));
			UserBluetoothStatusTask task = new UserBluetoothStatusTask(context) {
				public void onServiceReturned(LoginResponse response) {
				}
			};
			task.execute(request);

		} catch (Exception e) {
			Log.e(context, "mprox-services", e.getMessage());
			e.printStackTrace();
		}
	}

	private boolean isBluetoothOn() {

		boolean isBluetoothOn = false;
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter != null) {
			if (mBluetoothAdapter.isEnabled()) {
				isBluetoothOn = true;
			}
		}

		return isBluetoothOn;
	}

    public boolean hasBluetooth(Context context) {
        return context.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le");
    }
}
