package com.mprox.sdk.db.dao;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mprox.sdk.db.DataHelper;
import com.mprox.sdk.model.BeaconActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BeaconActivityDao {

	private Context mContext;

	public BeaconActivityDao(Context context) {
		mContext = context;
	}
	
	public void saveBeaconTrack(String beaconId, String productId){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			dataHelper.insert("beacon_track", "beaconUuid, productId, date", 
					"'" + beaconId + "'," + productId + "," + new Date().getTime());
			dataHelper.close();
			
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconTrack error [" + e.getMessage() + "]");
		}
	}
	
	public void saveBeaconTrack(List<BeaconActivity> beaconTracks){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			for (BeaconActivity beaconActivity : beaconTracks) {
				dataHelper.insert("beacon_track", "beaconUuid, productId, date",
						"'" + beaconActivity.getUuidBeacon() + "'," + beaconActivity.getIdProduct() + "," + beaconActivity.getDate());
			}
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconTrack error [" + e.getMessage() + "]");
		}
	}

	public void saveBeaconRecivedTrack(String beaconId, String productId){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			dataHelper.insert("beacon_recibe_track", "beaconUuid, productId, date", 
					"'" + beaconId + "'," + productId + "," + new Date().getTime());
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconRecivedTrack error [" + e.getMessage() + "]");
		}
	}
	
	public void saveBeaconRecivedTrack(List<BeaconActivity> beaconReciveTracks){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			for (BeaconActivity beaconActivity : beaconReciveTracks) {
				dataHelper.insert("beacon_recibe_track", "beaconUuid, productId, date",
						"'" + beaconActivity.getUuidBeacon() + "'," + beaconActivity.getIdProduct() + "," + beaconActivity.getDate());
			}
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconRecivedTrack error [" + e.getMessage() + "]");
		}
	}

	public void saveBeaconOpenTrack(String beaconId, String productId){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			dataHelper.insert("beacon_activity", "beaconUuid, productId, date", 
					"'" + beaconId + "'," + productId + "," + new Date().getTime());
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconOpenTrack error [" + e.getMessage() + "]");
		}
	}
	
	public void saveBeaconOpenTrack(List<BeaconActivity> beaconActivities){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			for (BeaconActivity beaconActivity : beaconActivities) {
				dataHelper.insert("beacon_activity", "beaconUuid, productId, date",
						"'" + beaconActivity.getUuidBeacon() + "'," + beaconActivity.getIdProduct() + "," + beaconActivity.getDate());
			}
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconOpenTrack error [" + e.getMessage() + "]");
		}
	}

	public void saveBeaconUserTrack(List<BeaconActivity> beaconUserTracks){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			for (BeaconActivity beaconActivity : beaconUserTracks) {
				dataHelper.insert("beacon_user_track", "beaconUuid, date, uptime", 
						"'" + beaconActivity.getUuidBeacon() + "'," + beaconActivity.getDate() + "," + beaconActivity.getUptime());			
			}
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconUserTrack error [" + e.getMessage() + "]");
		}
	}
	

	public void saveBeaconUserTrack(String beaconId){
		try{
            DataHelper dataHelper = new DataHelper(mContext);
			
			String[] savedData = getBeaconUserTrack();
			String lastBeaconId = savedData[0];
			String lastDate =  savedData[1];
			String idRow =  savedData[2];
			String update =  savedData[3];
			
			if(update == null || update.equals("")){
				update = "0";
			}
			
			if(lastDate != null && lastDate.length() > 1 && lastBeaconId.equalsIgnoreCase(beaconId) 
					&& getDateDiff(new Date(Long.valueOf(lastDate) + (Long.valueOf(update) * 1000)), new Date(), TimeUnit.MINUTES) < 5){
				
				List<String> columns = new ArrayList<String>();
				columns.add("uptime");
				
				List<String> data = new ArrayList<String>();
				data.add(String.valueOf(getDateDiff(new Date(Long.valueOf(lastDate)), new Date(), TimeUnit.SECONDS)));
				dataHelper.editItem("beacon_user_track", "_id", idRow, columns, data);
				dataHelper.close();
				
			}else{
				dataHelper.insert("beacon_user_track", "beaconUuid, date, uptime", 
						"'" + beaconId + "'," + new Date().getTime() + ",1");
			}
			
			dataHelper.close();
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- SaveBeaconUserTrack error [" + e.getMessage() + "]");
		}
	}
	
	private String[] getBeaconUserTrack(){
		try{
			String idRow = "";
			String idBeacon = "";
			String date = "";
			String update = "";
			
			DataHelper dataHelper = new DataHelper(mContext);
			Cursor cursor = dataHelper.selectAll("beacon_user_track", "date DESC");
			if (cursor.moveToFirst()) {
				do {
					if(idBeacon.equals("")){
						idRow = cursor.getString(cursor.getColumnIndex("_id"));
						idBeacon = cursor.getString(cursor.getColumnIndex("beaconUuid"));
						date = cursor.getString(cursor.getColumnIndex("date"));
						update = cursor.getString(cursor.getColumnIndex("uptime"));
					}
				} while (cursor.moveToNext());
			}
	
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			dataHelper.close();
			
			String[] data = new String[4];
			data[0] = idBeacon;
			data[1] = date;
			data[2] = idRow;
			data[3] = update;
			
			return data;
		
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- GetBeaconUserTrack error [" + e.getMessage() + "]");
			return null;
		}
	}
	
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		try{
			if(date1 != null && date2 != null){
				long diffInMillies = date2.getTime() - date1.getTime();
				return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
			}else{
				return 100;
			}
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- GetDateDiff error [" + e.getMessage() + "]");
			return 0;
		}
	}
	
	public List<BeaconActivity> getBeaconTrackList(){
		List<BeaconActivity> beacon_track = new ArrayList<BeaconActivity>();
		try{
			DataHelper dataHelper = new DataHelper(mContext);
			Cursor cursor = dataHelper.selectAll("beacon_track", null);
			if (cursor.moveToFirst()) {
				do {
					BeaconActivity beaconActivity = new BeaconActivity();
					beaconActivity.setUuidBeacon(cursor.getString(cursor.getColumnIndex("beaconUuid")));
					beaconActivity.setIdProduct(cursor.getLong(cursor.getColumnIndex("productId")));
					beaconActivity.setDate(cursor.getString(cursor.getColumnIndex("date")));

					beacon_track.add(beaconActivity);

				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}

			dataHelper.deleteAll("beacon_track");
			dataHelper.close();


		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- GetBeaconTrackList error [" + e.getMessage() + "]");
		}

		return beacon_track;
	}

	public List<BeaconActivity> getBeaconReciveTrackList(){
		List<BeaconActivity> beacon_track = new ArrayList<BeaconActivity>();
		try{
			DataHelper dataHelper = new DataHelper(mContext);
			Cursor cursor = dataHelper.selectAll("beacon_recibe_track", null);
			if (cursor.moveToFirst()) {
				do {
					BeaconActivity beaconActivity = new BeaconActivity();
					beaconActivity.setUuidBeacon(cursor.getString(cursor.getColumnIndex("beaconUuid")));
					beaconActivity.setIdProduct(cursor.getLong(cursor.getColumnIndex("productId")));
					beaconActivity.setDate(cursor.getString(cursor.getColumnIndex("date")));

					beacon_track.add(beaconActivity);

				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			dataHelper.deleteAll("beacon_recibe_track");
			dataHelper.close();

		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- GetBeaconReciveTrackList error [" + e.getMessage() + "]");
		}

		return beacon_track;
	}

	public List<BeaconActivity> getBeaconActivityList(){
		List<BeaconActivity> beacon_track = new ArrayList<BeaconActivity>();
		try{
			DataHelper dataHelper = new DataHelper(mContext);
			Cursor cursor = dataHelper.selectAll("beacon_activity", null);
			if (cursor.moveToFirst()) {
				do {
					BeaconActivity beaconActivity = new BeaconActivity();
					beaconActivity.setUuidBeacon(cursor.getString(cursor.getColumnIndex("beaconUuid")));
					beaconActivity.setIdProduct(cursor.getLong(cursor.getColumnIndex("productId")));
					beaconActivity.setDate(cursor.getString(cursor.getColumnIndex("date")));

					beacon_track.add(beaconActivity);

				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			dataHelper.deleteAll("beacon_activity");
			dataHelper.close();

		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- GetBeaconActivityList error [" + e.getMessage() + "]");
		}

		return beacon_track;
	}

	public List<BeaconActivity> getBeaconUserTrackList(){
		List<BeaconActivity> beacon_track = new ArrayList<BeaconActivity>();
		try{
			DataHelper dataHelper = new DataHelper(mContext);
			Cursor cursor = dataHelper.selectAll("beacon_user_track", null);
			if (cursor.moveToFirst()) {
				do {
					BeaconActivity beaconActivity = new BeaconActivity();
					beaconActivity.setUuidBeacon(cursor.getString(cursor.getColumnIndex("beaconUuid")));
					beaconActivity.setUptime(cursor.getString(cursor.getColumnIndex("uptime")));
					beaconActivity.setDate(cursor.getString(cursor.getColumnIndex("date")));
					
					beacon_track.add(beaconActivity);
					
				} while (cursor.moveToNext());
			}
	
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			dataHelper.deleteAll("beacon_user_track");
			dataHelper.close();
		
		}catch(Exception e){
			Log.i("mprox-services", "BeaconActivityDao -- GetBeaconUserTrackList error [" + e.getMessage() + "]");
		}
		
		return beacon_track;
	}
}
