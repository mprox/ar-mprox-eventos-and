package com.mprox.sdk.db;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DataHelper{
	
	private static final String DATABASE_NAME = "mProxSDK.db";
	private static final int DATABASE_VERSION = 1;
	 
	private Context context;
	private SQLiteDatabase db;
 
   public DataHelper(Context context) {
      this.context = context;
      SQLiteHelper openHelper = new SQLiteHelper(this.context, context.getApplicationContext().getPackageName() + "-" + DATABASE_NAME, 
    		  null, DATABASE_VERSION);
      this.db = openHelper.getWritableDatabase();
   }
 
   public void insert(String tableName, String colums, String data) {
	   db.execSQL("INSERT INTO " + tableName + "(" + colums + ")" +
               "VALUES (" + data +")");
   }
 
   public void deleteAll(String tableName) {
      db.delete(tableName, null, null);
   }
   
   
   public void deleteItem(String tableName, String column, String value) {
	   String query = "DELETE FROM " + tableName + " WHERE " + column + " = " + value;
	   db.execSQL(query);
   }
   
   public void editItem(String tableName, String columnUpdateName, String id, List<String> colums, List<String> data) {
	   String query = "UPDATE " + tableName + " SET ";
	   
	   for(int i=0;i <colums.size(); i++){
		   query = query +   colums.get(i) + " = '" + data.get(i) + "',";
	   }
	   
	   //Quito la ultima coma
	   query = query.substring(0, query.length()-1);
	   
	   query = query + " WHERE " + columnUpdateName + " = " + id;
	   
	   db.execSQL(query);
   }
   
   public void editItemByString(String tableName, String columnUpdateName, String id, List<String> colums, List<String> data) {
	   String query = "UPDATE " + tableName + " SET ";
	   
	   for(int i=0;i <colums.size(); i++){
		   query = query +   colums.get(i) + " = '" + data.get(i) + "',";
	   }
	   
	   //Quito la ultima coma
	   query = query.substring(0, query.length()-1);
	   
	   query = query + " WHERE " + columnUpdateName + " = '" + id + "'";
	   
	   db.execSQL(query);
   }
   
   public void editColumItem(String tableName, String id, String colum, String data) {
	   ContentValues nuevoRegistro = new ContentValues();
	   nuevoRegistro.put(colum, data);
   }
   
   public Cursor selectAll(String tableName, String order) {
	  Cursor cursor = this.db.query(tableName, null,
    		  null, null, null, null, order);
      
      return cursor;
   }
   
   public Cursor selectItems(String tableName, String[] colums, String where) {
	  Cursor cursor = this.db.query(tableName, colums,
    		  where, null, null, null, null);
      
      return cursor;
   }
   
   public Cursor selectItemsBy(String tableName, String where, String[] whereArgs) {
	  Cursor cursor = this.db.query(tableName, null,
    		  where, whereArgs, null, null, null);
      
      return cursor;
   }
   
   public Cursor selectItem(String tableName, String[] colums, String order) {
	   Cursor cursor = this.db.query(tableName, colums,
	    		  null, null, null, null, null);
	      
	   return cursor;

   }
   
   public void close(){
	   if (db != null)
	         db.close();
   }
}