package com.mprox.sdk.db.sql;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.util.Log;

public class Tables{
	
	private Context mContext;
	private List<String> tables = new ArrayList<String>();
	
	public Tables(Context context){
		mContext = context;
		loadData();
	}
		
	/**
	 * @param 
	 * @return List<String>
	 */
	public List<String> getTables(){
		return this.tables;
	}
	
	/**
	 * @param table (sql de creacion de tabla)
	 * @return void
	 */
	public void addTable(String table){
		this.tables.add(table);
	}
	
	
	/**
	 * Carga de las tables que se agregan en el sql.xml
	 * 
	 * @param 
	 * @return void
	 */
	private void loadData(){
		NodeList 		 auxNodeList;
		org.w3c.dom.Node auxNode;
		
		try{
			
			BufferedReader br = new BufferedReader(new InputStreamReader(mContext.getAssets().open("sql.xml")));
			String entry;
			String chars="";
	
			while ((entry = br.readLine()) != null){
				chars = chars + entry;
			}
			
			br.close();
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
	
			InputSource file = new InputSource();
			file.setCharacterStream(new StringReader(chars)); 
	
			Document documento = db.parse(file);
			documento.getDocumentElement().normalize();
	
			NodeList finds = documento.getElementsByTagName("Table");
			
			for (int i = 0; i < finds.getLength(); i++) {
				Element nodoItem = (Element) finds.item(i);
				
				auxNodeList = nodoItem.getElementsByTagName("Sql");
				auxNode = auxNodeList.item(0).getFirstChild();
				if(auxNode!=null) this.tables.add(auxNode.getNodeValue());
				
			}
		}catch (Exception e) {
			Log.i("mprox-services", "Tables -- loadData error [" + e.getMessage() + "]");
		}
	}	
}