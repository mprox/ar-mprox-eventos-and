package com.mprox.sdk.db.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.ImageView;

import com.mprox.sdk.db.DataHelper;
import com.mprox.sdk.model.Product;
import com.mprox.sdk.utils.UtilsMprox;
import com.squareup.picasso.Picasso;

public class ProductDao {

	private Context mContext;
	private Product product;

	public ProductDao(Context context) {
		mContext = context;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void deleteAll() {
		DataHelper dataHelper = new DataHelper(mContext);

		dataHelper.deleteAll("product_beacon");
		dataHelper.deleteAll("product");
		dataHelper.close();
	}

	public void updateTotalSendedToUserQuantity() {
		try {
			int totalSendedToUserQuantity = product.getTotalSendedToUserQuantity();
			totalSendedToUserQuantity++;
			product.setTotalSendedToUserQuantity(totalSendedToUserQuantity);

			updateProduct();

		} catch (Exception e) {
			Log.i("mprox-services", "ProductDao -- UpdateTotalSendedToUserQuantity error [" + e.getMessage() + "]");
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("SimpleDateFormat")
	public void updateDaySendedToUserQuantity() {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			if (product.getLastNotificationSendedTime() == null || product.getLastNotificationSendedTime().equalsIgnoreCase("0")) {
				product.setLastNotificationSendedTime(dateFormat.format(new Date()));
			}

			Date lasNotificationDate = dateFormat.parse(product.getLastNotificationSendedTime());
			Date now = new Date();
			if (lasNotificationDate.getDay() == now.getDay() && lasNotificationDate.getMonth() == now.getMonth()) {
				int daySendedToUserQuantity = product.getDaySendedToUserQuantity();
				daySendedToUserQuantity++;
				product.setDaySendedToUserQuantity(daySendedToUserQuantity);
			} else {
				product.setDaySendedToUserQuantity(1);
			}

			product.setLastNotificationSendedTime(dateFormat.format(new Date()));
			updateProduct();
		} catch (Exception e) {
			Log.i("mprox-services", "ProductDao -- UpdateDaySendedToUserQuantity error [" + e.getMessage() + "]");
		}
	}

	public void updateProduct() {
		try {
			List<String> columns = new ArrayList<String>();
			columns.add("categoryId");
			columns.add("category");
			columns.add("productType");
			columns.add("title");
			columns.add("message");
			columns.add("description");
			columns.add("imageUrl");
			columns.add("isPriority");
			columns.add("isHighlight");
			columns.add("imageFullScreenUrl");
			columns.add("videoFullScreenUrl");
			columns.add("photoPromo");
			columns.add("photoQR");
			columns.add("timeToWait");
			columns.add("startDate");
			columns.add("endDate");
			columns.add("showAfter");
			columns.add("showBefore");
			columns.add("daysOfWeek");
			columns.add("starHourToShow");
			columns.add("endHourToShow");
			columns.add("frequencyToShow");
			columns.add("datefrequencyToShow");
			columns.add("limitForUser");
			columns.add("daySendedToUserQuantity");
			columns.add("lastNotificationSendedTime");

			List<String> data = new ArrayList<String>();
			data.add(product.getCategoryId().toString());
			data.add(product.getCategory());
			data.add(String.valueOf(product.getProductType()));
			data.add(product.getTitle());
			data.add(product.getNotificationMessage());
			data.add(product.getDescription());
			data.add(product.getImageUrl());
			data.add(String.valueOf(product.isPriority()));
			data.add(String.valueOf(product.isHighlight()));
			data.add(product.getImageFullScreenUrl());
			data.add(product.getVideoFullScreenUrl());
			data.add(product.getPhotoPromo());
			data.add(product.getPhotoQR());
			data.add(product.getTimeToWait());
			data.add(product.getStartDate());
			data.add(product.getEndDate());
			data.add(String.valueOf(product.isShowAfter()));
			data.add(String.valueOf(product.isShowBefore()));
			data.add(product.getDaysOfWeek());
			data.add(product.getStarHourToShow());
			data.add(product.getEndHourToShow());
			data.add(String.valueOf(product.getFrequencyToShow()));
			data.add(String.valueOf(product.getDatefrequencyToShow()));
			data.add(String.valueOf(product.getLimitForUser()));
			data.add(String.valueOf(product.getDaySendedToUserQuantity()));
			data.add(product.getLastNotificationSendedTime());

			DataHelper dataHelper = new DataHelper(mContext);
			dataHelper.editItem("product", "idProduct", String.valueOf(product.getId()), columns, data);
			dataHelper.close();

		} catch (Exception e) {
			Log.i("mprox-services", "ProductDao -- UpdateProduct error [" + e.getMessage() + "]");
		}
	}

	public void save() {
		try {
			if (product.getBeaconsIds() != null && !product.getBeaconsIds().equalsIgnoreCase("null")) {

				DataHelper dataHelper = new DataHelper(mContext);
				dataHelper
						.insert("product",
								"idProduct, categoryId, category, productType, title, message, description, imageUrl, isPriority, isHighlight, "
										+ "imageFullScreenUrl, videoFullScreenUrl, photoPromo, photoQR, tags, timeToWait, startDate, endDate, showAfter, showBefore, "
										+ "daysOfWeek, starHourToShow, endHourToShow, frequencyToShow, datefrequencyToShow, limitForUser, "
										+ "daySendedToUserQuantity, totalSendedToUserQuantity, lastNotificationSendedTime",
								product.getId() + "," + product.getCategoryId() + ",'" + product.getCategory() + "'" + ","
										+ product.getProductType() + ",'" + product.getTitle() + "'" + ",'"
										+ product.getNotificationMessage() + "'" + ",'" + product.getDescription() + "'" + ",'"
										+ product.getImageUrl() + "'" + ",'" + product.isPriority() + "'" + ",'" + product.isHighlight()
										+ "'" + ",'" + product.getImageFullScreenUrl() + "'" + ",'" + product.getImageFullScreenUrl() + "'"
										+ ",'" + product.getPhotoPromo() + "'" + ",'" + product.getPhotoQR() + "'" + ",' '" + ","
										+ product.getTimeToWait() + ",'" + product.getStartDate() + "'" + ",'" + product.getEndDate() + "'"
										+ ",'" + product.isShowAfter() + "'" + ",'" + product.isShowBefore() + "'" + ",'"
										+ product.getDaysOfWeek() + "'" + ",'" + product.getStarHourToShow() + "'" + ",'"
										+ product.getEndHourToShow() + "'" + "," + product.getFrequencyToShow() + ","
										+ product.getDatefrequencyToShow() + "," + product.getLimitForUser() + ","
										+ product.getDaySendedToUserQuantity() + "," + product.getTotalSendedToUserQuantity() + ",'"
										+ product.getLastNotificationSendedTime() + "'");

				if (product.getBeaconsIds() != null && product.getBeaconsIds().contains("|")) {
					String[] beaconIds = product.getBeaconsIds().split("\\|");
					for (String id : beaconIds) {
						dataHelper.insert("product_beacon", "idBeacon, idProduct", "'" + id + "'," + product.getId());
					}
				} else {
					dataHelper.insert("product_beacon", "idBeacon, idProduct", "'" + product.getBeaconsIds() + "'," + product.getId());
				}

				dataHelper.close();
			}

			if (product.getImageUrl() != null) {
				ImageView picture = new ImageView(mContext);
				Picasso.with(mContext).load(UtilsMprox.formatImageUrl(product.getImageUrl())).into(picture);
			}
		} catch (Exception e) {
			Log.i("mprox-services", "ProductDao -- Save error [" + e.getMessage() + "]");
		}
	}

	public Product getProduct(String beaconId, int id) {
		try {
			DataHelper dataHelper = new DataHelper(mContext);
			String[] args = new String[1];
			args[0] = String.valueOf(id);
			Cursor cursor = dataHelper.selectItemsBy("product", "idProduct = ?", args);
			if (cursor.moveToFirst()) {
				do {
					product = new Product();
					product.setId(cursor.getInt(cursor.getColumnIndex("idProduct")));
					product.setProductType(cursor.getInt(cursor.getColumnIndex("productType")));
					product.setCategoryId(cursor.getInt(cursor.getColumnIndex("categoryId")));
					product.setCategory(cursor.getString(cursor.getColumnIndex("category")));
					product.setNotificationMessage(cursor.getString(cursor.getColumnIndex("message")));
					product.setTitle(cursor.getString(cursor.getColumnIndex("title")));
					product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
					product.setImageUrl(cursor.getString(cursor.getColumnIndex("imageUrl")));
					product.setPriority(Boolean.getBoolean(cursor.getString(cursor.getColumnIndex("isPriority"))));
					product.setStartDate(cursor.getString(cursor.getColumnIndex("startDate")));
					product.setTimeToWait(cursor.getString(cursor.getColumnIndex("timeToWait")));
					product.setEndDate(cursor.getString(cursor.getColumnIndex("endDate")));
					product.setShowBefore(Boolean.getBoolean(cursor.getString(cursor.getColumnIndex("showBefore"))));
					product.setShowAfter(Boolean.getBoolean(cursor.getString(cursor.getColumnIndex("showAfter"))));
					product.setHighlight(Boolean.getBoolean(cursor.getString(cursor.getColumnIndex("isHighlight"))));
					product.setImageFullScreenUrl(cursor.getString(cursor.getColumnIndex("imageFullScreenUrl")));
					product.setVideoFullScreenUrl(cursor.getString(cursor.getColumnIndex("videoFullScreenUrl")));
					product.setPhotoPromo(cursor.getString(cursor.getColumnIndex("photoPromo")));
					product.setPhotoQR(cursor.getString(cursor.getColumnIndex("photoQR")));
					product.setDaysOfWeek(cursor.getString(cursor.getColumnIndex("daysOfWeek")));
					product.setStarHourToShow(cursor.getString(cursor.getColumnIndex("starHourToShow")));
					product.setEndHourToShow(cursor.getString(cursor.getColumnIndex("endHourToShow")));
					product.setFrequencyToShow(cursor.getInt(cursor.getColumnIndex("frequencyToShow")));
					product.setDatefrequencyToShow(cursor.getInt(cursor.getColumnIndex("datefrequencyToShow")));
					product.setLimitForUser(cursor.getInt(cursor.getColumnIndex("limitForUser")));
					product.setDaySendedToUserQuantity(cursor.getInt(cursor.getColumnIndex("daySendedToUserQuantity")));
					product.setTotalSendedToUserQuantity(cursor.getInt(cursor.getColumnIndex("totalSendedToUserQuantity")));
					product.setLastNotificationSendedTime(cursor.getString(cursor.getColumnIndex("lastNotificationSendedTime")));

				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			dataHelper.close();

			if (getProductToShow(product)) {
				BeaconActivityDao beaconDao = new BeaconActivityDao(mContext);
				beaconDao.saveBeaconTrack(beaconId, String.valueOf(id));
				return product;
			} else {
				return null;
			}
		} catch (Exception e) {
			Log.i("mprox-services", "ProductDao -- GetProduct error [" + e.getMessage() + "]");
			return null;
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("SimpleDateFormat")
	private boolean getProductToShow(Product product) {
		try {

			boolean isSpam = false;
			boolean sendNot = true;

			if (product.getLimitForUser() != null && product.getLimitForUser() > 0 && !isSpam) {
				Integer cantNotToUser = product.getTotalSendedToUserQuantity();
				if (cantNotToUser > product.getLimitForUser()) {
					isSpam = true;
				}
			}

			if (product.getDatefrequencyToShow() != null && product.getDatefrequencyToShow() > 0 && !isSpam) {
				Integer datefrequencyToShow = product.getDatefrequencyToShow();
				Integer datefrequencyToday = product.getDaySendedToUserQuantity();
				if (datefrequencyToShow <= datefrequencyToday) {
					isSpam = true;
				}
			}

			if (product.getFrequencyToShow() != null && product.getFrequencyToShow() > 0 && !isSpam) {
				String lastNotTime = product.getLastNotificationSendedTime();
				if (lastNotTime != null && lastNotTime.length() > 0 && !lastNotTime.equalsIgnoreCase("null")
						&& !lastNotTime.equalsIgnoreCase("0")) {
					Date now = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date notDate = sdf.parse(lastNotTime);
					long diffInMillies = Math.abs(now.getTime() - notDate.getTime());
					Long minutosDiff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
					if (minutosDiff < product.getFrequencyToShow()) {
						isSpam = true;
					}
				}
			}

			if (!isSpam) {
				Calendar c = Calendar.getInstance();
				Date today = c.getTime();

				if (product.getStartDate() != null && product.getStartDate().length() > 0
						&& !product.getStartDate().equalsIgnoreCase("null")) {
					c.setTime(new Date(product.getStartDate()));
					Date startDate = c.getTime();

					if ((!product.isShowAfter()) && (startDate.after(today))) {
						sendNot = false;
					}
				}

				if (product.getEndDate() != null && product.getEndDate().length() > 0 && !product.getEndDate().equalsIgnoreCase("null")) {
					c.setTime(new Date(product.getEndDate()));
					Date endDate = c.getTime();

					if ((!product.isShowBefore()) && (endDate.before(today))) {
						sendNot = false;
					}
				}

				c = Calendar.getInstance();
				Integer dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

				if (product.getDaysOfWeek() != null && product.getDaysOfWeek().equals("") && !product.getDaysOfWeek().isEmpty()
						&& !product.getDaysOfWeek().contains(dayOfWeek.toString())) {
					sendNot = false;
				}

				Date now = c.getTime();
				if (product.getStarHourToShow() != null && product.getStarHourToShow().length() > 0
						&& !product.getStarHourToShow().equalsIgnoreCase("null")) {

					String[] hourAndMinutes = product.getStarHourToShow().split(":");
					c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), Integer.parseInt(hourAndMinutes[0]),
							Integer.parseInt(hourAndMinutes[0]));
					Date starHourToShow = c.getTime();

					if (now.before(starHourToShow)) {
						sendNot = false;
					}
				}

				if (product.getEndHourToShow() != null && product.getEndHourToShow().length() > 0
						&& !product.getEndHourToShow().equalsIgnoreCase("null")) {

					String[] hourAndMinutes = product.getEndHourToShow().split(":");
					c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), Integer.parseInt(hourAndMinutes[0]),
							Integer.parseInt(hourAndMinutes[0]));
					Date endHourToShow = c.getTime();
					if (now.after(endHourToShow)) {
						sendNot = false;
					}
				}
			}

			// check tags.
			// boolean tags = false;
			// if (product.getTags() != null && product.getTags().length() > 0)
			// {
			// String userTags = this.userDataService
			// .getByUserTagsDeviceAndApp(device.getUuid(), device
			// .getApplication().getId());
			//
			// if (userTags != null && userTags.length() > 0) {
			// List<String> userTagList = Arrays.asList(userTags
			// .split(";"));
			// for (String userTag : userTagList) {
			// if (product.getTags().contains(userTag)) {
			// tags = true;
			// break;
			// }
			// }
			// } else {
			// tags = true;
			// }
			// } else {
			// tags = true;
			// }

			if (!isSpam && sendNot) {
				return true;
			}

		} catch (Exception e) {
			Log.i("mprox-services", "Error checking if product is available to show: " + e.getMessage());
		}

		return false;
	}

}
