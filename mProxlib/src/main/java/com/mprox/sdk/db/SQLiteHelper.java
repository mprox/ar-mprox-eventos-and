package com.mprox.sdk.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.mprox.sdk.db.sql.Tables;


public class SQLiteHelper extends SQLiteOpenHelper{

	private Context mContext;

	public SQLiteHelper(Context contexto, String nombre, CursorFactory factory, int version){
        super(contexto, nombre, factory, version);
        mContext = contexto;
    }
 
    @Override
    public void onCreate(SQLiteDatabase db){
        /** Se ejecutan las sentencia SQL de creacion de la tablas guardadas en la
         * clase Tables
         */
    	Tables tables = new Tables(mContext);
        for(int i=0;i< tables.getTables().size();i++){
        	db.execSQL(tables.getTables().get(i));
        }
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {}
    
}