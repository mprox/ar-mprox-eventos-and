package com.mprox.sdk.db.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.mprox.sdk.db.DataHelper;

public class BeaconProductDao {

	private Context mContext;

	public BeaconProductDao(Context context) {
		mContext = context;
	}

	public List<Integer> getProductsForBeacon(String idBeacon) {
		List<Integer> productsIds = new ArrayList<Integer>();
		try{	
			DataHelper dataHelper = new DataHelper(mContext);
			String[] args = new String[1];
			args[0] = String.valueOf(idBeacon);
			Cursor cursor = dataHelper.selectItemsBy("product_beacon",
					"idBeacon = ?", args);
			if (cursor.moveToFirst()) {
				do {
					productsIds.add(cursor.getInt(cursor.getColumnIndex("idProduct")));
				} while (cursor.moveToNext());
			}
	
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			dataHelper.close();
	
			
		}catch(Exception e){
			Log.i("mprox-services", "BeaconProductDao -- GetProductsForBeacon error [" + e.getMessage() + "]");
		}
		return productsIds;
	}
}
