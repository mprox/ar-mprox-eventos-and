package com.mprox.sdk.comunication.request;

public class ProdutsByLocationRequest {

	private Long userId;
	private double latitude;
	private double longitude;
	
	/**
	 * @param deviceId
	 * @param latitude
	 * @param longitude
	 */
	public ProdutsByLocationRequest(double latitude,
	                                double longitude,
	                                Long userId) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.userId = userId;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

}
