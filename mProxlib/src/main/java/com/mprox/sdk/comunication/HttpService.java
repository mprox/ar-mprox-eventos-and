package com.mprox.sdk.comunication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;

import android.content.Context;
import android.util.Base64;

import com.mprox.sdk.utils.UtilsMprox;

public class HttpService {

	public static final String TAG = "Http service";

	public String post(Context context, String url, JSONObject params) {

		URL urlURL;

		try {
			urlURL = new URL(url);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + url);
		}

		byte[] bBody = params.toString().getBytes();
		String body = Base64.encodeToString(bBody, Base64.DEFAULT);
		bBody = body.getBytes();

		HttpURLConnection conn = null;
		try {
			
			conn = (HttpURLConnection) urlURL.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bBody.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "text/plain");
			conn.setRequestProperty(
					"Authorization",
					"Basic "
							+ Base64.encodeToString((UtilsMprox.getInstance(context).getSDKMainKey() + ":" + UtilsMprox
									.getInstance(context).getSDKSecretKey()).getBytes(), Base64.NO_WRAP));

			OutputStream out = conn.getOutputStream();
			out.write(bBody);
			out.close();

			int status = conn.getResponseCode();
			if (status != 200) {
				return null;
			}
			return streamToString(conn.getInputStream());

		} catch (Exception e) {
			e.printStackTrace();
			// nothing
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}

	private String streamToString(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}
}
