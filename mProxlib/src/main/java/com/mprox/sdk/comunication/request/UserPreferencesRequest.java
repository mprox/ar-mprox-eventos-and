package com.mprox.sdk.comunication.request;

import java.util.List;

public class UserPreferencesRequest {

	private String preferences;
	private String userId;

	public UserPreferencesRequest(String userId, List<String> preferences) {
		this.userId = userId;

		String userPreferences = new String("");
		for (String p : preferences) {
			userPreferences.concat(p + "|");
		}

		this.preferences = userPreferences.substring(0, userPreferences.length() - 1);
	}

	public String getPreferences() {
		return preferences;
	}

	public String getUserId() {
		return userId;
	}

}
