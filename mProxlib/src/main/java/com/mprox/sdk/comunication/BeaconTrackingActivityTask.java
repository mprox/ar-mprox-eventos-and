package com.mprox.sdk.comunication;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.sdk.comunication.request.BeaconTrackingActivityRequest;
import com.mprox.sdk.comunication.response.GetProductsResponse;
import com.mprox.sdk.model.BeaconActivity;

public abstract class BeaconTrackingActivityTask extends AsyncTask<BeaconTrackingActivityRequest, Void, String> {
	
	private static final String TRACKING_URL = ConstsService.PROD_BASE_URL + "/sdk/activity/track";
	
	private Context mContext;
	
	public BeaconTrackingActivityTask(Context context) {
		super();
		this.mContext = context;
	}
	
	@Override
	protected String doInBackground(BeaconTrackingActivityRequest... params) {
		String response = "";

		JSONObject serviceParams = new JSONObject();

		BeaconTrackingActivityRequest request = params[0];

		try {
			serviceParams.put("userId", String.valueOf(request.getUserId()));
			serviceParams.put("identifier", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));
			
			JSONArray jsonBeaconUserTrack = new JSONArray();
			for (BeaconActivity activity : request.getBeacon_user_track()) {
				JSONObject json = new JSONObject();
				json.put("uuidBeacon", activity.getUuidBeacon());
				json.put("date", activity.getDate());
				json.put("uptime", activity.getUptime());
				jsonBeaconUserTrack.put(json);
			}
			serviceParams.put("beacon_user_track", jsonBeaconUserTrack);

			response = new HttpService().post(mContext, TRACKING_URL, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}
		if (response != null) {
			Log.i("SearService Task", response);
		}
		
		return response;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		GetProductsResponse response = new GetProductsResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, GetProductsResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(GetProductsResponse response);

}
