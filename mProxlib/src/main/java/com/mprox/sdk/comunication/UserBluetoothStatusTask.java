package com.mprox.sdk.comunication;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mprox.sdk.comunication.request.UserBluetoothStatusRequest;
import com.mprox.sdk.comunication.response.LoginResponse;

public abstract class UserBluetoothStatusTask extends AsyncTask<UserBluetoothStatusRequest, Void, String> {

	private Context context;

	public UserBluetoothStatusTask(Context context) {
		super();
		this.context = context;
	}

	@Override
	protected String doInBackground(UserBluetoothStatusRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/user/updateUserBT";
		JSONObject serviceParams = new JSONObject();

		UserBluetoothStatusRequest request = params[0];

		try {
			serviceParams.put("userId", request.getUserId());
			serviceParams.put("bluetoothOn", request.getBluetoothOn());
            serviceParams.put("bluetoothSupported", request.isBluetoothSupported());

			response = new HttpService().post(this.context, baseUrl, serviceParams);

		} catch (Exception e) {
			response = "exeption during service execution";
		}
		Log.i("Register Task", "" + response);
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		LoginResponse response = new LoginResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, LoginResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(LoginResponse response);
}