package com.mprox.sdk.comunication.request;

public class UserInformationRequest {

	private String gender;
	private String birthDay;
	private String name;
	private String password;
	private String email;
	private Boolean erasePreferences;

	public UserInformationRequest(String name, String gender, String birthDay, String password, String email, Boolean erasePreferences) {

		this.name = name;
		this.birthDay = birthDay;
		this.gender = gender;
		this.password = password;
		this.email = email;
		this.erasePreferences = erasePreferences;
	}

	public String getGender() {
		return gender;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public Boolean getErasePreferences() {
		return erasePreferences;
	}

}
