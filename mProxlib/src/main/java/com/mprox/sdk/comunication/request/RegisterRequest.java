package com.mprox.sdk.comunication.request;

public class RegisterRequest {

	private String gender;
	private String birthDay;
	private String name;
	private String password;
	private String facebookEmail;
	private String email;

	public RegisterRequest(String name, String gender, String birthDay, String password, String facebookEmail,
			String email) {

		this.name = name;
		this.birthDay = birthDay;
		this.gender = gender;
		this.password = password;
		this.facebookEmail = facebookEmail;
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFacebookEmail() {
		return facebookEmail;
	}

	public void setFacebookEmail(String facebookEmail) {
		this.facebookEmail = facebookEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
