package com.mprox.sdk.comunication.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {
	
	@JsonProperty("success")
	protected boolean success;

	@JsonProperty("errorMessage")
	protected String errorMsessage;

	public String getErrorMsessage() {
		return errorMsessage;
	}

	public void setErrorMsessage(String errorMsessage) {
		this.errorMsessage = errorMsessage;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
