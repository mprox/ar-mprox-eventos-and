package com.mprox.sdk.comunication.request;

import java.util.List;

import com.mprox.sdk.model.BeaconActivity;

public class BeaconTrackingActivityRequest {

	private String userId;
	private List<BeaconActivity> beacon_user_track;
	
	public BeaconTrackingActivityRequest(String userId, List<BeaconActivity> beacon_user_track) {

		this.userId = userId;
		this.beacon_user_track = beacon_user_track;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<BeaconActivity> getBeacon_user_track() {
		return beacon_user_track;
	}

	public void setBeacon_user_track(List<BeaconActivity> beacon_user_track) {
		this.beacon_user_track = beacon_user_track;
	}
}
