package com.mprox.sdk.comunication.response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.sdk.model.Product;

public class GetProductsResponse extends BaseResponse {
	
	@JsonProperty("data")
	protected List<Product> data;

	public List<Product> getData() {
		return data;
	}

	public void setData(List<Product> data) {
		this.data = data;
	}
}