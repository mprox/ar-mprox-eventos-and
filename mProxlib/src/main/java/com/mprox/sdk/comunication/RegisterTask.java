package com.mprox.sdk.comunication;

import java.util.Locale;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.sdk.comunication.request.RegisterRequest;
import com.mprox.sdk.comunication.response.LoginResponse;
import com.mprox.sdk.utils.UtilsMprox;

public abstract class RegisterTask extends AsyncTask<RegisterRequest, Void, String> {

	private Context context;

	public RegisterTask(Context context) {
		super();
		this.context = context;
	}

	@Override
	protected String doInBackground(RegisterRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/asistentes/register";
		JSONObject serviceParams = new JSONObject();

		RegisterRequest request = params[0];

		try {

			if (!UtilsMprox.isNullOrEmpty(request.getName())) {
				serviceParams.put("name", request.getName());
			}

			if (!UtilsMprox.isNullOrEmpty(request.getGender())) {
				serviceParams.put("gender", request.getGender());
			}

			if (!UtilsMprox.isNullOrEmpty(request.getPassword())) {
				serviceParams.put("password", request.getPassword());
			}

			if (!UtilsMprox.isNullOrEmpty(request.getEmail())) {
				serviceParams.put("identifier", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
				serviceParams.put("email", request.getEmail());
			}

			if (!UtilsMprox.isNullOrEmpty(request.getBirthDay())) {
				serviceParams.put("birthday", request.getBirthDay());
			}

			if (!UtilsMprox.isNullOrEmpty(request.getFacebookEmail())) {
				serviceParams.put("identifier", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
				serviceParams.put("facebook_email", request.getFacebookEmail());
			}

			serviceParams.put("deviceType", "ANDROID");

			serviceParams.put("deviceId", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));

			serviceParams.put("deviceModel", android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);

			serviceParams.put("deviceVersion", android.os.Build.VERSION.RELEASE);

			serviceParams.put("deviceLanguageCode", Locale.getDefault().getISO3Language());

			serviceParams.put("deviceCountryCode", context.getResources().getConfiguration().locale.getISO3Country());

			response = new HttpService().post(this.context, baseUrl, serviceParams);

		} catch (Exception e) {
			response = "exeption during service execution";
		}
		Log.i("Register Task", "" + response);
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		LoginResponse response = new LoginResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, LoginResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(LoginResponse response);
}