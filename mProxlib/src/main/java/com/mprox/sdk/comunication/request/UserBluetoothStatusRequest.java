package com.mprox.sdk.comunication.request;


public class UserBluetoothStatusRequest {

	private boolean bluetoothOn;
	private String userId;
    private boolean bluetoothSupported;

    public UserBluetoothStatusRequest(String userId, boolean bluetoothOn, boolean bluetoothSupported) {
		this.userId = userId;
		this.bluetoothOn = bluetoothOn;
        this.bluetoothSupported = bluetoothSupported;
	}

	public boolean getBluetoothOn() {
		return bluetoothOn;
	}

	public String getUserId() {
		return userId;
	}

    public boolean isBluetoothSupported() {
        return bluetoothSupported;
    }

}
