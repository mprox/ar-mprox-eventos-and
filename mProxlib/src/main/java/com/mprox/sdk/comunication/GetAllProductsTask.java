package com.mprox.sdk.comunication;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.sdk.comunication.request.ProdutsByLocationRequest;
import com.mprox.sdk.comunication.response.GetProductsResponse;

public abstract class GetAllProductsTask extends AsyncTask<ProdutsByLocationRequest, Void, String> {

	private static final String SEARCH_URL = ConstsService.PROD_BASE_URL + "/sdk/activity/getProductsByLocation";

	private Context mContext;

	public GetAllProductsTask(Context context) {
		super();
		this.mContext = context;
	}

	@Override
	protected String doInBackground(ProdutsByLocationRequest... params) {
		String response = "";

		JSONObject serviceParams = new JSONObject();

		ProdutsByLocationRequest request = params[0];

		try {
			serviceParams.put("userId", String.valueOf(request.getUserId()));
			serviceParams.put("identifier", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));

			serviceParams.put("latitude", String.valueOf(request.getLatitude()));
			serviceParams.put("longitude", String.valueOf(request.getLongitude()));

			response = new HttpService().post(mContext, SEARCH_URL, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}
		if (response != null) {
			Log.i("SearService Task", response);
		}
		
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		GetProductsResponse response = new GetProductsResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, GetProductsResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(GetProductsResponse response);
}