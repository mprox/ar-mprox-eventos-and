package com.mprox.sdk.comunication.response;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.sdk.model.User;

public class LoginResponse extends BaseResponse{
	
	@JsonProperty("data")
	private User data;

	public User getData() {
		return data;
	}

	public void setData(User data) {
		this.data = data;
	}
}