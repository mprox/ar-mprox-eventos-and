package com.mprox.sdk.comunication.response;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.sdk.model.BeaconActivity;

public class BeaconActivityResponse extends BaseResponse{
	
	@JsonProperty("data")
	protected BeaconActivity data;

	public BeaconActivity getData() {
		return data;
	}

	public void setData(BeaconActivity data) {
		this.data = data;
	}
}