package com.mprox.sdk.comunication;

import java.util.Locale;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.sdk.comunication.request.UserPreferencesRequest;
import com.mprox.sdk.comunication.response.LoginResponse;
import com.mprox.sdk.utils.UtilsMprox;

public abstract class UserPreferencesTask extends AsyncTask<UserPreferencesRequest, Void, String> {

	private Context context;

	public UserPreferencesTask(Context context) {
		super();
		this.context = context;
	}

	@Override
	protected String doInBackground(UserPreferencesRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/user/updateUserPreferences";
		JSONObject serviceParams = new JSONObject();

		UserPreferencesRequest request = params[0];

		try {

			if (!UtilsMprox.isNullOrEmpty(request.getPreferences())) {
				serviceParams.put("preferences", request.getPreferences());
			}

			if (!UtilsMprox.isNullOrEmpty(request.getUserId())) {
				serviceParams.put("userId", request.getUserId());
			}

			serviceParams.put("deviceType", "ANDROID");

			serviceParams.put("deviceId", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));

			serviceParams.put("deviceModel", android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);

			serviceParams.put("deviceVersion", android.os.Build.VERSION.RELEASE);

			serviceParams.put("deviceLanguageCode", Locale.getDefault().getISO3Language());

			serviceParams.put("deviceCountryCode", context.getResources().getConfiguration().locale.getISO3Country());

			response = new HttpService().post(this.context, baseUrl, serviceParams);

		} catch (Exception e) {
			response = "exeption during service execution";
		}
		Log.i("Register Task", "" + response);
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		LoginResponse response = new LoginResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, LoginResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(LoginResponse response);
}