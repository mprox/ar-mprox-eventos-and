package com.mprox.middleware.core.service.request;

public class GetPointsRequest {

	private Long userId;

	public GetPointsRequest(Long userID) {
		userId = userID;
	}

	public Long getUserId() {
		return userId;
	}

}