package com.mprox.middleware.core.service.responses;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.Store;

public class GetStoresResponse extends BaseResponse{
	@JsonProperty("data")
	protected List<Store> data;

	public List<Store> getData() {
		return data;
	}

	public void setData(List<Store> data) {
		this.data = data;
	}
}