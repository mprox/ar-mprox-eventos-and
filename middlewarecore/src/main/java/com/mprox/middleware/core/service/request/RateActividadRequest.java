package com.mprox.middleware.core.service.request;

public class RateActividadRequest {

	private String actividadId;
	private int rating;
	private long userId;
	private String observaciones;

	public RateActividadRequest(String actividadId, int newRating, long userId, String observaciones) {
		this.rating = newRating;
		this.actividadId = actividadId;
		this.userId = userId;
		this.observaciones = observaciones;
	}

	public String getActividadId() {
		return actividadId;
	}

	public int getRating() {
		return rating;
	}

	public long getUserId() {
		return userId;
	}

	public String getObservaciones(){
		return observaciones;
	}
}