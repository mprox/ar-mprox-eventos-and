package com.mprox.middleware.core.domain;


import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Respuesta implements Parcelable {

    @JsonProperty("id")
	private Long id;

    @JsonProperty("opcion")
	private Opcion opcion;

	@JsonProperty ("pregunta")
	private Pregunta pregunta;

    @JsonProperty("usuario")
	private User usuario;

    @JsonProperty("fecha")
	private String fecha;

	@JsonProperty("textoresp")
	private String textoresp;

	public Respuesta() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the opcion
	 */
	public Opcion getOpcion() {
		return opcion;
	}

	/**
	 * @param opcion
	 *            the opcion to set
	 */
	public void setOpcion(Opcion opcion) {
		this.opcion = opcion;
	}

	/**
	 * @return the usuario
	 */
	public User getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the textoResp
	 */
	public String getTextoResp() {
		return textoresp;
	}

	/**
	 * @param textoresp
	 *            the textoResp to set
	 */
	public void setTextoResp(String textoresp) {
		this.textoresp = textoresp;
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	@Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeParcelable(opcion, flags);
        dest.writeParcelable(usuario, flags);
        dest.writeString(fecha);
		dest.writeString(textoresp);
    }

    public Respuesta(Parcel source) {
        id = source.readLong();
        opcion = source.readParcelable(Opcion.class.getClassLoader());
        usuario = source.readParcelable(User.class.getClassLoader());
        fecha = source.readString();
		textoresp = source.readString();
    }

    public static final Parcelable.Creator<Respuesta> CREATOR = new Parcelable.Creator<Respuesta>() {
        public Respuesta createFromParcel(Parcel source) {
            return new Respuesta(source);
        }

        public Respuesta[] newArray(int size) {
            return new Respuesta[size];
        }
    };

}