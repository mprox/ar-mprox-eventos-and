package com.mprox.middleware.core.service.responses;

import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.core.domain.CalificacionActividad;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class GetCalificacionesResponse extends BaseResponse{

	@JsonProperty("data")
	protected List<CalificacionActividad> data;

	public List<CalificacionActividad> getData() {
		return data;
	}

	public void setData(List<CalificacionActividad> data) {
		this.data = data;
	}
}