package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.mprox.middleware.core.service.request.AsignPointRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.ConstsService;

public abstract class AsignAccionPointsTask extends AsyncTask<AsignPointRequest, Void, String> {

	@Override
	protected String doInBackground(AsignPointRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/user/setUserPoint";
		JSONObject serviceParams = new JSONObject();

		AsignPointRequest rateMsg = params[0];

		try {

			serviceParams.put("userId", rateMsg.getUserId());
			serviceParams.put("activity", rateMsg.getActivity());
			serviceParams.put("date", rateMsg.getDate());
			serviceParams.put("description", rateMsg.getDescription());
			serviceParams.put("points", rateMsg.getPoints());

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Rating Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		BaseResponse response = new BaseResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, BaseResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(BaseResponse response);
}