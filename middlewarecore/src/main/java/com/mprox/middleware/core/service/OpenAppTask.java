package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.middleware.core.service.request.OpenAppRequest;
import com.mprox.middleware.core.service.responses.BeaconActivityResponse;
import com.mprox.middleware.core.util.ConstsService;

public abstract class OpenAppTask extends AsyncTask<OpenAppRequest, Void, String> {
	
	private Context mContext;

	public OpenAppTask(Context context) {
		super();
		this.mContext = context;
	}

	@Override
	protected String doInBackground(OpenAppRequest... params) {
		String response = "";

		OpenAppRequest request = params[0];
		String baseUrl = ConstsService.PROD_BASE_URL + "/activity/track/openApp";
		JSONObject serviceParams = new JSONObject();

		try {
			serviceParams.put("deviceId", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));
			serviceParams.put("beaconId", request.getBeaconId());
			serviceParams.put("productId", request.getProductId());
			serviceParams.put("date", request.getDate());
			serviceParams.put("time", request.getTime());
		} catch (JSONException e1) {
			e1.printStackTrace();
			Log.e("OpenAppTask", "Error while making json");
		}

		try {
			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("OpenAppTask", "" + response);

		return response;
	}


	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		BeaconActivityResponse response = new BeaconActivityResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, BeaconActivityResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}
		onServiceReturned(response);
	}
	
	public abstract void onServiceReturned(BeaconActivityResponse response);
}