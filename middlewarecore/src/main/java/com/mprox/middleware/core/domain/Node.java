package com.mprox.middleware.core.domain;

import java.util.List;

public class Node implements Comparable<Node> {

	private String id;
	private String name;
	private Float posx;
	private Float posy;
	private NodeType type;
	private Double minDistance = Double.POSITIVE_INFINITY;
	private Node previous;
	private int localShoppingId;
	private boolean parking;
	private List<Edge> edges;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPosx() {
		return posx;
	}

	public void setPosx(Float posx) {
		this.posx = posx;
	}

	public Float getPosy() {
		return posy;
	}

	public void setPosy(Float posy) {
		this.posy = posy;
	}

	public NodeType getType() {
		return type;
	}

	public void setType(NodeType type) {
		this.type = type;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public double getMinDistance() {
		return minDistance;
	}

	public void setMinDistance(double minDistance) {
		this.minDistance = minDistance;
	}

	public Node getPrevious() {
		return previous;
	}

	public void setPrevious(Node previous) {
		this.previous = previous;
	}

	public void setMinDistance(Double minDistance) {
		this.minDistance = minDistance;
	}

	@Override
	public int compareTo(Node another) {
		return Double.compare(minDistance, another.minDistance);
	}

	public int getLocalId() {
		return localShoppingId;
	}

	public void setLocalId(int localId) {
		this.localShoppingId = localId;
	}

	public boolean isParking() {
		return parking;
	}

	public void setParking(boolean parking) {
		this.parking = parking;
	}
}
