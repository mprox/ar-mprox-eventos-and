package com.mprox.middleware.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UtilsMiddleware {
	private static UtilsMiddleware mInstance = null;

	private static final String MIDDLEWARE_PREFERENCES = "mprox_eventos_preferences";
	private static final String LOGED_USER_EMAIL = "loged_user_email";
	private static final String LOGED_USER_ID = "loged_user_id";
    private static final String LOGED_USER_TEAM = "loged_user_team";
	private static final String LOGED_USER_FULL_NAME = "loged_user_full_name";
	private static final String LOGED_USER_IMAGE = "loged_user_image";

	private static final String RATED_ITEMS = "rated_benefits";
	private static final String RATED_STORES = "rated_stores";
	private static final String RATED_ITEMS_SEPARATOR = "--00000--";

	private static final String APP_PREFERENCES_CHECKED = "app_preferences_checked";
	private static final String APP_PREFERENCES_SEPARATOR = "--app_preferences_separator--";
	private static final String APP_PREFERENCES_BENEFITS_SHOWED = "benefits_showed";
	
	private static final String APP_PREFERENCES_RETURNED_TO_CARPARK = "returned_to_carpark";

	private Context mContext;
	private SharedPreferences mSharedPreferences;

	private int subscribedActivities;

	public UtilsMiddleware(Context context) {
		mContext = context;
		mSharedPreferences = mContext.getSharedPreferences(MIDDLEWARE_PREFERENCES, Context.MODE_PRIVATE);
	}

	public static UtilsMiddleware getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new UtilsMiddleware(context);
		}

		return mInstance;
	}

	private Editor getEditor() {
		return mSharedPreferences.edit();
	}

	public void saveLogedUserEmail(String userEmail) {
		getEditor().putString(LOGED_USER_EMAIL, userEmail).commit();
	}

	public String getLogedUserEmail() {
		return mSharedPreferences.getString(LOGED_USER_EMAIL, null);
	}

	public void saveLogedUserId(int userId) {
		getEditor().putInt(LOGED_USER_ID, userId).commit();
	}

    public void saveLogedUserTeam(String team) {
        getEditor().putString(LOGED_USER_TEAM, team).commit();
    }
    public String getLogedUserTeam() {
        return mSharedPreferences.getString(LOGED_USER_TEAM, "");
    }

	public String getLogedUserName() {
		return mSharedPreferences.getString(LOGED_USER_FULL_NAME, "");
	}

	public void saveLogedUserName(String userName) {
		getEditor().putString(LOGED_USER_FULL_NAME, userName).commit();
	}

	public String getLogedUserImage() {
		return mSharedPreferences.getString(LOGED_USER_IMAGE, "");
	}

	public void saveLogedUserImage(String userImage) {
		getEditor().putString(LOGED_USER_IMAGE, userImage).commit();
	}

	public int getLogedUserId() {
		return mSharedPreferences.getInt(LOGED_USER_ID, 0);
	}

	public void addSubscribedActivity() {
		subscribedActivities++;
	}

	public void removeSubscribedActivity() {
		subscribedActivities--;
		if (subscribedActivities < 0) {
			subscribedActivities = 0;
		}
	}

	public boolean isAppActive() {
		return subscribedActivities > 0;
	}

	public void clearProductsRated() {
		getEditor().putString(RATED_ITEMS, "").commit();
	}
	
	public ArrayList<String> getProductsRated() {
		String ratedItemsString = mSharedPreferences.getString(RATED_ITEMS, "");
		String[] ratedItemsVector = ratedItemsString.split(RATED_ITEMS_SEPARATOR);

		return new ArrayList<String>(Arrays.asList(ratedItemsVector));
	}

	public void addRatedProduct(String productId) {
		String ratedItemsString = mSharedPreferences.getString(RATED_ITEMS, "");

		if (ratedItemsString.length() != 0) {
			ratedItemsString = ratedItemsString.concat(RATED_ITEMS_SEPARATOR);
		}

		ratedItemsString = ratedItemsString.concat(productId);

		getEditor().putString(RATED_ITEMS, ratedItemsString).commit();
	}

	public ArrayList<String> getStoresRated() {
		String ratedItemsString = mSharedPreferences.getString(RATED_STORES, "");
		String[] ratedItemsVector = ratedItemsString.split(RATED_ITEMS_SEPARATOR);
		return new ArrayList<String>(Arrays.asList(ratedItemsVector));
	}
	
	
	public void addBenefitsShowed(Integer productId) {
		String showedItemsString = mSharedPreferences.getString(APP_PREFERENCES_BENEFITS_SHOWED, "");

		if (showedItemsString.length() != 0) {
			showedItemsString = showedItemsString.concat(";");
		}

		Long time = new Date().getTime();
		showedItemsString = showedItemsString.concat(productId+","+time);

		getEditor().putString(APP_PREFERENCES_BENEFITS_SHOWED, showedItemsString).commit();
	}

	public Boolean wasBenefitsShowed(Integer productId) {
		String showedItemsString = mSharedPreferences.getString(APP_PREFERENCES_BENEFITS_SHOWED, "");
		
		if(showedItemsString.equals(""))
			return false;

		
		String[] showedItemsVector = showedItemsString.split(";");
		for(int i=0;i<showedItemsVector.length;i++) {
			String[] data = showedItemsVector[i].split(",");
			Long now = new Date().getTime();
			if(productId == Integer.parseInt(data[0]) && now > Long.parseLong(data[1])){
				return true;
			}
		}
		return false;
	}
	
	public void setUserReturnedToCarPark(Boolean returned) {
		getEditor().putBoolean(APP_PREFERENCES_RETURNED_TO_CARPARK, returned).commit();
	}
	
	public Boolean hasUserReturnedToCarPark() {
		return mSharedPreferences.getBoolean(APP_PREFERENCES_RETURNED_TO_CARPARK, false);
	}

	public void addRatedStore(String storeId) {

		String ratedItemsString = mSharedPreferences.getString(RATED_STORES, "");

		if (ratedItemsString.length() != 0) {
			ratedItemsString = ratedItemsString.concat(RATED_ITEMS_SEPARATOR);
		}

		ratedItemsString = ratedItemsString.concat(storeId);

		getEditor().putString(RATED_STORES, ratedItemsString).commit();
	}

	public static boolean isNullOrEmpty(String stringToCheck) {
		if (stringToCheck == null) {
			return true;
		}

		if (stringToCheck.length() == 0) {
			return true;
		}

		return false;
	}

	public void addAppPreference(String preference) {
		String preferences = mSharedPreferences.getString(APP_PREFERENCES_CHECKED, "");

		if (!isNullOrEmpty(preferences)) {
			preferences = preferences.concat(APP_PREFERENCES_SEPARATOR);
		}

		preferences = preferences.concat(preference);

		getEditor().putString(APP_PREFERENCES_CHECKED, preferences).commit();
	}

	public ArrayList<String> getCheckedPreferences() {
		String preferences = mSharedPreferences.getString(APP_PREFERENCES_CHECKED, "");

		String[] preferenceVector = preferences.split(APP_PREFERENCES_SEPARATOR);

		ArrayList<String> returnedArray = new ArrayList<String>();
		if (!isNullOrEmpty(preferences)) {
			returnedArray.addAll(Arrays.asList(preferenceVector));
		}

		return returnedArray;
	}

	public void removeAppPreference(String preference) {
		String preferences = mSharedPreferences.getString(APP_PREFERENCES_CHECKED, "");

		String[] preferenceVector = preferences.split(APP_PREFERENCES_SEPARATOR);

		ArrayList<String> returnedArray = new ArrayList<String>();
		if (!isNullOrEmpty(preferences)) {
			returnedArray.addAll(Arrays.asList(preferenceVector));
		}

		returnedArray.remove(preference);
		preferences = "";

		for (String aux : returnedArray) {
			if (returnedArray.indexOf(aux) != 0) {
				preferences = preferences.concat(APP_PREFERENCES_SEPARATOR);
			}

			preferences = preferences.concat(aux);
		}

		getEditor().putString(APP_PREFERENCES_CHECKED, preferences).commit();
	}

	public static String formatImageUrl(String originalUrl) {
		try {
			int startSubString = originalUrl.indexOf("public_html/mproxeventos") + "public_html/mproxeventos/".length();

			String finalPart = originalUrl.substring(startSubString);

			return ConstsService.BASE_URL.concat(finalPart);
		} catch (Exception e) {
			return ConstsService.BASE_URL.concat(originalUrl);
		}
	}

	public void clearTags() {
		getEditor().putString(APP_PREFERENCES_CHECKED, "").commit();
	}
	
}