package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.mprox.middleware.core.service.request.LoginRequest;
import com.mprox.middleware.core.service.responses.LoginResponse;
import com.mprox.middleware.core.util.ConstsService;
import com.mprox.middleware.core.util.UtilsMiddleware;

public abstract class LoginTask extends AsyncTask<LoginRequest, Void, String> {

	@Override
	protected String doInBackground(LoginRequest... params) {
		String response = "";

		LoginRequest request = params[0];

		String baseUrl = ConstsService.PROD_BASE_URL + "/asistentes/login";
		JSONObject serviceParams = new JSONObject();

		try {

			if (!UtilsMiddleware.isNullOrEmpty(request.getEmail())) {
				serviceParams.put("email", request.getEmail());
			}

			if (!UtilsMiddleware.isNullOrEmpty(request.getPassword())) {
				serviceParams.put("password", request.getPassword());
			}

			if (!UtilsMiddleware.isNullOrEmpty(request.getFacebookEmail())) {
				serviceParams.put("facebook_email", request.getFacebookEmail());
			}

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}
		Log.i("Login Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		LoginResponse response = new LoginResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, LoginResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(LoginResponse response);
}
