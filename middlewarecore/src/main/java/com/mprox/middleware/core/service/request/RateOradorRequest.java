package com.mprox.middleware.core.service.request;

public class RateOradorRequest {

	private String oradorId;
	private int rating;
	private long userId;
	private String observaciones;

	public RateOradorRequest(String oradorId, int newRating, long userId, String observaciones) {
		this.rating = newRating;
		this.oradorId = oradorId;
		this.userId = userId;
		this.observaciones = observaciones;
	}

	public String getOradorId() {
		return oradorId;
	}

	public int getRating() {
		return rating;
	}

	public long getUserId() {
		return userId;
	}

	public String getObservaciones(){
		return observaciones;
	}
}