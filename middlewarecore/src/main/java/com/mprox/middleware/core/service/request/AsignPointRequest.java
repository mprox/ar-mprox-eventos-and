package com.mprox.middleware.core.service.request;

public class AsignPointRequest {

	private Long userId;
	private String activity;
	private String date;
	private String description;
	private Integer points;

	public AsignPointRequest(Long userId, String activity, String date, String description, Integer points) {
		this.userId = userId;
		this.activity = activity;
		this.date = date;
		this.description = description;
		this.points = points;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

}