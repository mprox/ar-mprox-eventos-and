package com.mprox.middleware.core.service.request;

import com.mprox.middleware.core.domain.Respuesta;

public class SendEncuestaRequest {

	private String userId;
	private String optionId;
    private String preguntaId;
    private String textoResp;

	public SendEncuestaRequest(String userId, String optionId, String preguntaId, String textoResp){
		this.userId = userId;
		this.optionId = optionId;
        this.preguntaId = preguntaId;
        this.textoResp = textoResp;
	}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getPreguntaId() {return preguntaId; }

    public void setPreguntaId(String preguntaId) {this.preguntaId = preguntaId;}

    public String getTextoResp() {return textoResp; }

    public void setTextoResp(String textoResp) {this.textoResp = textoResp; }
}