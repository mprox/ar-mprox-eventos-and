package com.mprox.middleware.core.service.responses;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.User;

public class LoginResponse extends BaseResponse{
	
	@JsonProperty("data")
	private User data;

	public User getData() {
		return data;
	}

	public void setData(User data) {
		this.data = data;
	}
}