package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Opcion implements Parcelable {

    @JsonProperty("id")
	private Long id;

    @JsonProperty("opcion")
	private String opcion;

    public Opcion() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(opcion);
    }

    public Opcion(Parcel source) {
        id = source.readLong();
        opcion = source.readString();
    }

    public static final Parcelable.Creator<Opcion> CREATOR = new Parcelable.Creator<Opcion>() {
        public Opcion createFromParcel(Parcel source) {
            return new Opcion(source);
        }

        public Opcion[] newArray(int size) {
            return new Opcion[size];
        }
    };

}