/**
 * 
 */
package com.mprox.middleware.core.application;

import android.app.Application;

/**
 * 
 * Entry point for any Android application
 * 
 * @author Leo
 *
 */
public abstract class AndroidApplication extends Application {

	public void onCreate() {

		super.onCreate();

	}

}
