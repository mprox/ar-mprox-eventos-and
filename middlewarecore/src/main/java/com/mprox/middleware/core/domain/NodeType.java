package com.mprox.middleware.core.domain;

public enum NodeType {

	CORNER(0), BEACON(1);
	
	private int value;

	private NodeType(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	public static NodeType getFromValue(int value) {
		for (NodeType nodeType : NodeType.values()) {
			if (nodeType.getValue() == value)
				return nodeType;
		}
		return null;
	}
};
