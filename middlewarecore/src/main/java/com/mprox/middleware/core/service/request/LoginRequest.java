package com.mprox.middleware.core.service.request;

public class LoginRequest {

	private String email;
	private String password;
	private String facebookEmail;
	
	public LoginRequest(String email, String password, String facebookEmail){
		this.email = email;
		this.password = password;
		this.facebookEmail = facebookEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFacebookEmail() {
		return facebookEmail;
	}

	public void setFacebookEmail(String facebookEmail) {
		this.facebookEmail = facebookEmail;
	}
}