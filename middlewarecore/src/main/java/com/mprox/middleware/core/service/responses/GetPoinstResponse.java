package com.mprox.middleware.core.service.responses;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.UserPoints;

public class GetPoinstResponse extends BaseResponse{
	
	@JsonProperty("data")
	protected List<UserPoints> data;

	public List<UserPoints> getData() {
		return data;
	}

	public void setData(List<UserPoints> data) {
		this.data = data;
	}
}