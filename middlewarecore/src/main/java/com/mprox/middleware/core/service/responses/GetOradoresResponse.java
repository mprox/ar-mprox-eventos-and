package com.mprox.middleware.core.service.responses;

import com.mprox.middleware.core.domain.Orador;
import com.mprox.middleware.core.domain.User;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class GetOradoresResponse extends BaseResponse{

	@JsonProperty("data")
	protected List<User> data;

	public List<User> getData() {
		return data;
	}

	public void setData(List<User> data) {
		this.data = data;
	}
}