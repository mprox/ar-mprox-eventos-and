package com.mprox.middleware.core.service.responses;

import com.mprox.middleware.core.domain.Opcion;
import com.mprox.middleware.core.domain.Pregunta;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class GetEncuestaResponse extends BaseResponse{
	
	@JsonProperty("data")
	protected List<Pregunta> data;

	public List<Pregunta> getData() {
		return data;
	}

	public void setData(List<Pregunta> data) {
		this.data = data;
	}
}