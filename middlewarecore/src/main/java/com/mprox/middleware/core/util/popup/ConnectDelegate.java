
package com.mprox.middleware.core.util.popup;

/**
 * @author Matias Dumrauf
 * @since 1.0
 */
public interface ConnectDelegate {


	/**
	 * On click RETRY event
	 */
	public void onRetry();


	/**
	 * On click CANCEL event
	 */
	public void onCancel();


}
