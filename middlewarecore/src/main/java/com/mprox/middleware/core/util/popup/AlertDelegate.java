
package com.mprox.middleware.core.util.popup;

/**
 * @author Matias Dumrauf
 * @since 1.0
 */
public interface AlertDelegate {

	/**
	 * On click OK event
	 */
	public void onOk();

}
