package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.middleware.core.service.responses.GetStoresResponse;
import com.mprox.middleware.core.util.ConstsService;

public abstract class StoresTask extends AsyncTask<Void, Void, String> {

	private Context mContext;

	public StoresTask(Context context) {
		super();
		this.mContext = context;
	}

	@Override
	protected String doInBackground(Void... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/stores/getStores";
		JSONObject serviceParams = new JSONObject();

		try {
			serviceParams.put("deviceId", Secure.getString(mContext.getContentResolver(),
					Secure.ANDROID_ID));
		} catch (JSONException e1) {
			e1.printStackTrace();
			Log.e("Get Benefits Task", "Error while making json");
		}
		
		try {
			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Get Stores Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		GetStoresResponse response = new GetStoresResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, GetStoresResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(GetStoresResponse response);

}
