package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by plamas on 19/8/2016.
 */
public class ConsultaActividad implements Parcelable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("consulta")
    private String consulta;

    @JsonProperty("actividad")
    private Actividad actividad;

    @JsonProperty("user")
    private User user;

    public ConsultaActividad() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(consulta);
        dest.writeParcelable(actividad, flags);
        dest.writeParcelable(user, flags);
    }

    public ConsultaActividad(Parcel source) {
        id = source.readLong();
        consulta = source.readString();
        actividad = source.readParcelable(User.class.getClassLoader());
        user = source.readParcelable(Evento.class.getClassLoader());
    }

    public static final Creator<ConsultaActividad> CREATOR = new Creator<ConsultaActividad>() {
        public ConsultaActividad createFromParcel(Parcel source) {
            return new ConsultaActividad(source);
        }

        public ConsultaActividad[] newArray(int size) {
            return new ConsultaActividad[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
