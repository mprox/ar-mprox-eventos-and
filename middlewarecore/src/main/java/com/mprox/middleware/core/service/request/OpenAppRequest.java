package com.mprox.middleware.core.service.request;

public class OpenAppRequest {

	private String beaconId;
	private String productId;
	private String date;
	private String time;

	public OpenAppRequest(String beaconId, String productId, String date,
			String time) {

		this.beaconId = beaconId;
		this.productId = productId;
		this.date = date;
		this.time = time;

	}

	public String getBeaconId() {
		return beaconId;
	}

	public void setBeaconId(String beaconId) {
		this.beaconId = beaconId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
