package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Orador implements Parcelable {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("nombre")
	private String nombre;

	@JsonProperty("activa")
	private Boolean activa;

	@JsonProperty("descripcion")
	private String descripcion;

	@JsonProperty("imagen")
	private String imagen;

	@JsonProperty("rating")
	private Float rating;

	@JsonProperty("voteQuantity")
	private Float voteQuantity;

	@JsonProperty("voteTotal")
	private Float voteTotal;

    @JsonProperty("user")
    private User user;

	public Orador() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the activa
	 */
	public Boolean getActiva() {
		return activa;
	}

	/**
	 * @param activa the activa to set
	 */
	public void setActiva(Boolean activa) {
		this.activa = activa;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the imagen
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * @param imagen the imagen to set
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	/**
	 * @return the rating
	 */
	public Float getRating() {
		if(rating != null){
			return rating;
		}
		return 0f;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(Float rating) {
		this.rating = rating;
	}

	/**
	 * @return the voteQuantity
	 */
	public Float getVoteQuantity() {
		return voteQuantity;
	}

	/**
	 * @param voteQuantity the voteQuantity to set
	 */
	public void setVoteQuantity(Float voteQuantity) {
		this.voteQuantity = voteQuantity;
	}

	/**
	 * @return the voteTotal
	 */
	public Float getVoteTotal() {
        if(voteTotal == null){
            return 0f;
        }
		return voteTotal;
	}

	/**
	 * @param voteTotal the voteTotal to set
	 */
	public void setVoteTotal(Float voteTotal) {
		this.voteTotal = voteTotal;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(nombre);
		dest.writeByte((byte) (activa ? 1 : 0));
		dest.writeString(descripcion);
		dest.writeString(imagen);
		dest.writeFloat((rating!= null)?rating:0);
		dest.writeFloat((voteQuantity!= null)?voteQuantity:0);
		dest.writeFloat((voteTotal!= null)?voteTotal:0);
        dest.writeParcelable(user, flags);
	}

	public Orador(Parcel source) {
		id = source.readLong();
		nombre = source.readString();
		activa = source.readByte() == 1;
		descripcion = source.readString();
		imagen = source.readString();
		rating = source.readFloat();
		voteQuantity = source.readFloat();
		voteTotal = source.readFloat();
        user = source.readParcelable(User.class.getClassLoader());
	}

	public static final Parcelable.Creator<Orador> CREATOR = new Parcelable.Creator<Orador>() {
		public Orador createFromParcel(Parcel source) {
			return new Orador(source);
		}

		public Orador[] newArray(int size) {
			return new Orador[size];
		}
	};

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}