package com.mprox.middleware.core.service;

import android.os.AsyncTask;
import android.util.Log;

import com.mprox.middleware.core.service.request.RateActividadRequest;
import com.mprox.middleware.core.service.request.RateOradorRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.ConstsService;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

public abstract class RatingOradorTask extends AsyncTask<RateOradorRequest, Void, String> {

	@Override
	protected String doInBackground(RateOradorRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/orador/setCalificacion";
		JSONObject serviceParams = new JSONObject();

        RateOradorRequest rateMsg = params[0];

		try {

			serviceParams.put("oradorId", rateMsg.getOradorId());
			serviceParams.put("rating", rateMsg.getRating() + "");
			serviceParams.put("userId", rateMsg.getUserId());

			if(rateMsg.getObservaciones() != null)
				serviceParams.put("observacion", rateMsg.getObservaciones());

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Rating Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		BaseResponse response = new BaseResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, BaseResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(BaseResponse response);
}