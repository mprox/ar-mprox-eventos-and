package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by plamas on 19/8/2016.
 */
public class CalificacionActividad implements Parcelable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("voteTotal")
    private Float voteTotal;

    @JsonProperty("observacion")
    private String observacion;

    @JsonProperty("actividad")
    private Actividad actividad;

    @JsonProperty("user")
    private User user;

    public CalificacionActividad() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeFloat((voteTotal != null) ? voteTotal : 0);
        dest.writeString(observacion);
        dest.writeParcelable(actividad, flags);
        dest.writeParcelable(user, flags);
    }

    public CalificacionActividad(Parcel source) {
        id = source.readLong();
        voteTotal = source.readFloat();
        observacion = source.readString();
        actividad = source.readParcelable(User.class.getClassLoader());
        user = source.readParcelable(Evento.class.getClassLoader());
    }

    public static final Parcelable.Creator<CalificacionActividad> CREATOR = new Parcelable.Creator<CalificacionActividad>() {
        public CalificacionActividad createFromParcel(Parcel source) {
            return new CalificacionActividad(source);
        }

        public CalificacionActividad[] newArray(int size) {
            return new CalificacionActividad[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getVoteTotal() {
        return voteTotal;
    }

    public void setVoteTotal(Float voteTotal) {
        this.voteTotal = voteTotal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
