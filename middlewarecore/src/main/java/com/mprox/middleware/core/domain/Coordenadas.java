/**
 * 
 */
package com.mprox.middleware.core.domain;



/**
 * @author Matias Dumrauf
 *
 */
public class Coordenadas {

	private String longitud;
	
	private String latitud;

	/**
	 * @param longitud 
	 * @param latitud
	 */
	public Coordenadas(String longitud, String latitud) {
		this.longitud = longitud;
		this.latitud = latitud;
	}
	
	public Coordenadas() {
		this.longitud = "NO DATA";
		this.latitud = "NO DATA";
	}

	/**
	 * @return the longitud
	 */
	public String getLongitud() {
		return this.longitud;
	}

	/**
	 * @return the latitud
	 */
	public String getLatitud() {
		return this.latitud;
	}

	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	/**
	 * @param latitud the latitud to set
	 */
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
}
