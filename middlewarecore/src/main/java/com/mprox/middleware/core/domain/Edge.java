package com.mprox.middleware.core.domain;

public class Edge {

	private String argTarget;

	private double argWeight;

	public String getArgTarget() {
		return argTarget;
	}

	public void setArgTarget(String argTarget) {
		this.argTarget = argTarget;
	}

	public double getArgWeight() {
		return argWeight;
	}

	public void setArgWeight(double argWeight) {
		this.argWeight = argWeight;
	}

}
