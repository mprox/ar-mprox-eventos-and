package com.mprox.middleware.core.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class BeaconActivity {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("id")
	private Long idBeacon;

	@JsonProperty("id")
	private String uuidBeacon;

	@JsonProperty("id")
	private String nameBeacon;

	@JsonProperty("id")
	private String identifierBeacon;

	@JsonProperty("id")
	private String deviceType;

	@JsonProperty("id")
	private Long idProduct;

	@JsonProperty("id")
	private String date;

	@JsonProperty("id")
	private String time;

	@JsonProperty("id")
	private String uptime;

	public BeaconActivity() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdBeacon() {
		return idBeacon;
	}

	public void setIdBeacon(Long idBeacon) {
		this.idBeacon = idBeacon;
	}

	public String getUuidBeacon() {
		return uuidBeacon;
	}

	public void setUuidBeacon(String uuidBeacon) {
		this.uuidBeacon = uuidBeacon;
	}

	public String getNameBeacon() {
		return nameBeacon;
	}

	public void setNameBeacon(String nameBeacon) {
		this.nameBeacon = nameBeacon;
	}

	public String getIdentifierBeacon() {
		return identifierBeacon;
	}

	public void setIdentifierBeacon(String identifierBeacon) {
		this.identifierBeacon = identifierBeacon;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

}
