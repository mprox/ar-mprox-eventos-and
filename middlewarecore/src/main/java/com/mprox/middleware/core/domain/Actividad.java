package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Actividad implements Parcelable{

	@JsonProperty("id")
	private Long id;

	@JsonProperty("nombre")
	private String nombre;

	@JsonProperty("activa")
	private Boolean activa;

	@JsonProperty("descripcion")
	private String descripcion;

	@JsonProperty("contenido")
	private String contenido;

	@JsonProperty("imagen")
	private String imagen;

    @JsonProperty("fechaStart")
	private String fechaStart;

	@JsonProperty("fechaEnd")
	private String fechaEnd;

	@JsonProperty("horaStart")
	private String horaStart;

	@JsonProperty("horaEnd")
	private String horaEnd;

	@JsonProperty("rating")
	private Float rating;

	@JsonProperty("voteQuantity")
	private Float voteQuantity;

	@JsonProperty("voteTotal")
	private Float voteTotal;

	@JsonProperty("user")
	private User user;

	@JsonProperty("sala")
	private Sala sala;

    @JsonProperty("color")
    private String color;

	public Actividad() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the activa
	 */
	public Boolean getActiva() {
		return activa;
	}

	/**
	 * @param activa the activa to set
	 */
	public void setActiva(Boolean activa) {
		this.activa = activa;
	}

	/**
	 * @return the sala
	 */
	public Sala getSala() {
		return sala;
	}

	/**
	 * @param sala the sala to set
	 */
	public void setSala(Sala sala) {
		this.sala = sala;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the imagen
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * @param imagen the imagen to set
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	/**
	 * @return the rating
	 */
	public Float getRating() {
		if(rating == null){
			return 0f;
		}
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(Float rating) {
		this.rating = rating;
	}

	/**
	 * @return the voteQuantity
	 */
	public Float getVoteQuantity() {
		return voteQuantity;
	}

	/**
	 * @param voteQuantity the voteQuantity to set
	 */
	public void setVoteQuantity(Float voteQuantity) {
		this.voteQuantity = voteQuantity;
	}

	/**
	 * @return the voteTotal
	 */
	public Float getVoteTotal() {
		return voteTotal;
	}

	/**
	 * @param voteTotal the voteTotal to set
	 */
	public void setVoteTotal(Float voteTotal) {
		this.voteTotal = voteTotal;
	}

	/**
	 * @return the fechaStart
	 */
	public String getFechaStart() {
		return fechaStart;
	}

	/**
	 * @param fechaStart the fechaStart to set
	 */
	public void setFechaStart(String fechaStart) {
		this.fechaStart = fechaStart;
	}

	/**
	 * @return the fechaEnd
	 */
	public String getFechaEnd() {
		return fechaEnd;
	}

	/**
	 * @param fechaEnd the fechaEnd to set
	 */
	public void setFechaEnd(String fechaEnd) {
		this.fechaEnd = fechaEnd;
	}

	/**
	 * @return the horaStart
	 */
	public String getHoraStart() {
		return horaStart;
	}

	/**
	 * @param horaStart the horaStart to set
	 */
	public void setHoraStart(String horaStart) {
		this.horaStart = horaStart;
	}

	/**
	 * @return the horaEnd
	 */
	public String getHoraEnd() {
		return horaEnd;
	}

	/**
	 * @param horaEnd the horaEnd to set
	 */
	public void setHoraEnd(String horaEnd) {
		this.horaEnd = horaEnd;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(nombre);
		dest.writeByte((byte) (activa ? 1 : 0));
		dest.writeParcelable(sala, flags);
		dest.writeString(descripcion);
        dest.writeString(contenido);
		dest.writeString(imagen);
		dest.writeString(fechaStart);
		dest.writeString(fechaEnd);
		dest.writeString(horaStart);
		dest.writeString(horaEnd);
		dest.writeFloat((rating!= null)?rating:0);
		dest.writeFloat((voteQuantity!= null)?voteQuantity:0);
		dest.writeFloat((voteTotal!= null)?voteTotal:0);
		dest.writeParcelable(user, flags);
        dest.writeString(color);
	}

	public Actividad(Parcel source) {
		id = source.readLong();
		nombre = source.readString();
		activa = source.readByte() == 1;
		sala = source.readParcelable(Sala.class.getClassLoader());
		descripcion = source.readString();
        contenido = source.readString();
		imagen = source.readString();
		fechaStart = source.readString();
		fechaEnd = source.readString();
		horaStart = source.readString();
		horaEnd = source.readString();
		rating = source.readFloat();
		voteQuantity = source.readFloat();
		voteTotal = source.readFloat();
		user = source.readParcelable(User.class.getClassLoader());
        color = source.readString();
	}

	public static final Parcelable.Creator<Actividad> CREATOR = new Parcelable.Creator<Actividad>() {
		public Actividad createFromParcel(Parcel source) {
			return new Actividad(source);
		}

		public Actividad[] newArray(int size) {
			return new Actividad[size];
		}
	};
}