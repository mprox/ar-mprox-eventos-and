package com.mprox.middleware.core.domain;

import android.os.Parcelable;

public interface Ubicable extends Parcelable{
	
	/**
	 * @return coordenadas
	 */
	public Coordenadas getCoordenadas();

	/**
	 * @return nombre
	 */
	public String getNombre();
}
