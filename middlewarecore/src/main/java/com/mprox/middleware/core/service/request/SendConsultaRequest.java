package com.mprox.middleware.core.service.request;

public class SendConsultaRequest {

	private String userId;
	private String consulta;
    private String actividadId;

	public SendConsultaRequest(String userId, String actividadId,String consulta){
		this.userId = userId;
		this.consulta = consulta;
        this.actividadId = actividadId;
	}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String optionId) {
        this.consulta = optionId;
    }

    public String getActividadId() {
        return actividadId;
    }

    public void setActividadId(String actividadId) {
        this.actividadId = actividadId;
    }
}