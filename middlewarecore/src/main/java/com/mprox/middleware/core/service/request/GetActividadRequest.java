package com.mprox.middleware.core.service.request;

public class GetActividadRequest {

	private long userId;

	public GetActividadRequest(long userId) {
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}

}