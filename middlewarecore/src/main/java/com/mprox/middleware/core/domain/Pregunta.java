package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Pregunta implements Parcelable {

    @JsonProperty("id")
	private Long id;

    @JsonProperty("pregunta")
	private String pregunta;

    @JsonProperty("opciones")
	private List<Opcion> opciones;

    public Pregunta() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public List<Opcion> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<Opcion> opciones) {
		this.opciones = opciones;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(pregunta);
        dest.writeList(opciones);
    }

    public Pregunta(Parcel source) {
        id = source.readLong();
        pregunta = source.readString();
        opciones = source.readArrayList(Opcion.class.getClassLoader());
    }

    public static final Parcelable.Creator<Pregunta> CREATOR = new Parcelable.Creator<Pregunta>() {
        public Pregunta createFromParcel(Parcel source) {
            return new Pregunta(source);
        }

        public Pregunta[] newArray(int size) {
            return new Pregunta[size];
        }
    };

}