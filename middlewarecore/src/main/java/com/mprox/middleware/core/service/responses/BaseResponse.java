package com.mprox.middleware.core.service.responses;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {
	
	@JsonProperty("success")
	protected boolean success;

	@JsonProperty("errorMessage")
	protected String errorMsessage;

    @JsonProperty("errorTrace")
    protected String errorTrace;

	public String getErrorMsessage() {
		return errorMsessage;
	}

	public void setErrorMsessage(String errorMsessage) {
		this.errorMsessage = errorMsessage;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

    public String getErrorTrace() {
        return errorTrace;
    }

    public void setErrorTrace(String errorTrace) {
        this.errorTrace = errorTrace;
    }
}
