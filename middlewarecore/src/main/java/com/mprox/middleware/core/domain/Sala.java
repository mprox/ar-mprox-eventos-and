package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Sala  implements Parcelable {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("nombre")
	private String nombre;

	@JsonProperty("activa")
	private Boolean activa;

	@JsonProperty("descripcion")
	private String descripcion;

	public Sala() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the activa
	 */
	public Boolean getActiva() {
		return activa;
	}

	/**
	 * @param activa the activa to set
	 */
	public void setActiva(Boolean activa) {
		this.activa = activa;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(nombre);
		dest.writeByte((byte) (activa ? 1 : 0));
		dest.writeString(descripcion);
	}

	public Sala(Parcel source) {
		id = source.readLong();
		nombre = source.readString();
		activa = source.readByte() == 1;
		descripcion = source.readString();
	}

	public static final Parcelable.Creator<Sala> CREATOR = new Parcelable.Creator<Sala>() {
		public Sala createFromParcel(Parcel source) {
			return new Sala(source);
		}

		public Sala[] newArray(int size) {
			return new Sala[size];
		}
	};

}