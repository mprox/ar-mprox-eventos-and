package com.mprox.middleware.core.domain;

import java.util.List;

public class Floor {

	private int floorNumber;
	private int floorWidth;
	private int floorHeight;
	private Double floorScaleWidth;
	private Double floorScaleHeight;
	private List<Node> nodes;

	public int getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	public int getFloorWidth() {
		return floorWidth;
	}

	public void setFloorWidth(int floorWidth) {
		this.floorWidth = floorWidth;
	}

	public int getFloorHeight() {
		return floorHeight;
	}

	public void setFloorHeight(int floorHeight) {
		this.floorHeight = floorHeight;
	}

	public Double getFloorScaleWidth() {
		return floorScaleWidth;
	}

	public void setFloorScaleWidth(Double floorScaleWidth) {
		this.floorScaleWidth = floorScaleWidth;
	}

	public Double getFloorScaleHeight() {
		return floorScaleHeight;
	}

	public void setFloorScaleHeight(Double floorScaleHeight) {
		this.floorScaleHeight = floorScaleHeight;
	}
}
