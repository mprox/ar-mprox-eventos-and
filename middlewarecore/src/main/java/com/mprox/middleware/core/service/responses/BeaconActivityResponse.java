package com.mprox.middleware.core.service.responses;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.BeaconActivity;

public class BeaconActivityResponse extends BaseResponse{
	
	@JsonProperty("data")
	protected BeaconActivity data;

	public BeaconActivity getData() {
		return data;
	}

	public void setData(BeaconActivity data) {
		this.data = data;
	}
}