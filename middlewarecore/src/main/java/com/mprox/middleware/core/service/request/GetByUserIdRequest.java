package com.mprox.middleware.core.service.request;

public class GetByUserIdRequest {

	private Long appId;

	public GetByUserIdRequest(Long appId) {
		this.appId = appId;
	}

	public Long getAppId() {
		return appId;
	}

}