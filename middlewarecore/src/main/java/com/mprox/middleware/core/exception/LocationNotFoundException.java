/**
 * 
 */
package com.mprox.middleware.core.exception;

/**
 * @author Matias Dumrauf
 *
 */
public class LocationNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
