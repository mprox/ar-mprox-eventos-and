package com.mprox.middleware.core.service.request;

public class ProdutsByLocationRequest {

	private double latitude;
	private double longitude;
	
	/**
	 * @param deviceId
	 * @param latitude
	 * @param longitude
	 */
	public ProdutsByLocationRequest(double latitude,
	                                double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}
	
	

}
