package com.mprox.middleware.core.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class UserPointsRanking {

	@JsonProperty("points")
	private Long points;

	@JsonProperty("ranking")
	private Long ranking;

	public UserPointsRanking() {

	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}

	public Long getRanking() {
		return ranking;
	}

	public void setRanking(Long ranking) {
		this.ranking = ranking;
	}

}
