package com.mprox.middleware.core.service.request;

public class RedeemRequest {

	private Long userId;
	private String productsId;
	private String date;
	private String store;
	private String description;
	private String adress;
	private String invoiceNumber;
	private String invoiceTicketImage;
	private String productPhotoPromo;
	private Float total;
	private Float discount;
	private Float valuation;

	public RedeemRequest(Long userId, String productsId, String date, String store, String description, String adress,
			String invoiceNumber, String invoiceTicketImage, Float total, Float discount, Float valuation, String productPhotoPromo) {

		this.userId = userId;
		this.productsId = productsId;
		this.date = date;
		this.store = store;
		this.description = description;
		this.adress = adress;
		this.invoiceNumber = invoiceNumber;
		this.total = total;
		this.discount = discount;
		this.valuation = valuation;
		this.invoiceTicketImage = invoiceTicketImage;
		this.productPhotoPromo = productPhotoPromo;
	}

	public String getInvoiceTicketImage() {
		return invoiceTicketImage;
	}

	public void setInvoiceTicketImage(String invoiceTicketImage) {
		this.invoiceTicketImage = invoiceTicketImage;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public String getProductsId() {
		return productsId;
	}

	public void setProductsId(String productsId) {
		this.productsId = productsId;
	}

	public String getProductPhotoPromo() {
		return productPhotoPromo;
	}

	public void setProductPhotoPromo(String productPhotoPromo) {
		this.productPhotoPromo = productPhotoPromo;
	}

	public Float getValuation() {
		return valuation;
	}

	public void setValuation(Float valuation) {
		this.valuation = valuation;
	}

}
