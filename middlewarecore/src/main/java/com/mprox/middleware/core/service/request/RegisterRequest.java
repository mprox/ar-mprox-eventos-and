package com.mprox.middleware.core.service.request;

public class RegisterRequest {

	private String id;
	private String gender;
	private String birthDay;
	private String name;
	private String password;
	private String facebookEmail;
	private String email;
	private String imagen;

	public RegisterRequest(String id, String name, String gender, String birthDay, String password, String facebookEmail,
			String email, String image) {

		this.id = id;
		this.name = name;
		this.birthDay = birthDay;
		this.gender = gender;
		this.password = password;
		this.facebookEmail = facebookEmail;
		this.email = email;
		this.imagen = image;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFacebookEmail() {
		return facebookEmail;
	}

	public void setFacebookEmail(String facebookEmail) {
		this.facebookEmail = facebookEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String image) {
		this.imagen = image;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
