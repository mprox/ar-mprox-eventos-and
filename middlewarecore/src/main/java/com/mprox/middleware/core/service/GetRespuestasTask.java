package com.mprox.middleware.core.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mprox.middleware.core.service.request.GetRespuestaRequest;
import com.mprox.middleware.core.service.responses.GetEncuestaResponse;
import com.mprox.middleware.core.service.responses.GetRespuestaResponse;
import com.mprox.middleware.core.util.ConstsService;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public abstract class GetRespuestasTask extends AsyncTask<GetRespuestaRequest, Void, String> {

	private Context mContext;

	public GetRespuestasTask(Context context) {
		super();
		mContext = context;
	}

	@Override
	protected String doInBackground(GetRespuestaRequest... params) {
		
		String response = "";
		String baseUrl = ConstsService.PROD_BASE_URL + "/cuestionario/getRespuestas";
		JSONObject serviceParams = new JSONObject();

        GetRespuestaRequest rateMsg = params[0];
		try {
            serviceParams.put("userId", rateMsg.getUserId());

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Send Get Point Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

        GetRespuestaResponse response = new GetRespuestaResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			
			if(result == null){
				result = readCache();
			}else{
				saveCache(result);
			}
			
			response = mapper.readValue(result, GetRespuestaResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(GetRespuestaResponse response);

	
	private void saveCache(String result){
		 try {
		        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput("getUserRedeems.txt", Context.MODE_PRIVATE));
		        outputStreamWriter.write(result);
		        outputStreamWriter.close();
		 }catch (IOException e) {
		     Log.e("Exception", "File write failed: " + e.toString());
		 } 
	}
	
	private String readCache(){
		String ret = "";

	    try {
	        InputStream inputStream = mContext.openFileInput("getUserRedeems.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}
}