package com.mprox.middleware.core.domain;

import org.codehaus.jackson.annotate.JsonProperty;

public class UserPoints {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("productId")
	private Long productId;

	@JsonProperty("activity")
	private String activity;

	@JsonProperty("date")
	private String date;

	@JsonProperty("description")
	private String description;

	@JsonProperty("points")
	private Long points;

	public UserPoints() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}

}
