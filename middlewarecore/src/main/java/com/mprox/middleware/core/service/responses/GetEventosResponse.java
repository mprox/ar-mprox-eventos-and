package com.mprox.middleware.core.service.responses;

import com.mprox.middleware.core.domain.Evento;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class GetEventosResponse extends BaseResponse{

	@JsonProperty("data")
	protected List<Evento> data;

	public List<Evento> getData() {
		return data;
	}

	public void setData(List<Evento> data) {
		this.data = data;
	}
}