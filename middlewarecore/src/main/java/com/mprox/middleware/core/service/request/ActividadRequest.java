package com.mprox.middleware.core.service.request;

public class ActividadRequest {

	private long actividadId;

	public ActividadRequest(long actividadId) {
		this.actividadId = actividadId;
	}

	public long getActividadId() {
		return actividadId;
	}
}