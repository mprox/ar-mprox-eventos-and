package com.mprox.middleware.core.domain;

public enum UserType {

	ROOKIE(1L, 100L, "Rookie User"),

	REGULAR(2L, 500L, "Regular User"),

	PLATINUM(3L, 1000L, "Platinum User"),

	GOLD(4L, 5000L, "Gold User"),

	ELITE(5L, 10000L, "Elite User");

	private Long id;

	private Long points;

	private String name;

	private UserType(final Long id, final Long points, final String name) {

		this.id = id;
		this.points = points;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public Long getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}

	public static UserType fromId(final Long id) {
		for (UserType userType : UserType.values()) {
			if (userType.getId().equals(id)) {
				return (userType);
			}
		}
		return (null);
	}

	public static UserType fromPoints(final Long points) {
		if (UserType.ROOKIE.getPoints() >= points) {
			return UserType.ROOKIE;
		} else if (UserType.REGULAR.getPoints() >= points) {
			return UserType.REGULAR;
		} else if (UserType.PLATINUM.getPoints() >= points) {
			return UserType.PLATINUM;
		} else if (UserType.GOLD.getPoints() >= points) {
			return UserType.GOLD;
		} else {
			return UserType.ELITE;
		}
	}
}
