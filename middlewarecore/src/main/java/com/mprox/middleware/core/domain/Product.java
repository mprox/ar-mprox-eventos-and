package com.mprox.middleware.core.domain;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

	@JsonProperty("idProduct")
	private int id;

	@JsonProperty("productType")
	private int productType;

	@JsonProperty("category")
	private String category;

	@JsonProperty("notificationMessage")
	private String notificationMessage;

	@JsonProperty("title")
	private String title;

	@JsonProperty("description")
	private String description;

	@JsonProperty("imageUrl")
	private String imageUrl;

	@JsonProperty("priceBefore")
	private float priceBefore;

	@JsonProperty("priceNow")
	private float priceNow;

	@JsonProperty("discount")
	private float discount;

	@JsonProperty("points")
	private float points;

	@JsonProperty("rating")
	private float rating;

	@JsonProperty("isPriority")
	private boolean isPriority;

	@JsonProperty("startDate")
	private String startDate;

	@JsonProperty("timeToWait")
	private String timeToWait;

	@JsonProperty("endDate")
	private String endDate;

	@JsonProperty("termAndConditions")
	private String termAndConditions;

	@JsonProperty("showBefore")
	private boolean showBefore;

	@JsonProperty("showAfter")
	private boolean showAfter;

	@JsonProperty("isHighlight")
	private boolean isHighlight;

	@JsonProperty("imageFullScreenUrl")
	private String imageFullScreenUrl;

	@JsonProperty("videoFullScreenUrl")
	private String videoFullScreenUrl;

	@JsonProperty("photoPromo")
	private String photoPromo;

	@JsonProperty("photoQR")
	private String photoQR;

	@JsonProperty("logoMapUrl")
	private String logoMapUrl;

	@JsonProperty("logoUrl")
	private String logoUrl;

	@JsonProperty("storeName")
	private String storeName;

	@JsonProperty("voteQuantity")
	private Integer voteQuantity;
	
	private String productHeader;
	
	@JsonProperty("storesRelated")
	private List<Store> storesRelated;

	public Product() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProductType() {
		return productType;
	}

	public void setProductType(int productType) {
		this.productType = productType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public float getPriceBefore() {
		return priceBefore;
	}

	public void setPriceBefore(float priceBefore) {
		this.priceBefore = priceBefore;
	}

	public float getPriceNow() {
		return priceNow;
	}

	public void setPriceNow(float priceNow) {
		this.priceNow = priceNow;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public float getPoints() {
		return points;
	}

	public void setPoints(float points) {
		this.points = points;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public boolean isPriority() {
		return isPriority;
	}

	public void setPriority(boolean isPriority) {
		this.isPriority = isPriority;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTimeToWait() {
		return timeToWait;
	}

	public void setTimeToWait(String timeToWait) {
		this.timeToWait = timeToWait;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTermAndConditions() {
		return termAndConditions;
	}

	public void setTermAndConditions(String termAndConditions) {
		this.termAndConditions = termAndConditions;
	}

	public boolean isShowBefore() {
		return showBefore;
	}

	public void setShowBefore(boolean showBefore) {
		this.showBefore = showBefore;
	}

	public boolean isShowAfter() {
		return showAfter;
	}

	public void setShowAfter(boolean showAfter) {
		this.showAfter = showAfter;
	}

	public boolean isHighlight() {
		return isHighlight;
	}

	public void setHighlight(boolean isHighlight) {
		this.isHighlight = isHighlight;
	}

	public String getImageFullScreenUrl() {
		return imageFullScreenUrl;
	}

	public void setImageFullScreenUrl(String imageFullScreenUrl) {
		this.imageFullScreenUrl = imageFullScreenUrl;
	}

	public String getVideoFullScreenUrl() {
		return videoFullScreenUrl;
	}

	public void setVideoFullScreenUrl(String videoFullScreenUrl) {
		this.videoFullScreenUrl = videoFullScreenUrl;
	}

	public String getPhotoPromo() {
		return photoPromo;
	}

	public void setPhotoPromo(String photoPromo) {
		this.photoPromo = photoPromo;
	}

	public String getPhotoQR() {
		return photoQR;
	}

	public void setPhotoQR(String photoQR) {
		this.photoQR = photoQR;
	}

	public String getLogoMapUrl() {
		return logoMapUrl;
	}

	public void setLogoMapUrl(String logoMapUrl) {
		this.logoMapUrl = logoMapUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Integer getVoteQuantity() {
		return voteQuantity;
	}

	public void setVoteQuantity(Integer voteQuantity) {
		this.voteQuantity = voteQuantity;
	}
	
	public String getProductHeader() {
		return productHeader;
	}

	public void setProductHeader(String productHeader) {
		this.productHeader = productHeader;
	}
	
	public List<Store> getStoresRelated() {
		return storesRelated;
	}

	public void setStoresRelated(List<Store> storesRelated) {
		this.storesRelated = storesRelated;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeInt(productType);
		dest.writeFloat(priceBefore);
		dest.writeFloat(priceNow);
		dest.writeFloat(discount);
		dest.writeFloat(points);
		dest.writeFloat(rating);
		dest.writeString(category);
		dest.writeString(notificationMessage);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(imageUrl);
		dest.writeString(startDate);
		dest.writeString(timeToWait);
		dest.writeString(endDate);
		dest.writeString(termAndConditions);
		dest.writeString(imageFullScreenUrl);
		dest.writeString(videoFullScreenUrl);
		dest.writeString(photoPromo);
		dest.writeString(photoQR);
		dest.writeString(logoMapUrl);
		dest.writeString(logoUrl);
		dest.writeString(storeName);
		dest.writeByte((byte) (isPriority ? 1 : 0));
		dest.writeByte((byte) (showBefore ? 1 : 0));
		dest.writeByte((byte) (showAfter ? 1 : 0));
		dest.writeByte((byte) (isHighlight ? 1 : 0));
		dest.writeInt((voteQuantity !=null ? voteQuantity : 0));
		dest.writeString((productHeader!=null ? productHeader : ""));
		try{
			dest.writeList(storesRelated);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public Product(Parcel source) {
		id = source.readInt();
		productType = source.readInt();
		priceBefore = source.readFloat();
		priceNow = source.readFloat();
		discount = source.readFloat();
		points = source.readFloat();
		rating = source.readFloat();
		category = source.readString();
		notificationMessage = source.readString();
		title = source.readString();
		description = source.readString();
		imageUrl = source.readString();
		startDate = source.readString();
		timeToWait = source.readString();
		endDate = source.readString();
		termAndConditions = source.readString();
		imageFullScreenUrl = source.readString();
		videoFullScreenUrl = source.readString();
		photoPromo = source.readString();
		photoQR = source.readString();
		logoMapUrl = source.readString();
		logoUrl = source.readString();
		storeName = source.readString();
		isPriority = source.readByte() == 1;
		showBefore = source.readByte() == 1;
		showAfter = source.readByte() == 1;
		isHighlight = source.readByte() == 1;
		voteQuantity = source.readInt();
		productHeader = source.readString();
		try{
			storesRelated = new ArrayList<Store>();
			source.readList(storesRelated,Store.class.getClassLoader());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
		public Product createFromParcel(Parcel source) {
			return new Product(source);
		}

		public Product[] newArray(int size) {
			return new Product[size];
		}
	};
}