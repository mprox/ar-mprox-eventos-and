package com.mprox.middleware.core.service.responses;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.User;
import com.mprox.middleware.core.domain.UserRedeem;

public class GetUserResponse extends BaseResponse{
	
	@JsonProperty("data")
	protected List<User> data;

	public List<User> getData() {
		return data;
	}

	public void setData(List<User> data) {
		this.data = data;
	}
}