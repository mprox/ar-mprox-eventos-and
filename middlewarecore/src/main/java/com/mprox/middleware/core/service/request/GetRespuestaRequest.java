package com.mprox.middleware.core.service.request;

public class GetRespuestaRequest {

	private long userId;

	public GetRespuestaRequest(long userId) {
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}

}