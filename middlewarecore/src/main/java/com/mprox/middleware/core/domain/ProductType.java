package com.mprox.middleware.core.domain;

public enum ProductType {
	PROMOTION(1),
	CHECKIN(2),
	REGISTER(3),
	VIDEO(4),
	IMAGEN(5),
	MENSAJE(6);
	
	private final int productTypeCode;
	
	private ProductType(int productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	/**
	 * @return the productTypeCode
	 */
	public int getProductTypeCode() {
		return productTypeCode;
	}

}
