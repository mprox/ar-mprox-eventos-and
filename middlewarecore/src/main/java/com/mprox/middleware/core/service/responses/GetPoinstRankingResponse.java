package com.mprox.middleware.core.service.responses;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.UserPointsRanking;

public class GetPoinstRankingResponse extends BaseResponse {

	@JsonProperty("data")
	protected UserPointsRanking data;

	public UserPointsRanking getData() {
		return data;
	}

	public void setData(UserPointsRanking data) {
		this.data = data;
	}
}