package com.mprox.middleware.core.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.middleware.core.service.request.GetActividadRequest;
import com.mprox.middleware.core.service.responses.GetAgendaResponse;
import com.mprox.middleware.core.util.ConstsService;

public abstract class GetAgendaTask extends AsyncTask<GetActividadRequest, Void, String> {

	private Context mContext;

	public GetAgendaTask(Context context) {
		super();
		this.mContext = context;
	}

	@Override
	protected String doInBackground(GetActividadRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/actividad/getList";
		JSONObject serviceParams = new JSONObject();

		GetActividadRequest request = params[0];

		try {
			serviceParams.put("deviceId", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));
			serviceParams.put("userId", request.getUserId());

		} catch (JSONException e1) {
			e1.printStackTrace();
			Log.e("Get Benefits Task", "Error while making json");
		}

		try {
			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Send Report Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		GetAgendaResponse response = new GetAgendaResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			
			if(result == null){
				result = readCache();
			}else{
				saveCache(result);
			}
			
			response = mapper.readValue(result, GetAgendaResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(GetAgendaResponse response);

	private void saveCache(String result){
		 try {
		        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput("getBenefits.txt", Context.MODE_PRIVATE));
		        outputStreamWriter.write(result);
		        outputStreamWriter.close();
		 }catch (IOException e) {
		     Log.e("Exception", "File write failed: " + e.toString());
		 } 
	}
	
	private String readCache(){
		String ret = "";

	    try {
	        InputStream inputStream = mContext.openFileInput("getBenefits.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}
}