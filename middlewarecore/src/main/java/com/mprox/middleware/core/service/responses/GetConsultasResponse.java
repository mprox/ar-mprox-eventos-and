package com.mprox.middleware.core.service.responses;

import com.mprox.middleware.core.domain.CalificacionActividad;
import com.mprox.middleware.core.domain.ConsultaActividad;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class GetConsultasResponse extends BaseResponse{

	@JsonProperty("data")
	protected List<ConsultaActividad> data;

	public List<ConsultaActividad> getData() {
		return data;
	}

	public void setData(List<ConsultaActividad> data) {
		this.data = data;
	}
}