package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Evento  implements Parcelable {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("title")
	private String title;

	@JsonProperty("descripcion")
	private String descripcion;

	@JsonProperty("fechaStart")
	private String fechaStart;

	@JsonProperty("fechaEnd")
	private String fechaEnd;

	@JsonProperty("horaStart")
	private String horaStart;

	@JsonProperty("horaEnd")
	private String horaEnd;

	@JsonProperty("status")
	private Boolean status;

	@JsonProperty("imagen")
	private String imagen;

	public Evento() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return the imagen
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * @param imagen the imagen to set
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	/**
	 * @return the fechaStart
	 */
	public String getFechaStart() {
		return fechaStart;
	}

	/**
	 * @param fechaStart the fechaStart to set
	 */
	public void setFechaStart(String fechaStart) {
		this.fechaStart = fechaStart;
	}

	/**
	 * @return the fechaEnd
	 */
	public String getFechaEnd() {
		return fechaEnd;
	}

	/**
	 * @param fechaEnd the fechaEnd to set
	 */
	public void setFechaEnd(String fechaEnd) {
		this.fechaEnd = fechaEnd;
	}

	/**
	 * @return the horaStart
	 */
	public String getHoraStart() {
		return horaStart;
	}

	/**
	 * @param horaStart the horaStart to set
	 */
	public void setHoraStart(String horaStart) {
		this.horaStart = horaStart;
	}

	/**
	 * @return the horaEnd
	 */
	public String getHoraEnd() {
		return horaEnd;
	}

	/**
	 * @param horaEnd the horaEnd to set
	 */
	public void setHoraEnd(String horaEnd) {
		this.horaEnd = horaEnd;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(title);
		dest.writeString(descripcion);
		dest.writeString(fechaStart);
		dest.writeString(fechaEnd);
		dest.writeString(horaStart);
		dest.writeString(horaEnd);
		dest.writeByte((byte) (status ? 1 : 0));
		dest.writeString(imagen);
	}

	public Evento(Parcel source) {
		id = source.readLong();
		title = source.readString();
		descripcion = source.readString();
		fechaStart = source.readString();
		fechaEnd = source.readString();
		horaStart = source.readString();
		horaEnd = source.readString();
		status = source.readByte() == 1;
		imagen = source.readString();
	}

	public static final Parcelable.Creator<Evento> CREATOR = new Parcelable.Creator<Evento>() {
		public Evento createFromParcel(Parcel source) {
			return new Evento(source);
		}

		public Evento[] newArray(int size) {
			return new Evento[size];
		}
	};

}