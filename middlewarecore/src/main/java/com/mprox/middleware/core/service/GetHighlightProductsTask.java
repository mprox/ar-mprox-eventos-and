package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mprox.middleware.core.service.responses.GetAgendaResponse;
import com.mprox.middleware.core.util.ConstsService;

public abstract class GetHighlightProductsTask extends AsyncTask<Void, Void, String> {

	public GetHighlightProductsTask(Context context) {
		super();
	}

	@Override
	protected String doInBackground(Void... params) {

		String response = "";
		String baseUrl = ConstsService.PROD_BASE_URL + "/product/getHighlightProducts";
		JSONObject serviceParams = new JSONObject();

		try {
			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Send Get Point Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		GetAgendaResponse response = new GetAgendaResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, GetAgendaResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(GetAgendaResponse response);

}
