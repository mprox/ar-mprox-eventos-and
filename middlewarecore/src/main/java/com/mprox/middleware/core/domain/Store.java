package com.mprox.middleware.core.domain;

import org.codehaus.jackson.annotate.JsonProperty;

import android.os.Parcel;
import android.os.Parcelable;

public class Store implements Parcelable {

	@JsonProperty("idStore")
	private int idStore;

	@JsonProperty("description")
	private String description;

	@JsonProperty("latitude")
	private String latitude;

	@JsonProperty("longitude")
	private String longitude;

	@JsonProperty("location")
	private String location;

	@JsonProperty("logoMapUrl")
	private String logoMapUrl;

	@JsonProperty("logoUrl")
	private String logoUrl;

	@JsonProperty("photoDetailsUrl")
	private String photoDetailsUrl;

	@JsonProperty("name")
	private String name;

	@JsonProperty("rating")
	private float rating;

	@JsonProperty("voteQuantity")
	private int voteQuantity;

	public Store() {
	}

	public int getIdStore() {
		return idStore;
	}

	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLogoMapUrl() {
		return logoMapUrl;
	}

	public void setLogoMapUrl(String logoMapUrl) {
		this.logoMapUrl = logoMapUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getVoteQuantity() {
		return voteQuantity;
	}

	public void setVoteQuantity(int voteQuantity) {
		this.voteQuantity = voteQuantity;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(idStore);
		dest.writeString(description);
		dest.writeString(latitude);
		dest.writeString(longitude);
		dest.writeString(location);
		dest.writeString(logoMapUrl);
		dest.writeString(logoUrl);
		dest.writeString(photoDetailsUrl);
		dest.writeString(name);
		dest.writeFloat(rating);
		dest.writeInt(voteQuantity);
	}

	public Store(Parcel source) {
		idStore = source.readInt();
		description = source.readString();
		latitude = source.readString();
		longitude = source.readString();
		location = source.readString();
		logoMapUrl = source.readString();
		logoUrl = source.readString();
		photoDetailsUrl = source.readString();
		name = source.readString();
		rating = source.readFloat();
		voteQuantity = source.readInt();
	}

	public static final Parcelable.Creator<Store> CREATOR = new Parcelable.Creator<Store>() {
		public Store createFromParcel(Parcel source) {
			return new Store(source);
		}

		public Store[] newArray(int size) {
			return new Store[size];
		}
	};

	public String getPhotoDetailsUrl() {
		return photoDetailsUrl;
	}

	public void setPhotoDetailsUrl(String photoDetailsUrl) {
		this.photoDetailsUrl = photoDetailsUrl;
	}
}
