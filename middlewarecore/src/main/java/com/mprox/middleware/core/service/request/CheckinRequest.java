package com.mprox.middleware.core.service.request;

public class CheckinRequest {

	private String beaconId;
	private String productId;
	private String userId;
	
	public CheckinRequest(String beaconId, String productId, String userId){
		this.beaconId = beaconId;
		this.productId = productId;
		this.userId = userId;
	}

	/**
	 * @return the beaconId
	 */
	public String getBeaconId() {
		return beaconId;
	}

	/**
	 * @param beaconId the beaconId to set
	 */
	public void setBeaconId(String beaconId) {
		this.beaconId = beaconId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

}