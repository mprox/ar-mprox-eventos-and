package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.middleware.core.service.request.CheckinRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.ConstsService;
import com.mprox.middleware.core.util.UtilsMiddleware;

public abstract class CheckinTask extends AsyncTask<CheckinRequest, Void, String> {

	private Context	mContext;
	
	public CheckinTask(Context context) {
		this.mContext = context;
	}
	
	@Override
	protected String doInBackground(CheckinRequest... params) {
		String response = "";

		CheckinRequest request = params[0];

		String baseUrl = ConstsService.PROD_BASE_URL + "/user/checkin";
		JSONObject serviceParams = new JSONObject();

		try {

			if (!UtilsMiddleware.isNullOrEmpty(request.getBeaconId())) {
				serviceParams.put("beaconId", request.getBeaconId());
			}

			if (!UtilsMiddleware.isNullOrEmpty(request.getProductId())) {
				serviceParams.put("productId", request.getProductId());
			}

			if (!UtilsMiddleware.isNullOrEmpty(request.getUserId())) {
				serviceParams.put("userId", request.getUserId());
			}
			
			serviceParams.put("deviceId", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}
		Log.i("Checkin Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		BaseResponse response = new BaseResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, BaseResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(BaseResponse response);
}
