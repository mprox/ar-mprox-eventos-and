package com.mprox.middleware.core.service.responses;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.mprox.middleware.core.domain.Actividad;
import com.mprox.middleware.core.domain.Product;

public class GetAgendaResponse extends BaseResponse{

	@JsonProperty("data")
	protected List<Actividad> data;

	public List<Actividad> getData() {
		return data;
	}

	public void setData(List<Actividad> data) {
		this.data = data;
	}
}