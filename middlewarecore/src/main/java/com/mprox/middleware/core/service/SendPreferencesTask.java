package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;

import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.ConstsService;
import com.mprox.middleware.core.util.UtilsMiddleware;

public abstract class SendPreferencesTask extends AsyncTask<String, Void, String> {

	private Context mContext;

	public SendPreferencesTask(Context context) {
		super();
		mContext = context;
	}

	@Override
	protected String doInBackground(String... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/user/updateUserTags";
		JSONObject serviceParams = new JSONObject();

		String preferences = params[0];

		try {

			serviceParams.put("tags", preferences);

			serviceParams.put("deviceId", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));

			if (UtilsMiddleware.getInstance(mContext).getLogedUserId() != 0) {
				serviceParams.put("userId", UtilsMiddleware.getInstance(mContext).getLogedUserId() + "");
			}

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Send Preferences Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		BaseResponse response = new BaseResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, BaseResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(BaseResponse response);

}
