package com.mprox.middleware.core.domain;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonProperty;

public class User implements Parcelable{
	
	@JsonProperty("id")
	private Long id;

    @JsonProperty("category")
    private String category;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("gender")
	private String gender;

    @JsonProperty("identifier")
    private String identifier;

	@JsonProperty("tags")
	private String tags;

	@JsonProperty("imagen")
	private String imagen;

    @JsonProperty("esOrador")
    private Boolean esOrador = false;

    @JsonProperty("gerencia")
    private String gerencia;

    @JsonProperty("descripcion")
    private String descripcion;

    @JsonProperty("hobbies")
    private String hobbies;

    @JsonProperty("equipoParticipante")
    private String equipoParticipante;

    @JsonProperty("voteQuantity")
    private Float voteQuantity;

    @JsonProperty("voteTotal")
    private Float voteTotal;

    public Float getVoteTotal() {
        if(voteTotal == null){
            return 0f;
        }
        return voteTotal;
    }

    public Float getVoteQuantity() {
        return voteQuantity;
    }

    public String getImagen() {
        return imagen;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setVoteQuantity(Float voteQuantity) {
        this.voteQuantity = voteQuantity;
    }

    public void setVoteTotal(Float voteTotal) {
        this.voteTotal = voteTotal;
    }

    public User() {
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

    public String getImage() {
        return imagen;
    }

    public void setImagen(String image) {
        this.imagen = image;
    }

    public Boolean getEsOrador() {
        return esOrador;
    }

    public void setEsOrador(Boolean esOrador) {
        this.esOrador = esOrador;
    }

    public String getGerencia() {
        return gerencia;
    }

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEquipoParticipante() {
        return equipoParticipante;
    }

    public void setEquipoParticipante(String equipoParticipante) {
        this.equipoParticipante = equipoParticipante;
    }

    @Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(email);
		dest.writeString(gender);
		dest.writeString(tags);
        dest.writeString(imagen);
        dest.writeByte((byte) (esOrador ? 1 : 0));
        dest.writeString(gerencia);
        dest.writeString(descripcion);
        dest.writeString(hobbies);
        dest.writeString(equipoParticipante);
        dest.writeFloat((voteTotal!= null)?voteTotal:0);
        dest.writeFloat((voteQuantity!= null)?voteQuantity:0);

	}

	public User(Parcel source) {
		id = source.readLong();
		name = source.readString();
		email = source.readString();
		gender = source.readString();
		tags = source.readString();
		imagen = source.readString();
        esOrador = source.readByte() == 1;
        gerencia = source.readString();
        descripcion = source.readString();
        hobbies = source.readString();
        equipoParticipante = source.readString();
        voteQuantity = source.readFloat();
        voteTotal = source.readFloat();
	}

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
