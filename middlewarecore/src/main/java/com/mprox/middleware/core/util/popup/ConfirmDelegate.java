
package com.mprox.middleware.core.util.popup;

/**
 * @author Matias Dumrauf
 * @since 1.0
 */
public interface ConfirmDelegate {


	/**
	 * On click YES event
	 */
	public void onYes();


	/**
	 * On click NO event
	 */
	public void onNo();


}
