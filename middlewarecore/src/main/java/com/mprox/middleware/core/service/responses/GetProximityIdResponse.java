package com.mprox.middleware.core.service.responses;

import org.codehaus.jackson.annotate.JsonProperty;

public class GetProximityIdResponse extends BaseResponse{
	@JsonProperty("data")
	protected String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}