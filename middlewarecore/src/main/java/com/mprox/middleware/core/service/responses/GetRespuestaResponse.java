package com.mprox.middleware.core.service.responses;

import com.mprox.middleware.core.domain.Respuesta;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class GetRespuestaResponse extends BaseResponse{
	
	@JsonProperty("data")
	protected List<Respuesta> data;

	public List<Respuesta> getData() {
		return data;
	}

	public void setData(List<Respuesta> data) {
		this.data = data;
	}
}