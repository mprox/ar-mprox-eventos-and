package com.mprox.middleware.core.service;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.mprox.middleware.core.service.request.RateActividadRequest;
import com.mprox.middleware.core.service.responses.BaseResponse;
import com.mprox.middleware.core.util.ConstsService;

public abstract class RatingActividadTask extends AsyncTask<RateActividadRequest, Void, String> {

	@Override
	protected String doInBackground(RateActividadRequest... params) {
		String response = "";

		String baseUrl = ConstsService.PROD_BASE_URL + "/actividad/setCalificacion";
		JSONObject serviceParams = new JSONObject();

		RateActividadRequest rateMsg = params[0];

		try {

			serviceParams.put("actividadId", rateMsg.getActividadId());
			serviceParams.put("rating", rateMsg.getRating() + "");
			serviceParams.put("userId", rateMsg.getUserId());

			if(rateMsg.getObservaciones() != null)
				serviceParams.put("observacion", rateMsg.getObservaciones());

			response = new HttpService().post(baseUrl, serviceParams);
		} catch (Exception e) {
			response = "exeption during service execution";
		}

		Log.i("Rating Task", "" + response);

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		BaseResponse response = new BaseResponse();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(result, BaseResponse.class);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMsessage("Error while parsing");
		}

		onServiceReturned(response);
	}

	public abstract void onServiceReturned(BaseResponse response);
}