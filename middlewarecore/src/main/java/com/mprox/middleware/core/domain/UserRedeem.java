package com.mprox.middleware.core.domain;

import org.codehaus.jackson.annotate.JsonProperty;

import android.os.Parcel;
import android.os.Parcelable;

public class UserRedeem implements Parcelable {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("date")
	private String date;

	@JsonProperty("store")
	private String store;

	@JsonProperty("description")
	private String description;

	@JsonProperty("adress")
	private String adress;

	@JsonProperty("invoiceNumber")
	private String invoiceNumber;

	@JsonProperty("total")
	private Float total;

	@JsonProperty("discount")
	private Float discount;

	@JsonProperty("valuation")
	private Float valuation;

	@JsonProperty("productId")
	private String productsId;

	@JsonProperty("productPhotoPromo")
	private String productPhotoPromo;

	public UserRedeem() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Float getValuation() {
		return valuation;
	}

	public void setValuation(Float valuation) {
		this.valuation = valuation;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(date);
		dest.writeString(store);
		dest.writeString(description);
		dest.writeString(adress);
		dest.writeString(invoiceNumber);
		dest.writeFloat(total);
		dest.writeFloat(discount);
		dest.writeFloat(valuation);
		dest.writeString(productsId);
		dest.writeString(productPhotoPromo);
	}

	public UserRedeem(Parcel source) {
		id = source.readLong();
		date = source.readString();
		store = source.readString();
		description = source.readString();
		adress = source.readString();
		invoiceNumber = source.readString();
		total = source.readFloat();
		discount = source.readFloat();
		valuation = source.readFloat();
		productsId = source.readString();
		productPhotoPromo = source.readString();
	}

	public static final Parcelable.Creator<UserRedeem> CREATOR = new Parcelable.Creator<UserRedeem>() {
		public UserRedeem createFromParcel(Parcel source) {
			return new UserRedeem(source);
		}

		public UserRedeem[] newArray(int size) {
			return new UserRedeem[size];
		}
	};

	public String getProductPhotoPromo() {
		return productPhotoPromo;
	}

	public void setProductPhotoPromo(String productPhotoPromo) {
		this.productPhotoPromo = productPhotoPromo;
	}

	public String getProductsId() {
		return productsId;
	}

	public void setProductsId(String productsId) {
		this.productsId = productsId;
	}

}
